;;; tabkit.el --- A tabs UI for Emacs -*- lexical-binding:t -*-

;; Copyright (C) 2003-2025 David Ponce

;; Author: David Ponce <da_vid@orange.fr>
;; Maintainer: David Ponce <da_vid@orange.fr>
;; Created: 25 February 2003
;; Keywords: convenience windows tabs

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; This library provides a global minor mode to display tabs on the
;; `tab-line' area of every window, when Emacs is using a graphic
;; display.  To enable this mode, run the command `tabkit-mode'.
;;
;; You can find more details in the README.md file (or the README.html
;; file generated from it).  And of course by reading the code itself!
;;

;;; Code:

(require 'easymenu)
(require 'map)
(require 'shr)

(require 'gmouse)
(require 'nuancier nil t)
(require 'pic)

;;; Top elements
;;
(defgroup tabkit nil
  "A tabs UI for Emacs."
  :group 'convenience)

(defconst tabkit-line 'tab-line
  "Were to display the tabkit line.")

(defconst tabkit-line-format 'tab-line-format
  "The variable that controls the display of the tabkit line.")

(defconst tabkit-line-template '(:eval (tabkit-line))
  "The template for displaying the tabkit line.")

(defconst tabkit--catch-all-group-name (make-temp-name "tabkit--")
  "Name of internal catch-all group used when there is no group.")

;;; Hooks
;;
(defvar tabkit-after-tabs-change-hook nil
  "Hook run after tabs have changed.
This hook is useful to also refresh other dependent data.")

(defvar tabkit-tabs-data-cleanup-hook nil
  "Hook run before to cleanup tabs data when refresh is requested.
This hook is useful to also cleanup other dependent tabs data.")

(defvar tabkit-before-data-save-hook nil
  "Hook run before to save persistent data.
This hook is run before to save persistent variables defined in
`tabkit-persistent-variables', to eventually update their value.")

(defvar tabkit-after-data-load-hook nil
  "Hook run after persistent data has been loaded.")

(defvar tabkit-refresh-ui-functions nil
  "List of functions to call after UI has been changed.
Each function is called with one of these value:
- a symbol naming an UI element, like a button name.
- t for all UI elements.")


;;; Misc. convenience code
;;
(defsubst tabkit--ratio (n)
  "Return non-nil if N is a positive float > 0."
  (and (floatp n) (> n 0)))

(defsubst tabkit--%ratio (n)
  "Return non-nil if N is a positive float > 0 and <= 1."
  (and (floatp n) (> n 0) (<= n 1)))

(defsubst tabkit--natnum* (n)
  "Return non-nil if N is a positive integer > 0."
  (and (natnump n) (> n 0)))

(defsubst tabkit--list-same-p (list1 list2 &optional testfn)
  "Return non-nil if LIST1 and LIST2 have same elements and length.
If TESTFN is non-nil, equality of elements is tested by `equal',
otherwise by `eq'."
  (funcall (if testfn #'+-list-samequal-p #'+-list-sameq-p)
           list1 list2))

(defun tabkit--pretty-symbol-name (symbol)
  "Return a pretty symbol name from SYMBOL.
Replace all occurrences of characters `-', `:', and `/' by a single
space and capitalize words."
  (capitalize
   (replace-regexp-in-string "[-:/]+" " " (symbol-name symbol) t t)))

(defmacro tabkit-display-update ()
  "Force display refresh."
  '(force-mode-line-update t))

(defun tabkit--move-mouse-pointer (dx dy)
  "Move the mouse pointer to (x+DX . y+DY) pixel position."
  (let ((p (mouse-absolute-pixel-position)))
    (set-mouse-absolute-pixel-position (+ (car p) dx) (+ (cdr p) dy))))

;;; Persistent data
;;
(defvar tabkit-persistent-variables
  '(
    tabkit--closed-tabs
    tabkit--instant-color-tab-criteria
    tabkit--instant-color-group-criteria
    tabkit--instant-group-criteria
    tabkit--persistent-tabs
    )
  "List of variables that contains persistent data.
Variables are saved then restored in the specified order.")

(defcustom tabkit-backup-persistent-data nil
  "Non-nil means make a backup of persistent data when it is saved.
Persistent variables in the package `tabkit' are copied to the backup
package `tabkit~'."
  :type 'boolean)

(defun tabkit-persistent-data-backup ()
  "Backup previous value of persistent data."
  (when tabkit-backup-persistent-data
    (dolist (v tabkit-persistent-variables)
      (setf (+-persistent-value v 'tabkit~)
            (+-persistent-value v 'tabkit)))))

(defvar tabkit--persistent-data-loaded nil
  "Non-nil means persistent data has been loaded.")

(defun tabkit-persistent-data-save ()
  "Save persistent data contained in `tabkit-persistent-variables'.
Run hook `tabkit-before-data--save-hook' before to save persistent
variables."
  (when tabkit--persistent-data-loaded
    (tabkit-persistent-data-backup)
    (run-hooks 'tabkit-before-data-save-hook)
    (dolist (v tabkit-persistent-variables)
      (ignore-errors
        (setf (+-persistent-value v 'tabkit) (symbol-value v))))
    ;; Mark persistent data to be loaded again.
    (setq tabkit--persistent-data-loaded nil)))

(defun tabkit-persistent-data-load ()
  "Load persistent data.
Run hook `tabkit-after-data-load-hook' after persistent variables has
been loaded."
  (unless tabkit--persistent-data-loaded
    (dolist (v tabkit-persistent-variables)
        (set v (+-persistent-value v 'tabkit)))
    ;; Mark persistent data as loaded.
    (setq tabkit--persistent-data-loaded t)
    (run-hooks 'tabkit-after-data-load-hook)))

;;; Annotate tab names
;;
;; Annotations are serializable as text properties.
;; Tab names are annotated to distinguish them in serialized data.
;; For example, to distinguish a buffer tab visiting a file and a
;; buffer tab not visiting a file, when both have the same name.
;; Instant colors and groups are associated to annotated tab names.
;; That is, a buffer tab visiting a file and a buffer tab not visiting
;; a file can have different instant colors and groups, even if they
;; have the same name.
;;
(defmacro tabkit-annotated-name (name symbol)
  "Return a copy of NAME annotated with SYMBOL."
  `(propertize ,name ,symbol t))

(defmacro tabkit-annotated-name-p (name symbol)
  "Return non-nil if NAME is annotated with SYMBOL."
  `(get-text-property 0 ,symbol ,name))

(defalias 'tabkit-name-equal #'equal-including-properties
  "Return non-nil if NAME1 is equal to NAME2 including annotations.
That is, NAME1 string is equal to NAME2 string and both have the same
text properties.")

;;; Widgets
;;
(define-widget 'tabkit--ratio 'float
  "A positive non-zero floating-point value."
  :tag "Positive ratio"
  :value 1.0
  :type-error "Invalid positive floating-point ratio."
  :match-alternatives `(,#'tabkit--ratio))

(define-widget 'tabkit--%ratio 'float
  "A positive non-zero floating-point value not greater than 1."
  :tag "Floating-point percentage"
  :value 1.0
  :type-error "Invalid percentage ratio."
  :match-alternatives `(,#'tabkit--%ratio))

(define-widget 'tabkit--natnum* 'natnum
  "A positive non-zero integer value."
  :tag "Positive integer"
  :value 1
  :type-error "Invalid positive integer."
  :match-alternatives `(,#'tabkit--natnum*))

(define-widget 'tabkit--optfun 'function
  "A Lisp function or nil."
  :format "%{%t%}: %v\n"
  :match-alternatives '(functionp null)
  :validate (lambda (widget)
              (let ((v (widget-value widget)))
                (unless (or (null v) (functionp v))
                  (widget-put
                   widget :error (format "Invalid function: %S" v))
                  widget))))

(define-widget 'tabkit-icon-simple 'pic-icon-glyphs
  "Widget to customize a Simple type icon."
  :format (concat pic-icon-glyphs-group-format "\n"))

(define-widget 'tabkit-icon-conditional 'cons
  "Widget to customize Conditional type icon."
  :tag ""
  :format pic-icon-glyphs-group-format
  :args '((pic-icon-glyphs :tag "Enabled")
          (pic-icon-glyphs :tag "Disabled")))

(define-widget 'tabkit-icon-toggle 'cons
  "Widget to customize a Toggle type icon."
  :tag ""
  :format pic-icon-glyphs-group-format
  :args `((pic-icon-glyphs :tag "State1")
          (pic-icon-glyphs :tag "State2")))

;;; Window local variables
;;
(eval-when-compile (require 'wilo))
(define-window-local-group tabkit)

(defun tabkit--window-local-clone (&optional window)
  "Return a twin copy of tabkit data local to WINDOW.
WINDOW can be any window and defaults to the selected one."
  (let ((group (copy-sequence (tabkit-window-local-bindings window)))
        views)
    (dolist (v (plist-get group :views))
      (let* ((view (copy-sequence v))
             (tabs (copy-sequence (plist-get (cdr view) :tabs))))
        (plist-put (cdr view) :tabs tabs)
        (push view views)))
    (plist-put group :views (nreverse views))
    group))

;;; Memoization
;;
(eval-when-compile (require 'memo))
(define-memo tabkit)

;;; Custom stuff
;;
(defun tabkit--custom-set-p (symbol)
  "Return non-nil if SYMBOL default and standard values are different."
  (custom-load-symbol symbol)
  (let* ((get (or (get symbol 'custom-get) #'default-value))
         (val (funcall get symbol))
         (std (get symbol 'standard-value)))
    (unless (and std (equal val (ignore-errors (eval (car std)))))
      (list val))))

(defun tabkit-refresh-ui-element (element)
  "Clear the cached value of UI ELEMENT.
ELEMENT is either a symbol that names an UI element, like a button
name; or t which means all UI elements."
  (if (eq element t)
      (tabkit-memo-init)
    (setf (tabkit-memo element) nil)))

(defun tabkit--custom-set-and-refresh-elt (variable value)
  "Set custom VARIABLE to VALUE, and refresh corresponding element.
Same as `custom-set-default', plus clear cached value of VARIABLE."
  (custom-set-default variable value)
  (run-hook-with-args 'tabkit-refresh-ui-functions variable))

(defun tabkit--custom-set-and-refresh-all (variable value)
  "Set custom VARIABLE to VALUE, and refresh all elements.
Same as `custom-set-default', plus clear all cached values."
  (custom-set-default variable value)
  (run-hook-with-args 'tabkit-refresh-ui-functions t))

(defun tabkit--custom-set-and-refresh-tabs (variable value)
  "Set custom VARIABLE to VALUE, and refresh tabs.
Same as `custom-set-default', plus clear all cached values and
internal tabs data."
  (tabkit--custom-set-and-refresh-all variable value)
  ;; Not fbound when custom-file loaded before library.
  (and (fboundp 'tabkit-tabs-data-refresh)
       (tabkit-tabs-data-refresh)))

;;; Delegated functions
;;
(defcustom tabkit-current-tabset-function
  #'tabkit-buffer-tabs
  "Function to obtain the list of tabs to display on the tabkit line.
This function is called with no argument, and must return a tabset.
This variable can also be set in a `tabkit-mode-hook' run when
`tabkit-mode' is toggled."
  :type 'function)

(defcustom tabkit-exclude-predicate
  #'tabkit-buffer-exclude-p
  "Function to exclude tab from the tabkit line.
This function receives an optional tab value, and return non-nil to
not create a corresponding tab on the tabkit line.  If not a function,
no tab is excluded.
This variable can also be set in a `tabkit-mode-hook' run when
`tabkit-mode' is toggled."
  :type 'tabkit--optfun)

;;; Faces
;;
(defface tabkit--base
  '((t
     ))
  "Base face of the tabkit line for internal use.
Customization of this face is ignored.")

(defface tabkit-default
  '((t
     :inherit tab-line
     :box nil
     ))
  "Default face of the tabkit line.")

(defface tabkit-separator
  '((t
     :background "gray"
     :width condensed
     ))
  "Face of separator.")

(defface tabkit-button-highlight
  '((t
     :inherit highlight
     ))
  "Face to highlight a button component on mouse hover.")

(defface tabkit-tab-unselected
  '((t
     :foreground "dark gray"
     ))
  "Face of unselected tab.")

(defface tabkit-tab-selected
  '((t
     :background "white"
     ))
  "Face of the selected tab.")

(defface tabkit-tab-highlight
  '((t
     ))
  "Face to highlight tab on mouse hover.")

(defface tabkit-tab-drag-highlight
  '((t
     :overline nil
     :underline nil
     :box (:line-width (-3 . -3) :color "black" :style flat-button)
     ))
  "Face to highlight dragged tab.")

(defface tabkit-tab-group
  '((t
     ))
  "Face of group tab.")

(defface tabkit-tab-instant-group
  '((t
     :inherit tabkit-tab-group
     :slant italic
     ))
  "Face of instant group tab.")

;;; Base icons
;;
(define-pic-icon tabkit-icon nil
  '((:emoji nil :face (:family "Noto Color Emoji")))
  "The base icon that all other icons inherit from."
  :set #'tabkit--custom-set-and-refresh-all)

(define-pic-icon tabkit-button-icon tabkit-icon
  '()
  "The icon that all button icons inherit from."
  :set #'tabkit--custom-set-and-refresh-all)

(define-pic-icon tabkit-tab-icon tabkit-icon
  '()
  "The icon that all tab icons inherit from."
  :set #'tabkit--custom-set-and-refresh-all)

(define-pic-icon tabkit-misc-icon tabkit-icon
  '()
  "The icon that all misc. icons inherit from."
  :set #'tabkit--custom-set-and-refresh-all)

;;; Misc options
;;
(defcustom tabkit-use-groups t
  "If non-nil, tabs will be organized in groups.
Only 1 group is displayed at a time on the tabkit line, and you can
navigate across the different groups.  Otherwise, all tabs will be
displayed at the same time on the tabkit line."
  :type 'boolean
  :set #'tabkit--custom-set-and-refresh-tabs)

(define-inline tabkit-showing-groups (&optional window)
  "Return non-nil if displaying tab groups in WINDOW.
WINDOW can be any window and defaults to the selected one.
Value can be set with (setf (tabkit-showing-group WINDOW) VALUE)."
  (inline-quote
   (and tabkit-use-groups
        (tabkit-window-local :showing-groups ,window))))

(gv-define-setter tabkit-showing-groups (value &optional window)
  `(progn
     (setf (tabkit-window-local :showing-groups ,window) ,value)
     (tabkit-display-update)))

(define-inline tabkit-tab-showing-group (tab)
  "Return non-nil if TAB is showing group in selected window.
That is, either the displayed group name or nil if not showing group."
  (inline-quote
   (and (tabkit-showing-groups) (tabkit-tab-group-name ,tab))))

;;; Images
;;
(defcustom tabkit-use-images t
  "Non-nil means to try to use images on the tabkit line."
  :type 'boolean
  :set #'tabkit--custom-set-and-refresh-all)

(defcustom tabkit-image-icon-theme nil
  "Theme name used to lookup XDG icon images, or nil for default."
  :type '(choice
          (const  :tag "Default" nil)
          (string :tag "XDG Icon Theme"))
  :set #'tabkit--custom-set-and-refresh-all)

(defcustom tabkit-image-icon-size nil
  "Size used to lookup XDG icon images, or nil for default."
  :type '(choice
          (const  :tag "Default" nil)
          (tabkit--natnum* :tag "XDG Icon Size" 48))
  :set #'tabkit--custom-set-and-refresh-all)

(defcustom tabkit-image-icon-scale nil
  "Scale used to lookup XDG icon images, or nil for default."
  :type '(choice
          (const :tag "Default" nil)
          (tabkit--ratio :tag "XDG Icon Scale" 1.0))
  :set #'tabkit--custom-set-and-refresh-all)

(defcustom tabkit-line-height 1.3
  "Height of the tabkit line.

- An integer specifies an absolute number of pixels.  A value
  lower than the font height of the `tabkit-default' face, will
  be considered as this minimum height.

- A floating-point value specifies a percentage ratio of the font
  height of the `tabkit-default' face.

Use the function `tabkit-line-pixel-height' to get the actual value."
  :type `(choice
          (tabkit--natnum* :tag "Number of pixels")
          (tabkit--ratio   :tag "Ratio of font height"))
  :set #'tabkit--custom-set-and-refresh-all)

(define-inline tabkit-scale-font-height (scale &optional window face)
  "Return font pixels height multiplied by SCALE.
WINDOW must be a live window and defaults to the selected one.
Use font from FACE, or that from the `tabkit-default' face, if FACE is
nil or omitted."
  (inline-quote
   (ceiling (* (window-font-height ,window (or ,face 'tabkit-default))
               ,scale))))

(defun tabkit-line-pixel-height ()
  "Return the height in pixels of the tabkit line.
See also the option `tabkit-line-height'.
Returned value is not less than the height in pixels of the
`tabkit-default' face."
  (tabkit-with-memo 'tabkit-line-pixel-height
    (max (tabkit-scale-font-height 1.0)
         (cond
          ((tabkit--ratio tabkit-line-height)
           (tabkit-scale-font-height tabkit-line-height))
          ((tabkit--natnum* tabkit-line-height)
           tabkit-line-height)
          ((error "Invalid line height, %S"
                  tabkit-line-height))))))

(defcustom tabkit-image-height 0.7
  "Height of images on the tabkit line.

- An integer specifies an absolute number of pixels.  A value greater
  than the `tabkit-line-height', will be considered as this maximum
  height.

- A floating-point value specifies a percentage ratio of the
  `tabkit-line-height'.

Use the function `tabkit-image-pixel-height' to get the actual value."
  :type '(choice
          (tabkit--natnum* :tag "Number of pixels")
          (tabkit--%ratio  :tag "Percentage ratio of line height"))
  :set #'tabkit--custom-set-and-refresh-all)

(defun tabkit-image-pixel-height ()
  "Return the height in pixels of an image on the tabkit line.
See also the option `tabkit-image-height'.
Returned value is not greater than the height in pixels of the tabkit
line returned by the function `tabkit-line-pixel-height'."
  (tabkit-with-memo 'tabkit-image-pixel-height
    (let ((line-pixel-height (tabkit-line-pixel-height)))
      (min line-pixel-height
           (cond
            ((tabkit--%ratio tabkit-image-height)
             (round (* line-pixel-height tabkit-image-height)))
            ((tabkit--natnum* tabkit-image-height)
             tabkit-image-height)
            ((error "Invalid image height, %S"
                    tabkit-image-height)))))))

(defun tabkit-image-normalize (image)
  "Unless otherwise specified, normalize IMAGE appearance.
That is:

1) Unless image `:scale' is set, set it to 1.

2) Unless image `:ascent' is set, center image vertically.

3) Unless image `:mask' is set, make image background transparent.

4) Unless image `:height' or `:width' are set, resize the image to fit
   specified `tabkit-image-height'.

5) Unless image `:margin' is set, adjust margin so that the image
   occupies a square of `tabkit-line-height' side."
  (or (plist-member (cdr image) :scale)
      ;; Don't scale image by default.
      (setf (pic-image-property image :scale) 1))
  (or (plist-member (cdr image) :ascent)
      ;; Center image.
      (setf (pic-image-property image :ascent) 'center))
  (or (plist-member (cdr image) :mask)
      ;; Make image background transparent.
      (setf (pic-image-property image :mask) '(heuristic t)))
  (or (plist-member (cdr image) :height)
      (plist-member (cdr image) :width)
      ;; Resize image taller than tabkit image height.
      (let ((h (tabkit-image-pixel-height)))
        (if (> (cdr (pic-image-size image)) h)
            (setf (pic-image-property image :height) h))))
  (or (plist-member (cdr image) :margin)
      ;; Adjust margins to occupy a square of line height side.
      (let* ((lh (tabkit-line-pixel-height))
             (is (pic-image-size image))
             (hm (max (/ (- lh (car is)) 2) 0))
             ;; Don't set v-margin when image is v-centered.
             (vm (if (eq (pic-image-property image :ascent) 'center)
                     0
                   (max (/ (- lh (cdr is)) 2) 0))))
        (setf (pic-image-property image :margin) (cons hm vm))))
  image)

(defvar xdg-icontheme-theme)
(defvar xdg-icontheme-size)
(defvar xdg-icontheme-scale)

(defun tabkit-find-image (specs &optional props)
  "Find an image, choosing one of a list of image specifications.
Return an image normalized by `tabkit-image-normalize' or nil if not
found.
SPECS is a list of image specifications, (SPEC1 [SPEC2] ...).
Optional argument PROPS is a list of image properties applied to the
returned image.."
  (when tabkit-use-images
    (let* ((xdg-icontheme-theme tabkit-image-icon-theme)
           (xdg-icontheme-size  tabkit-image-icon-size)
           (xdg-icontheme-scale tabkit-image-icon-scale)
           (pic-image-properties props)
           (image (pic-image-find specs)))
      (and image (tabkit-image-normalize image)))))

;;; Misc. visual elements
;;
(defcustom tabkit-show-tab-icons t
  "Non-nil means to try to display tab icons."
  :type 'boolean
  :set #'tabkit--custom-set-and-refresh-all)

(defsubst tabkit-space-ascent-center (height)
  "Compute ascent center based on display space HEIGHT.
Either return (:ascent (PIXELS)) or nil if HEIGHT is less than the
height of the tabkit-default face, or `font-info' is unsuccessful.
Assume a graphic display, required by tabkit."
  (when-let* ((ac (+-face-ascent-center 'tabkit-default height)))
    `(:ascent (,ac))))

(defconst tabkit-space-default-width 5
  "Default space width in pixels used around a tab label.")

(defun tabkit-space (&optional width height face)
  "Return display space of WIDTH pixels.
If WIDTH is nil, it defaults to `tabkit-space-default-width'.  If
HEIGHT is non nil, space will be of the specified HEIGHT and centered
ascent.  Optional argument FACE is a face applied to the returned
space, by default the `tabkit-default' face."
  (let* ((face   (and face (list face)))
         (width  (or width tabkit-space-default-width))
         (ascent (and height (tabkit-space-ascent-center height)))
         (height (and height `(:height (,height)))))
    (propertize " "
                :space t
                'display `(space :width (,width) ,@height ,@ascent)
                'rear-nonsticky t
                'face    `(,@face tabkit-default tabkit--base))))

(defun tabkit-text (text &optional face props)
  "Return a copy of TEXT suitable for a tabkit visual component.
That is, add optional additional text properties PROPS, then append
the tabkit default faces, then add optional FACE to TEXT.  Any already
existing TEXT face and properties are preserved."
  (let* ((txt (apply #'propertize text 'rear-nonsticky t props))
         (l (length txt)))
    (add-face-text-property 0 l '(tabkit-default tabkit--base) t txt)
    (and face (add-face-text-property 0 l face nil txt))
    txt))

(defmacro tabkit-text-fit (text width &optional align shrink ellipsis)
  "From string TEXT return a new string that fits in pixels WIDTH.
The returned text is truncated or padded with space to be of pixels
WIDTH.
See also the function `+-text-fit' for the meaning of arguments ALIGN,
SHRINK and ELLIPSIS."
 `(+-text-fit ,text ,width ,align ,shrink ,ellipsis 'tabkit-default))

(defconst tabkit--text-join-barrier (tabkit-space 0))

(defun tabkit-text-join (&rest texts)
  "Joins all TEXTS while isolating each text's own properties."
  (mapconcat #'identity texts tabkit--text-join-barrier))

(cl-defgeneric tabkit--icon-displayable-glyph (glyph-spec &rest args)
  "Return first displayable glyph from GLYPH-SPEC.
Call `pic-icon-displayable-glyph' with  GLYPH-SPEC and ARGS."
  (apply #'pic-icon-displayable-glyph glyph-spec args))

(cl-defmethod tabkit--icon-displayable-glyph ((glyph-spec (head :image))
                                              &rest image-props)
  "Return first displayable glyph from :image GLYPH-SPEC.
IMAGE-PROPS are image properties applied to the image."
  (message "tabkit--icon-displayable-glyph spec(%S) props(%S)"
           glyph-spec image-props)
  (if-let* ((image (tabkit-find-image (cadr glyph-spec) image-props)))
      (apply #'propertize " " 'display image (cddr glyph-spec))))

(defmacro tabkit-icon (name &optional parent &rest image-props)
  "Return the displayable glyph of icon NAME.
PARENT is the parent icon when name is an alist of icon glyphs.
IMAGE-PROPS are image properties applied to image icon glyph."
  `(pic-icon (pic-icon-name ,name ,parent)
             nil nil
             #'tabkit--icon-displayable-glyph
             ,@image-props))

;;; Basic text rendering
;;
(defface tabkit--text nil
  "Base face of rendered text.")

(defface tabkit-text-desc
  '((t
     (:height 0.9 :weight light)
     ))
  "Face of description text, between <d>...</d> tags.
Inactive in tooltips if `use-system-tooltips' is non-nil.")

(defface tabkit-text-key0
  '((t
     (:weight bold)
     ))
  "Face of level 0 key text, between <k0>...</k0> tags.
Inactive in tooltips if `use-system-tooltips' is non-nil.")

(defface tabkit-text-key1
  '((t
     (:weight semibold)
     ))
  "Face of level 1 key text between <k1>...</k1> tags.
Inactive in tooltips if `use-system-tooltips' is non-nil.")

(defface tabkit-text-key2
  '((t
     (:weight book)
     ))
  "Face of level 2 key text between <k2>...</k2> tags.
Inactive in tooltips if `use-system-tooltips' is non-nil.")

(defun tabkit--text-render-desc (dom)
  "Render <d> tag specified in DOM.
This markup is intended for additional description starting on a
newline and fontified with the `tabkit-text-desc' face."
  (shr-ensure-newline)
  (shr-fontize-dom dom 'tabkit-text-desc))

(defun tabkit--text-render-key0 (dom)
  "Render <k0> tag specified in DOM.
This markup is intended for level 0 key text fontified with the
`tabkit-text-key0' face."
  (shr-fontize-dom dom 'tabkit-text-key0))

(defun tabkit--text-render-key1 (dom)
  "Render <k1> tag specified in DOM.
This markup is intended for level 1 key text fontified with the
`tabkit-text-key1' face."
  (shr-fontize-dom dom 'tabkit-text-key1))

(defun tabkit--text-render-key2 (dom)
  "Render <k2> tag specified in DOM.
This markup is intended for level 2 key text fontified with the
`tabkit-text-key2' face."
  (shr-fontize-dom dom 'tabkit-text-key2))

(defvar tabkit--text-rendering-functions
  '(
    (d  . tabkit--text-render-desc)
    (k0 . tabkit--text-render-key0)
    (k1 . tabkit--text-render-key1)
    (k2 . tabkit--text-render-key2)
    )
  "Alist of tag/function pairs used to render help-echo markup.")

(cl-defgeneric tabkit--text-render (_type text)
  "Render TEXT according to markup.
Default method that ignores markup and render TEXT as plain text."
  (let ((case-fold-search t))
    (setq
     ;; Collapse extra whitespace.
     text (string-clean-whitespace text)
     ;; Replace tags that break line by newline.
     text (replace-regexp-in-string "<\\(br\\|[dp]\\)[^>]*>" "\n" text)
     ;; Remove all tags (all forms like <xxxxx>).
     text (replace-regexp-in-string "<[^>]*>" "" text)
     ;; Remove blank lines.
     text (replace-regexp-in-string "^[[:blank:]]*\n" "" text)
     ;; Remove whitespace at the beginning of lines.
     text (replace-regexp-in-string "^[[:blank:]]*" "" text)
     ;; Remove whitespace at the end of lines.
     text (replace-regexp-in-string "[[:blank:]]*$" "" text)
     )))

(cl-defmethod tabkit--text-render ((_type (eql 'html)) text)
  "Render TEXT according to markup.
Method that renders HTML tags when Emacs is compiled with libxml2."
  (let ((shr-external-rendering-functions
         tabkit--text-rendering-functions))
    (with-work-buffer
      (insert text)
      (let ((dom (libxml-parse-html-region (point-min) (point-max)))
            ;; Use an empty base face that will not override
            ;; attributes of tags faces.
            (shr-current-font 'tabkit--text))
        (erase-buffer)
        (shr-insert-document dom))
      ;; Remove any extra blank and newline at the end.
      (goto-char (point-max))
      (and (re-search-backward "[^[:blank:]\n]" nil t)
           (not (eobp))
           (delete-region (1+ (point)) (point-max)))
      (buffer-string))))

(defun tabkit-text-render (&rest objects)
  "Render a string out of OBJECTS.
OBJECTS are passed to `format', and the first object must be a string.
If Emacs is compiled with libxml2, render markup found in the
formatted string, otherwise clean markup and whitespace, and insert
needed newlines."
  (tabkit--text-render
   (and (fboundp 'libxml-parse-html-region) 'html)
   (apply #'format objects)))

;;; Internal data structures
;;
(defvar tabkit--tabsets nil
  "The tabsets store.")

(defvar tabkit--meta-tabset nil
  "The special tabset of existing tabsets.")

(defvar tabkit--pinned-tabs nil
  "List of currently pinned tabs.")

;;; Core tabset and tab functions
;;
(define-inline tabkit-meta-tabset-p (tabset)
  "Return non-nil if TABSET is the tabset of all selected tabs."
  (inline-quote (eq ,tabset tabkit--meta-tabset)))

(define-inline tabkit-tabset (name)
  "Return the tabset whose name is the string NAME.
Return nil if not found."
  (inline-quote (intern-soft ,name tabkit--tabsets)))

(defalias 'tabkit-tabset-name #'symbol-name
  "Return the TABSET name as a string.")

(defalias 'tabkit-global-tabs #'symbol-value
  "Return the global value of TABSET's list of tabs.")

(define-inline tabkit-make-tab (object tabset)
  "Return a new tab with value OBJECT.
TABSET is the tabset the tab belongs to."
  (inline-quote (list ,object ,tabset)))

(defalias 'tabkit-tab-value #'car
  "Return the value of tab TAB.")

(defalias 'tabkit-tab-tabset #'cadr
  "Return the tabset TAB belongs to.")

(defun tabkit-tab-p (object)
  "Return non-nil if OBJECT is a tabkit tab."
  (let ((ts (car-safe (cdr-safe object))))
    (and ts (symbolp ts)
         (eq ts (tabkit-tabset (symbol-name ts))))))

(defconst tabkit--tab-all-types
  '(tabkit-buffer-tab tabkit--buffer-tab tabkit-tab tabkit--tab)
  "List tabkit tab types from the most specific to least specific.")

(defconst tabkit--tab-types-inheritance
  '((tabkit-buffer-tab tabkit--buffer-tab tabkit-tab tabkit--tab)
    ;; (tabkit--buffer-tab tabkit-tab tabkit--tab)
    (tabkit-tab tabkit--tab)
    ;; (tabkit--tab)
    )
  "List tabkit tab types inheritance.")

(defun tabkit-tab-type (object)
  "Return the tab type of OBJECT, or nil if not a tabkit tab."
  (and (tabkit-tab-p object)
       (if (bufferp (tabkit-tab-value object))
           'tabkit-buffer-tab
         'tabkit-tab)))

;; (defun tabkit-buffer-tab-p (object)
;;   "Return non-nil if OBJECT is a tabkit buffer tab."
;;   (eq (tabkit-tab-type object) 'tabkit-buffer-tab))

(cl-generic-define-generalizer tabkit--tab-type-generalizer
  10
  (lambda (name &rest _) `(tabkit-tab-type ,name))
  (lambda (tag  &rest _) (assq tag tabkit--tab-types-inheritance)))

(cl-defmethod cl-generic-generalizers :extra "tabkit-tab-type" (type)
  "Support for dispatch on tabkit tab type."
  (or (and (memq type tabkit--tab-all-types)
           (list tabkit--tab-type-generalizer))
      (cl-call-next-method)))

(defun tabkit--tab-put-property (tab prop value)
  "In TAB properties, change value of PROP to VALUE.
If VALUE is nil, remove PROP from TAB properties."
  (let ((tail (cdr tab)))
    (setcdr tail (if value
                     (plist-put (cdr tail) prop value)
                   (+-plist-delq (cdr tail) prop)))))

(define-inline tabkit-tab-property (tab prop)
  "Return value of TAB property PROP.
Value can be set with (setf (tabkit-tab-property TAB PROP) VALUE)."
  (declare (gv-setter tabkit--tab-put-property))
  (inline-quote (plist-get (cddr ,tab) ,prop)))

(defmacro tabkit-with-stashed-tab-property (tab prop &rest code)
"Return the value of CODE and stash it in TAB property TAB.
If the value of TAB property PROP is non-nil, return it without
evaluating CODE."
  (declare (indent 2))
  `(with-memoization (tabkit-tab-property ,tab ,prop)
     ,@code))

(define-inline tabkit-tab-pinned-p (tab)
  "Return non-nil if TAB is pinned."
  (inline-quote (if (memq ,tab tabkit--pinned-tabs) t)))

(defun tabkit-tab-pin (tab)
  "Pin TAB."
  (or (tabkit-tab-pinned-p tab)
      (push tab tabkit--pinned-tabs)))

(defun tabkit-tab-unpin (tab)
  "Unpin TAB."
  (and (tabkit-tab-pinned-p tab)
       (setq tabkit--pinned-tabs (delq tab tabkit--pinned-tabs))))

(define-inline tabkit-tab (object tabset)
  "Search for a tab with value OBJECT in TABSET.
Return the tab found, or nil if not found."
  (inline-quote (assoc ,object (tabkit-global-tabs ,tabset))))

(defun tabkit-tab-member (tab tabset)
  "Return non-nil if TAB is in TABSET."
  (or (eq (tabkit-tab-tabset tab) tabset)
      ;; Case TABSET is tabkit--meta-tabset.
      (if (tabkit-meta-tabset-p tabset)
          (memq tab (tabkit-global-tabs tabset)))))

(defun tabkit-add-tab (tabset-name object)
  "Add to TABSET-NAME a tab with value OBJECT, if not already present.
New tab is added after all other already present tabs.  Return the
tab."
  (let ((tabset (tabkit-tabset tabset-name)))
    (unless tabset
      (setq tabset (intern tabset-name tabkit--tabsets))
      (set tabset nil)
      (put tabkit--meta-tabset :valid nil))
    (let ((tab (tabkit-tab object tabset)))
      (unless tab
        (setq tab (tabkit-make-tab object tabset))
        (set tabset (nconc (tabkit-global-tabs tabset) (list tab))))
      tab)))

(defmacro tabkit-walk-tabsets (function)
  "Call FUNCTION on every tabset."
  `(mapatoms ,function tabkit--tabsets))

(defun tabkit-map-tabsets (function)
  "Call FUNCTION on every tabset and make a list of the results.
The result is a list just as long as the number of existing tabsets."
  (let (result)
    (tabkit-walk-tabsets
     (lambda (tabset)
       (push (funcall function tabset) result)))
    result))

(defun tabkit-tab-at-pos (pos string)
  "Return the tab object at POS in STRING, or nil if not a tab."
  (let ((tab (get-text-property pos :tab string)))
    (if (tabkit-tab-p tab) tab)))

(defun tabkit-posn-tab (position)
  "Return the tab object of POSITION, or nil if not a tab.
POSITION should be a list of the form returned by the `event-start'
and `event-end' functions."
  (let ((target (posn-string position)))
    (if target (tabkit-tab-at-pos (cdr target) (car target)))))

(defun tabkit-visible-tabs ()
  "List of tabs visible on the tabkit line of the selected window."
  (let* ((line (tabkit-line))
         (pos 0)
         tab tabs)
    (while (setq pos (next-single-property-change pos :tab line))
      (setq pos (1+ pos))
      (setq tab (tabkit-tab-at-pos pos line))
      (if tab (push tab tabs)))
    (nreverse tabs)))

(defun tabkit-current-tabs ()
  "List of all tabs, visible or not, displayed in the selected window.
List tabs from both current tabset and pinned tabs, in display order."
  (let* ((tabset (tabkit-current-tabset))
         (tabs   (tabkit-local-tabs tabset)))
    (if (tabkit-meta-tabset-p tabset)
        tabs
      (let* ((vtabs (tabkit-visible-tabs))
             ;; If a pinned tab is visible, include pinned tabs.
             (ptabs (if (seq-find #'tabkit-tab-pinned-p vtabs)
                        tabkit--pinned-tabs)))
        ;; Preserve display order of pinned and current tabset tabs.
        (if (tabkit-tab-pinned-p (car vtabs))
            (seq-union (reverse ptabs) tabs)
          (seq-union tabs (reverse ptabs)))))))

;;; Window local states
;;
(defun tabkit--tabset-set-prop (tabset prop value &optional window)
  "Set to VALUE the TABSET window local property PROP.
WINDOW can be any window and defaults to the selected one."
  (let* ((alist (tabkit-window-local :views window))
         (plist (alist-get tabset alist)))
       ;; (setf (plist-get plist prop) value)
       ;; (setf (alist-get tabset alist) plist)
       (setf (alist-get tabset alist) (plist-put plist prop value))
       (setf (tabkit-window-local :views window) alist)
       value))

(define-inline tabkit-tabset-prop (tabset prop &optional window)
  "Get value of the TABSET window local property PROP.
WINDOW can be any window and defaults to the selected one.
Value can be set with (setf (tabkit-tabset-prop TABSET PROP) VALUE)."
  (inline-quote
   (plist-get (alist-get ,tabset (tabkit-window-local :views ,window))
              ,prop)))

(gv-define-setter tabkit-tabset-prop (value tabset prop &optional window)
  `(tabkit--tabset-set-prop ,tabset ,prop ,value ,window))

(defun tabkit-tabs-around (tab tabs)
  "Return tabs before and after TAB in TABS.
Return a pair (BEFORE-TABS . AFTER-TABS) where BEFORE-TABS and
AFTER-TABS are the list tabs respectively before and after TAB, or nil
if there is no such tab.  Tabs are ordered by increasing distance from
TAB: (car BEFORE-TABS) is the tab just before TAB and (car AFTER-TABS)
the one just after."
  (let (previous)
    (while (and tabs (not (eq tab (car tabs))))
      (push (car tabs) previous)
      (setq tabs (cdr tabs)))
    (cons previous (cdr tabs))))

(defun tabkit-tab-around (tab tabs)
  "Return the tab before and the tab after TAB in TABS.
Return a pair (BEFORE-TAB . AFTER-TAB) where BEFORE-TAB and AFTER-TAB
are the tabs respectively before and after TAB, or nil if there is no
such tab."
  (let ((around (tabkit-tabs-around tab tabs)))
    (cons (caar around) (cadr around))))

(defun tabkit-tab-next (tabset tab &optional before)
  "Search in TABSET for the tab after TAB.
If optional argument BEFORE is non-nil, search for the tab before TAB.
Return the tab found, or nil otherwise."
  (let ((around (tabkit-tab-around tab (tabkit-local-tabs tabset))))
    (if before (car around) (cdr around))))

(defmacro tabkit-last-tabset ()
  "Return the last selected tabset in selected window.
Value can be set with (setf (tabkit-last-tabset) VALUE)."
  `(tabkit-window-local :last-tabset))

(gv-define-setter tabkit-last-tabset (value)
  `(setf (tabkit-window-local :last-tabset) ,value))

(defmacro tabkit-dragged-tab ()
  "Return the tab dragged in selected window or nil if none.
Value can be set with (setf (tabkit-dragged-tab) VALUE)."
  `(tabkit-window-local :dragged))

(gv-define-setter tabkit-dragged-tab (value)
  `(setf (tabkit-window-local :dragged) ,value))

(define-inline tabkit-tab-dragged-p (tab)
  "Return non-nil if TAB is currently dragged in selected window."
  (inline-quote (eq ,tab (tabkit-dragged-tab))))

(defmacro tabkit-tabset-hscroll (tabset)
  "Return non-nil if TABSET hscroll is set in selected window."
  `(tabkit-tabset-prop ,tabset :hscroll))

(defmacro tabkit-tabset-set-hscroll (tabset flag)
  "In current window, set TABSET hscroll to FLAG."
  `(setf (tabkit-tabset-prop ,tabset :hscroll) ,flag))

(define-inline tabkit-tabset-start (tabset)
  "Return the index of the first visible tab in TABSET."
  (inline-quote (or (tabkit-tabset-prop ,tabset :start) 0)))

(define-inline tabkit-tabset-set-start (tabset start)
  "Set to START the index of the first visible tab in TABSET."
  (inline-quote (setf (tabkit-tabset-prop ,tabset :start) ,start)))

(define-inline tabkit-selected-tab (tabset)
  "Return the tab selected in TABSET."
  (inline-letevals (tabset)
    (inline-quote
     (or (tabkit-tabset-prop ,tabset :selected)
         ;; By default select the first tab.
         (car (tabkit-global-tabs ,tabset))))))

(define-inline tabkit-tab-selected-p (tab tabset)
  "Return non-nil if TAB is the selected tab in TABSET."
  (inline-quote (eq ,tab (tabkit-selected-tab ,tabset))))

(defun tabkit-select-tab (tab tabset)
  "Make TAB the selected tab in TABSET.
Does nothing if TAB is not found in TABSET.
Return TAB if selected, nil if not."
  (if (tabkit-tab-member tab tabset)
      (let ((selected (tabkit-tab-selected-p tab tabset)))
        (or (eq 'force (tabkit-tabset-hscroll tabset))
            (tabkit-tabset-set-hscroll tabset (not selected)))
        ;; If another tab is selected in TABSET, and TABSET is not the
        ;; meta tabset, mark the meta tabset to be refreshed.
        (unless selected
          (setf (tabkit-tabset-prop tabset :selected) tab)
          (or (tabkit-meta-tabset-p tabset)
              (put tabkit--meta-tabset :valid nil)))
        tab)))

(define-inline tabkit-select-tab-value (object tabset)
  "Make the tab with value OBJECT, the selected tab in TABSET.
Does nothing if a tab with value OBJECT is not found in TABSET.
Return the tab selected, or nil if nothing was selected."
  (inline-letevals (tabset)
    (inline-quote
     (tabkit-select-tab (tabkit-tab ,object ,tabset) ,tabset))))

(define-inline tabkit-tabset-selected-p (tabset)
  "Return non-nil if TABSET is selected in the selected window.
That is, if the selected tab belongs to TABSET."
  (inline-quote
   (eq ,tabset
       (tabkit-tab-tabset
        (tabkit-selected-tab (tabkit-current-tabset))))))

(defun tabkit-delete-tabset (tabset)
  "Remove TABSET from the tabsets store."
  (+-with-all-windows
   ;; Also remove the tabset from window local views
   (let ((views  (tabkit-window-local :views)))
     (setf (alist-get tabset views nil 'remove) nil)
     (setf (tabkit-window-local :views) views)))
  (unintern tabset tabkit--tabsets)
  (put tabkit--meta-tabset :valid nil))

(defun tabkit-delete-tab (tab)
  "Delete TAB."
  (tabkit-tab-unpin tab)
  (tabkit-group-tab-unmark tab)
  (let* ((tabset (tabkit-tab-tabset tab))
         (tabs   (tabkit-global-tabs tabset)))
    (if (null (cdr tabs))
        ;; TAB is the last tab in the tabset: remove the tabset.
        ;; - (cl-assert (eq tab (car tabs))) -
        (tabkit-delete-tabset tabset)
      ;; More than 1 tab remain: in all windows where TAB is selected,
      ;; select the tab displayed after (or before) TAB, then remove
      ;; TAB from tabset.
      (+-with-all-windows
        (when-let* ((_ (tabkit-tab-selected-p tab tabset))
                    (around (tabkit-tab-around
                             tab
                             ;; Synchronize local tabs with global
                             ;; data that may have changed during a
                             ;; succession of tab deletions without a
                             ;; redisplay to update window local data.
                             (tabkit--sync-local-tabs tabset)))
                    (next (or (cdr around) (car around))))
          (tabkit-tab-select next)))
      (set tabset (delq tab tabs)))))

(defun tabkit-tabset-view (tabset)
  "Return TABSET scrollable tab list.
That is the list of tabs that are not pinned."
  (if (and tabkit--pinned-tabs (not (tabkit-meta-tabset-p tabset)))
      (seq-difference (tabkit-local-tabs tabset)
                      tabkit--pinned-tabs #'eq)
    (tabkit-local-tabs tabset)))

(defun tabkit-tabset-scroll (tabset count)
  "Scroll the scrollable tabs in TABSET of COUNT units.
If COUNT is positive, move the view on right (scroll left).
If COUNT is negative, move the view on left (scroll right).
Return the index of the leftmost visible tab, or nil if unchanged."
  (setf (tabkit-tabset-prop tabset :scroll) t)
  (let* ((start (and (< count 0) (tabkit-tabset-prop tabset :start0)))
         (from (tabkit-tabset-start tabset))
         (to (min (max 0 (or start (+ from count)))
                  (1- (length (tabkit-tabset-view tabset))))))
    (if (or start (/= from to))
        (tabkit-tabset-set-start tabset to))))

;; Group tabs meta-tabset
;;
(defcustom tabkit-group-order nil
  "Display order of group tabs.
This is a list of group names whose order guides the display order of
group tabs.
The `instant-groups' symbol can also be put in this list to indicate
where are displayed instant groups tabs.  By default, they are
displayed before other group tabs, last modified first.  To order them
differently, put instant group names in this list, before the symbol
`instant-groups', if present.
Other groups not mentioned are displayed in alphabetic order."
  :type '(repeat :tag "Group names in display order"
                 (choice (string :tag "Group name")
                         (const instant-groups))))

(defvar tabkit-default-group-names nil
  "List of known default group names.")

(defun tabkit--group-order ()
  "Return the display order of group tabs."
  (let* ((og tabkit-group-order)
         (ig (tabkit-instant-group-names))
         (ip (seq-position og 'instant-groups)))
    (if ip
        ;; Put instant groups at specified location.
        (append (take ip og) ig (nthcdr (1+ ip) og))
      ;; Put instant groups before groups ordered by default.
      (append og ig))))

(defun tabkit--order-tabs (tabs)
  "Return TABS sorted according to `tabkit-group-order'."
  (sort tabs
        (let ((gl (tabkit--group-order)))
          (lambda (tab1 tab2)
            (let* ((g1 (tabkit-tab-group-name tab1))
                   (g2 (tabkit-tab-group-name tab2))
                   (p1 (length (member g1 gl)))
                   (p2 (length (member g2 gl))))
              (if (and (> p1 0) (> p2 0))
                  (> p1 p2)
                (or (> p1 0)
                    (if (= p2 0)
                        (string-collate-lessp g1 g2)))))))))

(define-inline tabkit-group-tabs ()
  "Return a fresh list of group tabs.
Tabs are sorted according to `tabkit-group-order'."
  (inline-quote
   (tabkit--order-tabs (tabkit-map-tabsets #'tabkit-selected-tab))))

(defun tabkit-meta-tabset ()
  "Return the transient tabset of all selected tabs."
  ;; Refresh window local tabs if needed.
  (tabkit-local-tabs tabkit--meta-tabset)
  tabkit--meta-tabset)

;;; Keep displayed window local tabs in sync. with global tabs
;;
(defun tabkit--group-names (tabs)
   "Return the list of TABS group names.
Ignore any invalid tab in TABS."
  (let (names)
    (dolist (tab tabs)
      (if (tabkit-tab-p tab)
          (push (tabkit-tab-group-name tab) names)))
    (nreverse names)))

(defun tabkit--sync-local-group-tabs (window)
  "Sync local value of group tabs in TABSET with global value.
WINDOW can be any window."
  (let ((gtabs (tabkit-group-tabs))
        (ltabs (tabkit-tabset-prop tabkit--meta-tabset :tabs window)))
    (set tabkit--meta-tabset gtabs)
    (put tabkit--meta-tabset :valid window)
    (setf (tabkit-tabset-prop tabkit--meta-tabset :tabs window)
          (if ltabs
              ;; Sorts the refreshed group tabs according to the order
              ;; of the current local tabs, so that the display order
              ;; is preserved as much as possible. Be careful, at this
              ;; stage, invalid local tabs may still be present in L1,
              ;; before later deletion. The `tabkit--group-names'
              ;; function takes care of ignoring such invalid tabs.
              (sort (copy-sequence gtabs)
                    (let ((gl (tabkit--group-names ltabs)))
                      (lambda (tab1 tab2)
                        (let* ((g1 (tabkit-tab-group-name tab1))
                               (g2 (tabkit-tab-group-name tab2))
                               (p1 (length (member g1 gl)))
                               (p2 (length (member g2 gl))))
                          (if (and (> p1 0) (> p2 0))
                              (> p1 p2)
                            (> p1 0))))))
            gtabs))))

(defun tabkit--sync-local-tabs (tabset &optional window)
  "Sync local value of non-group tabs in TABSET with global value.
WINDOW can be any window and defaults to the selected one."
  (let ((l1 (tabkit-tabset-prop tabset :tabs window))
        (l2 (tabkit-global-tabs tabset)))
    (setf (tabkit-tabset-prop tabset :tabs window)
          (if l1
              (let (sync)
                (dolist (e l1) (if (memq e l2) (push e sync)))
                (dolist (e l2) (or (memq e l1) (push e sync)))
                (nreverse sync))
            (copy-sequence l2)))))

;;; Current tabset and tabs displayed on the tabkit line
;;
(defun tabkit--get-previous-window (&optional window)
  "Get a tabkit window before WINDOW in cyclic ordering of windows.
That is a window which have tabkit local data setup.
Return the window found or WINDOW otherwise.
WINDOW can be any window and defaults to the selected one."
  (catch 'found
    (dolist (w (cdr (nreverse
                     (window-list-1 (next-window window 0 0) 0 0))))
      (if (tabkit-window-local :visited w)
          (throw 'found w)))
    window))

(defun tabkit-local-tabs (tabset &optional window)
  "Return the list of tabs in TABSET local to WINDOW.
WINDOW can be any window and defaults to the selected one."
  (let ((this-w (window-normalize-window window)))
    ;; Set local data of new window from that of the previous window,
    ;; if any.
    (or (tabkit-window-local :visited this-w)
        (let ((prev-w (tabkit--get-previous-window this-w)))
          (or (eq prev-w this-w)
              (setf (tabkit-window-local-bindings this-w)
                    (tabkit--window-local-clone prev-w)))
          (setf (tabkit-window-local :visited this-w) t)))
    ;; Return local tabs, if any, or those created from global data.
    (if (tabkit-meta-tabset-p tabset)
        (or (if (eq this-w (get tabkit--meta-tabset :valid))
                (tabkit-tabset-prop tabset :tabs this-w))
            (tabkit--sync-local-group-tabs this-w))
      (or (tabkit-tabset-prop tabset :tabs this-w)
          (tabkit--sync-local-tabs tabset this-w)))))

(defun tabkit-current-tabset (&optional update)
  "Return the tabset displayed on the tabkit line of selected window.
If optional argument UPDATE is non-nil, or if no tabset is associated
with the selected window, call the function specified by
`tabkit-current-tabset-function' to update the current tabset."
  (let ((tabset (unless update (tabkit-window-local :this-tabset))))
    (if tabset
        ;; Refresh window local tabs if needed.
        (tabkit-local-tabs tabset)
      ;; Refresh the current tabset and the meta tabset.  The function
      ;; specified in `tabkit-current-tabset-function' is responsible
      ;; to invalidate the meta tabset when needed.
      (setq tabset (funcall tabkit-current-tabset-function))
      (if (tabkit-showing-groups)
          (let ((seltab (tabkit-selected-tab tabset)))
            ;; This will refresh the meta tabset if needed.
            (setq tabset (tabkit-meta-tabset))
            (tabkit-select-tab seltab tabset)))
      (setf (tabkit-window-local :this-tabset) tabset))
    tabset))

;;; Setup of tabs global data
;;
(defvar tabkit--objects nil)
(defvar tabkit--groups  nil)

(defun tabkit-tabs-data-refresh ()
  "Refresh global tabs data."
  (run-hooks 'tabkit-tabs-data-cleanup-hook)
  (setq tabkit--objects nil tabkit--groups nil)
  (tabkit-display-update))

(defun tabkit--tabs-data-1 (objects grouping &optional testfn)
  "Setup the global tabs data from OBJECTS.
See `tabkit-tabs-data' for details about GROUPING and TESTFN."
  (setq tabkit--objects objects)
  (let (wl)
    ;; In wl, compute new alist of (object group-name1 ...) elts.
    (if tabkit-use-groups
        (dolist (o tabkit--objects)
          (let ((gl (funcall grouping o)))
            (push (cons o gl) wl)))
      (dolist (o tabkit--objects)
        (push (list o tabkit--catch-all-group-name) wl)))
    ;; Keep order of objects list.
    (setq wl (nreverse wl))
    ;; Update tabsets according to groups in wl, and transform wl (by
    ;; side effect) from an alist of (object group-name1 ...)  elts to
    ;; an alist of (object tabset1 ...) elts.
    (dolist (elt wl)
      (let ((tl nil))
        (dolist (group (cdr elt))
          (let ((tab (tabkit-add-tab group (car elt))))
            (tabkit-closed-tab-delete tab) ;; Clear closed tabs
            (push (tabkit-tab-tabset tab) tl)))
        ;; In wl, replace the list of group names by list of
        ;; corresponding tabsets ordered like groups criteria.
        (setcdr elt (nreverse tl))))
    ;; Remove orphan tabs that remain in tabsets.  That is, either
    ;; tabs having an orphan value (no more in the `tabkit--object'
    ;; list); or tabs that do not belong to one of their current
    ;; parent groups (tabs moved to other groups).
    (save-window-excursion
      (tabkit-walk-tabsets
       (lambda (tabset)
         (let ((afun (if testfn #'assoc #'assq)))
           (dolist (tab (tabkit-global-tabs tabset))
             (let ((elt (funcall afun (tabkit-tab-value tab) wl)))
               (unless (and elt (memq tabset (cdr elt)))
                 ;; (display-warning
                 ;;  'tabkit-update-tabs
                 ;;  (if ,elt
                 ;;      (format "delete-tab %s (object moved to %s)"
                 ;;              ,tab ,elt)
                 ;;    (format "delete-tab %s" ,tab))
                 ;;  :debug (messages-buffer))
                 (tabkit-delete-tab tab))))))))
    ;; Cache the newly computed alist of (object tabset1 ...) elts.
    (setq tabkit--groups wl)
    ;; Mark tabkit line to be refreshed on all windows.
    (tabkit-line-expired)))

(defun tabkit-tabs-data (objects grouping &optional select testfn)
  "Setup the global tabs data from OBJECTS.
OBJECTS is a list of Lisp objects.

GROUPING is a function that associates objects to groups.  It is
passed an element of OBJECTS and must return a list of group names
this element belongs to.

Optional argument SELECT is an element of OBJECTS that selects a
group.  It defaults to the first element of OBJECTS.  The selected
group is one of the groups SELECT belongs to, otherwise the first
built group.  When SELECT belongs to several groups, if one of them
was last selected it is selected again, otherwise the first group
SELECT belongs to is selected.

If TESTFN is non-nil, equality of elements is tested by `equal',
otherwise by `eq'.

Return the tabset for the selected group."
  ;; When the object list didn't change, assume that tab groups don't
  ;; need to be recomputed, and just reuse cached data.
  (or (tabkit--list-same-p objects tabkit--objects testfn)
      (tabkit--tabs-data-1 objects grouping testfn))
  ;; Return the selected tabset taking care that objects might belong
  ;; to several groups: if the SELECT object belongs to the last
  ;; selected tabset, return this tabset; else, return the first
  ;; tabset the SELECT object belongs to, otherwise return the first
  ;; tabset.
  (let* ((tabsets (alist-get (or select (car tabkit--objects))
                             tabkit--groups
                             (cdar tabkit--groups)
                             testfn))
         (last-tabset (tabkit-last-tabset)))
    (if (memq last-tabset tabsets)
        last-tabset
      (setf (tabkit-last-tabset) (car tabsets)))))

;;; Instant tab colors place
;;
(defvar tabkit--instant-color-tab-criteria nil)
(defvar tabkit--instant-color-group-criteria nil)

(defmacro tabkit--instant-color-place (tab &optional action)
  "Return an `alist-get' place of TAB instant color.
TAB is either a basic tab, or a currently displayed group tab.
Optional argument ACTION specifies how to use the place:

- `:del', the place is intended to remove the instant color of TAB
  with (setf (tabkit--instant-color-place TAB :clear) nil).

- `:set', the place is intended to set the instant color of TAB
  with (setf (tabkit--instant-color-place TAB :set) VALUE).

- Otherwise, the place retrieve the instant color of TAB with
  (tabkit--instant-color-place TAB [NO-INHERIT]).  If TAB does not
  have an instant color and is not a group TAB, return the instant
  color of its parent group, unless the NO-INHERIT sexp eval to
  non-nil."
  (let* ((tb (make-symbol "tb"))
         (gn (make-symbol "gn"))
         (rm (eq action :del))
         ;; Instant color of the currently displayed group tab.
         (gx `(alist-get ,gn
                         tabkit--instant-color-group-criteria
                         nil ,rm #'string-equal))
         ;; Instant color of TAB.
         (tx `(alist-get (tabkit-tab-unique-name ,tb)
                         tabkit--instant-color-tab-criteria
                         nil ,rm #'tabkit-name-equal))
         ;; Instant color of the parent group of TAB.
         (px `(alist-get  (tabkit-tab-group-name, tb)
                          tabkit--instant-color-group-criteria
                          nil ,rm #'string-equal)))
    `(let* ((,tb ,tab)
            (,gn (tabkit-tab-showing-group ,tb)))
       (if ,gn
           ,gx
         ,(if (memq action '(:del :set))
              tx
            `(or ,tx (unless ,action ,px)))))))

;;; Tab generic functions and methods
;;
(cl-defmethod tabkit-tab-name ((tab tabkit--tab))
  "Return name of TAB."
  (format "%s" (tabkit-tab-value tab)))

(cl-defmethod tabkit-tab-name ((tab tabkit--buffer-tab))
  "Return name of buffer TAB.
Return the name of TAB buffer."
  (buffer-name (tabkit-tab-value tab)))

(cl-defgeneric tabkit-tab-unique-name (tab)
  "Return TAB unique name."
  (tabkit-tab-name tab))

(cl-defmethod tabkit-tab-unique-name ((tab tabkit--buffer-tab))
  "Return buffer TAB unique name."
  (tabkit--buffer-unique-name (tabkit-tab-value tab)))


(cl-defgeneric tabkit-tab-display-name (tab)
  "Return name of TAB currently displayed on the tabkit line."
  (or (tabkit-tab-showing-group tab)
      (tabkit-tab-name tab)))

(cl-defgeneric tabkit-tab-name-help (tab)
  "Return a help-echo string for TAB name.
Return a string with help-echo markup."
  (format "<k0>%s</k0>" (tabkit-tab-display-name tab)))

(cl-defmethod tabkit-tab-name-help ((tab tabkit--buffer-tab))
  "Return a help-echo string for buffer TAB name.
Return a string with help-echo markup.  If buffer TAB is visiting a
file, the help text is the filename passed to the function
`abbreviate-file-name'; the default tab help text otherwise."
  (if-let* ((fn (buffer-file-name (tabkit-tab-value tab))))
      (format "<k0>%s</k0>" (abbreviate-file-name fn))
    (cl-call-next-method)))

(cl-defmethod tabkit-tab-group-name ((tab tabkit--tab))
  "Return a string representation of the group TAB belongs to.

Warning: never alter the returned string!  Doing that might make Emacs
dysfunctional, and might even crash Emacs."
  (tabkit-tabset-name (tabkit-tab-tabset tab)))

(cl-defgeneric tabkit-tab-group-name-help (tab)
  "Return a help-echo string for TAB group name.
That is, a string with help-echo markup."
  (format (if (tabkit-tab-in-default-group-p tab)
              "Group <k0>%s</k0>"
            "Group <k0><i>%s</i></k0>")
          (tabkit-tab-group-name tab)))

(cl-defmethod tabkit-tab-group-name-help ((tab tabkit--buffer-tab))
  "Return a help-echo string for buffer TAB group name.
That is, a string with help-echo markup."
  (if tabkit-use-groups
      (cl-call-next-method)
    (with-current-buffer (tabkit-tab-value tab)
      (format "<k0>%s</k0>" (tabkit--pretty-symbol-name major-mode)))))

(cl-defgeneric tabkit-tab-icon-name (tab)
  "Return an icon name for TAB.
That is, the name of the group TAB belongs to, used to lookup an icon
in `tabkit-tab-icon-custom-icons'."
  (tabkit-tab-group-name tab))

(cl-defmethod tabkit-tab-icon-name ((tab tabkit--buffer-tab))
  "Return an icon name for buffer TAB.
That is, when groups are used, the name of the group TAB belongs to;
otherwise, the first group name for which TAB buffer meets one of the
criteria defined by the option `tabkit-buffer-group-criteria'.  This
name is used to lookup an icon in `tabkit-tab-icon-custom-icons'."
  (if tabkit-use-groups
      (cl-call-next-method)
    (car (tabkit-buffer-groups (tabkit-tab-value tab)))))

(cl-defmethod tabkit-tab-instant-color ((tab tabkit--tab)
                                        &optional strict)
  "Return TAB instant color name or #RGB triplet or nil if none.
If optional argument STRICT is non-nil, ignore the instant color of
the parent group.  Otherwise, if TAB has no instant color, it inherits
the instant color of its parent group."
  (tabkit--instant-color-place tab strict))

;; Default gv setter for `tabkit-tab-instant-color'.
(cl-defmethod (setf tabkit-tab-instant-color) (color (tab tabkit--tab))
  "Set instant color of TAB to COLOR.
If COLOR is nil, remove instant color, so TAB has default color.
Return non-nil if success, that is, either the instant COLOR set, or t
if instant color is removed."
  (cond
   ((null color)
    (if (tabkit-tab-instant-color tab t)
        (progn
          (setf (tabkit--instant-color-place tab :del) nil)
          (message "Tab color reset to default")
          t)
      (message "Tab color is already set to default")
      nil))
   ((qolor-name-p color)
    (setf (tabkit--instant-color-place tab :set) color))
   (t
    (message "Wrong color name or #RGB triplet, %S" color)
    nil)))

(cl-defmethod tabkit-tab-default-color ((tab tabkit--tab))
  "Return a default color for TAB.
That is, the background color of the `tabkit-tab-selected' face if
TAB is selected, of the `tabkit-tab-unselected' face otherwise."
  (face-background
   (if (tabkit-tab-selected-p tab (tabkit-current-tabset))
       'tabkit-tab-selected
     'tabkit-tab-unselected)
   nil
   '(tabkit-default default)))

(cl-defmethod tabkit-tab-default-color ((tab tabkit--buffer-tab))
  "Return a default color for buffer TAB.
That is, either a specific color associated to TAB buffer (see option
`tabkit-buffer-color-criteria'), or a common default color."
  (or (tabkit-buffer-color (tabkit-tab-value tab))
      (cl-call-next-method)))

(cl-defgeneric tabkit-tab-color (tab)
  "Return a color for TAB.
That is, either an instant color or default color associated to TAB."
  (or (tabkit-tab-instant-color tab)
      (tabkit-tab-default-color tab)))

(cl-defgeneric tabkit-tab-faces (tab)
  "Return faces for TAB.
That is, a pair (TAB-FACE . [TAB-HIGHLIGHT]) where:

- TAB-FACE is the face applied to TAB.  By default TAB-FACE reflects
  TAB color (see also `tabkit-tab-color'), and TAB selected state, and
  inherits from the `tabkit-tab-selected' or `tabkit-tab-unselected'
  face depending on TAB selected state.

- TAB-HIGHLIGHT is the face to highlight TAB when mouse hovers it, or
  nil if no highlighting.  By default TAB-HIGHLIGHT is deduced from
  TAB color and inherits from the `tabkit-tab-highlight' face.  It is
  nil for the selected TAB."
  (if (tabkit-tab-selected-p tab (tabkit-current-tabset))
      (let* ((bg (tabkit-tab-color tab))
             (rd (qolor-readable bg))
             (fg (if (mode-line-window-selected-p)
                     rd
                   (qolor-gray bg))))
        `(( :inherit tabkit-tab-selected
            :foreground ,fg :background ,bg)
          ;; Don't highlight the selected tab.
          . nil))
    (let* ((hi (tabkit-tab-color tab))
           (bg (qolor-fade hi))
           (fg (qolor-gray bg)))
      `(( :inherit tabkit-tab-unselected
          :foreground ,fg :background ,bg)
        ;; Highlight unselected tab.
        :inherit tabkit-tab-highlight
        :foreground ,(qolor-readable hi) :background ,hi))))

;;; Tab mark
;;
(defvar tabkit--marked-group-tabs nil)

(define-inline tabkit-group-tab-marked-p (tab)
  "Return non-nil if group TAB is marked."
  (inline-quote (if (memq ,tab tabkit--marked-group-tabs) t)))

(defun tabkit-group-tab-mark (tab)
  "Mark group TAB.
Return non-nil if TAB was actually marked."
  (unless (tabkit-group-tab-marked-p tab)
    (push tab tabkit--marked-group-tabs)
    t))

(defun tabkit-group-tab-unmark (tab)
  "Unmark group TAB.
Return non-nil if TAB was actually unmarked"
  (when (tabkit-group-tab-marked-p tab)
    (setq tabkit--marked-group-tabs
          (delq tab tabkit--marked-group-tabs))
    t))

(defun tabkit-tab-marked-p (tab)
  "Return non-nil if TAB is marked."
  (if (tabkit-showing-groups)
      (tabkit-group-tab-marked-p tab)
    (tabkit-tab-property tab :mark)))

(defun tabkit-marked-tabs ()
  "Return a list of all marked tabs on the tabkit line.
That is all marked tabs in currently displayed tabset, plus marked
pinned tabs from other tabsets."
  (let ((tabset (tabkit-current-tabset)))
    (if (tabkit-meta-tabset-p tabset)
        tabkit--marked-group-tabs
      (seq-filter #'tabkit-tab-marked-p
                  (seq-union tabkit--pinned-tabs
                             (tabkit-global-tabs tabset))))))

(defun tabkit-tab-unmark (tab)
  "Unmark TAB.
Return non-nil if TAB was actually unmarked"
  (if (tabkit-showing-groups)
      (tabkit-group-tab-unmark tab)
    (when (tabkit-tab-marked-p tab)
      (setf (tabkit-tab-property tab :mark) nil)
      t)))

(define-inline tabkit-unmark-all-tabs ()
  "Unmark all marked tabs on the tabkit line of selected window."
  (inline-quote
   (mapc #'tabkit-tab-unmark (tabkit-marked-tabs))))

(defun tabkit-tab-mark (tab &optional reset)
  "Mark TAB.
If RESET is non-nil, also unmark all currently marked tabs.
Return non-nil if TAB was actually marked."
  (let ((mark (tabkit-tab-marked-p tab)))
    (if reset (tabkit-unmark-all-tabs))
    (if (tabkit-showing-groups)
        (tabkit-group-tab-mark tab)
      (setf (tabkit-tab-property tab :mark) t))
    (not mark)))

(define-inline tabkit-mark-all-tabs ()
  "Mark all tabs on the tabkit line of selected window."
  (inline-quote
   (mapc #'tabkit-tab-mark (tabkit-current-tabs))))

(defun tabkit-mark-range-from-tab (tab)
  "Mark tabs between TAB and the selected tab.
Return the number of marked tabs."
  (let* ((tabset (tabkit-current-tabset))
         (sel    (tabkit-selected-tab tabset)))
    (if (eq tab sel)
        (progn
          (tabkit-tab-mark tab t)
          1)
      (let* ((tabs (tabkit-current-tabs))
             (n 0)
             range)
        (while tabs
          (let ((cur (car tabs)))
            (cond
             ((or (eq cur tab) (eq cur sel))
              (push cur range)
              (if (> n 0)
                  (setq range (nreverse range)
                        tabs  nil))
              (setq n (1+ n)))
             ((> n 0)
              (push cur range)
              (setq n (1+ n)))))
          (setq tabs (cdr tabs)))
        (tabkit-unmark-all-tabs)
        (mapc #'tabkit-tab-mark range)
        n))))

;;; Tab modified state
;;
(cl-defgeneric tabkit-tab-modified-p (_tab)
  "Return non-nil if TAB contents is modified."
  nil)

(cl-defmethod tabkit-tab-modified-p ((tab tabkit--buffer-tab))
  "Return non-nil if TAB contents is modified.
That is, if buffer TAB is visiting a file and is modified."
  (let ((buf (tabkit-tab-value tab)))
    (if (buffer-file-name buf) (buffer-modified-p buf))))

;;; Tab badge
;;
(defcustom tabkit-tab-marked-ui
  `((:symbol "✅") ;; WHITE HEAVY CHECK MARK (U+2705)
     (:default help-echo "Marked tab (click to unmark)"))
  "Icon to show on the badge of a marked tab."
  :type 'tabkit-icon-simple
  :set #'tabkit--custom-set-and-refresh-elt)

(defcustom tabkit-tab-modified-ui
  '((:symbol "💾") ;; FLOPPY DISK (U+1F4BE)
    (:default help-echo "Modified file buffer (click to save)"))
  "Icon to show on the badge of a modified tab."
  :type 'tabkit-icon-simple
  :set #'tabkit--custom-set-and-refresh-elt)

(defun tabkit-tab-badge-refresh-ui (element)
  "Clear the cached value of UI ELEMENT."
  (when (memq element '(t tabkit-tab-marked-ui tabkit-tab-modified-ui))
    (tabkit-refresh-ui-element 'tabkit--tab-badge2)
    (tabkit-refresh-ui-element 'tabkit--tab-badge4)
    (tabkit-refresh-ui-element 'tabkit--tab-badge6)))

;; Use uninterned symbols for generated badge icons to avoid
;; cluttering the global namespace.
(defconst tabkit-tab-marked (make-symbol "tabkit-tab-marked"))
(defconst tabkit-tab-modified (make-symbol "tabkit-tab-modified"))

(cl-defgeneric tabkit-tab-badge (tab &optional props)
  "Return a badge for TAB or nil if none.
The badge has icons to indicate if TAB is marked or modified.
Non-nil means a propertized string to display an image with optional
additional image properties PROPS applied."
  (let* ((m1 (tabkit-tab-marked-p tab))
         (m2 (unless (tabkit-showing-groups)
               (tabkit-tab-modified-p tab)))
         (id (+ (if m1 2 0) (if m2 4 0))))
    (unless (zerop id)
      (tabkit-with-memo (intern (format "tabkit--tab-badge%d" id))
        (let* ((i1 (when m1
                     (pic-icon-set tabkit-tab-marked
                                   `(tabkit-misc-icon
                                     ,@tabkit-tab-marked-ui))
                     tabkit-tab-marked))
               (i2 (when m2
                     (pic-icon-set tabkit-tab-modified
                                   `(tabkit-misc-icon
                                     ,@tabkit-tab-modified-ui))
                     tabkit-tab-modified))
               (ih (tabkit-image-pixel-height))
               (xdg-icontheme-theme tabkit-image-icon-theme)
               (xdg-icontheme-size ih)
               (grid (pic-icon-svg-button-grid
                      `((,i1) (,i2))
                      :padding 2
                      :rect '(:stroke-width 0 :fill transparent)))
               (props (pic-image-merge-specs
                       props
                       ;; Impose the image properties below.
                       `( :original-map= ,(pic-icon-svg-grid-map grid)
                          :height= ,ih :scale= 1 :margin= 0))))
          (propertize " " 'display
                      (tabkit-image-normalize
                       (apply #'pic-svg-image grid props))))))))

;;; Persistent tabs
;;
(defvar tabkit--persistent-tabs nil
  "List of serializable representation of tabs.")

(defcustom tabkit-use-persistent-tabs nil
  "If non-nil, tabs will persist across Emacs sessions.
Only tabs visiting files can persist across sessions."
  :type 'boolean)

(defconst tabkit--persistent-tabs-options
  '(tabkit-use-groups tabkit-buffer-group-criteria)
  "List of options on which persistent tabs depend.")

;;; Save persistent tabs
;;
(defun tabkit-serial-form (tab)
  "Return a serializable form of TAB.
That, is a list (KEY SELECTED PINNED) where:

- KEY is a value from which an unique tab can be created.
  Currently, a buffer file name.

- SELECTED non-nil means tab is selected;

- PINNED non-nil means tab is pinned."
  (list (tabkit-tab-unique-name tab)
        (tabkit-tab-selected-p tab (tabkit-tab-tabset tab))
        (tabkit-tab-pinned-p tab)))

(defmacro tabkit-serial-key (stab)
  "Return the key from tab serial form STAB."
  `(car ,stab))

(defmacro tabkit-serial-sel (stab)
  "Return the selected flag from tab serial form STAB."
  `(nth 1 ,stab))

(defmacro tabkit-serial-pin (stab)
  "Return the pinned flag from tab serial form STAB."
  `(nth 2 ,stab))

(cl-defgeneric tabkit-tab-serialize (_tab)
  "Mark TAB to be saved in order to restore it later.
Does nothing by default and return nil."
  nil)

(cl-defmethod tabkit-tab-serialize ((tab tabkit--buffer-tab))
  "Return a serializable form of TAB or nil if it is not serializable.
TAB is serializable if its value is a buffer visiting a file.
See also the function `tabkit-serial-form'."
  (if (buffer-file-name (tabkit-tab-value tab))
      (tabkit-serial-form tab)))

(defun tabkit--serialize-tabset (tabset)
  "Return a serializable form of persistent tabs in TABSET."
  (when-let* ((stabs (seq-keep #'tabkit-tab-serialize
                               (tabkit-local-tabs tabset))))
    `(,(and tabkit-use-groups (tabkit-tabset-name tabset))
      ,(tabkit-tabset-selected-p tabset)
      ,@stabs)))

(defun tabkit--save-tabs ()
  "Save a serializable form of open tabs in `tabkit--persistent-tabs'.
Return the value of `tabkit--persistent-tabs', nil if there is no
serializable open tab or if `tabkit-use-persistent-tabs' is nil."
  (or tabkit--persistent-tabs ;; Preserve previous untouched value.
      (setq tabkit--persistent-tabs
            (when-let* ((tabkit-use-persistent-tabs)
                        (tabs (delq nil (tabkit-map-tabsets
                                         #'tabkit--serialize-tabset))))
              (setq tabs (list (cons :tabs tabs)))
              (dolist (opt tabkit--persistent-tabs-options)
                (when-let* ((v (tabkit--custom-set-p opt)))
                  (push (cons opt (car v)) tabs)))
              tabs))))

(add-hook 'tabkit-before-data-save-hook #'tabkit--save-tabs)

;;; Restore persistent tabs
;;
(defcustom tabkit-reopen-file-environment
  '(
    (recentf-exclude . (always)) ;; Preserve the recentf list.
    )
  "Dynamic environment active when reopening a file.
This is an alist (VARIABLE . VALUE) elements, where VARIABLE is a
dynamic variable symbol and VALUE is the value VARIABLE is bound to."
  :type '(repeat :tag "Dynamic Environment"
                 (cons :tag "Binding"
                       (symbol :tag "Variable")
                       (sexp :tag "Value")))
  :set (lambda (variable value)
         (custom-set-default variable value)
         ;; Re-define the function 'tabkit-reopen-file' from value.
         ;; When not fbound when custom-file loaded before library,
         ;; delay the initial definition when tabkit is loaded,
         (and (fboundp 'tabkit--define-reopen-file-function)
              (tabkit--define-reopen-file-function))))

(defalias 'tabkit-reopen-file #'ignore)

(defun tabkit--define-reopen-file-function ()
  "Define the `tabkit-reopen-file' function.
This function receive a filename, bind the environment defined by the
option `tabkit-reopen-file-environment', and pass filename to
`find-file-noselect'."
  (let ((file (make-symbol "file"))
        (bindings (mapcar (lambda (b)
                            (list (car b) (macroexp-quote (cdr b))))
                          tabkit-reopen-file-environment)))
    (fset 'tabkit-reopen-file
          (+-byte-compile-lexical
           `(lambda (,file)
              (dlet ,bindings
                (find-file-noselect ,file)))))))

(defun tabkit--restore-tabs ()
  "Restore persistent tabs.
That is, tabs that were visiting files."
  (when-let* ((tabkit-use-persistent-tabs)
              (tabs (cdr (assq :tabs tabkit--persistent-tabs))))
    (dolist (opt tabkit--persistent-tabs-options)
      (when-let* ((v (assq opt tabkit--persistent-tabs)))
        (customize-set-variable opt (cdr v))))
    (let (currsel lastsel lasttab)
      (dolist (elt tabs)
        (let ((gname (or (nth 0 elt) tabkit--catch-all-group-name))
              (gcurr (nth 1 elt)))
          (dolist (stab (nthcdr 2 elt))
            (when-let* ((file (tabkit-serial-key stab))
                        ((file-readable-p file)))
              (let* ((buf (tabkit-reopen-file file))
                     (tab (tabkit-add-tab gname buf))
                     (tabset (tabkit-tab-tabset tab)))
                ;; Mark tab pinned.
                (when (tabkit-serial-pin stab)
                  (tabkit-tab-pin tab))
                ;; Mark tab selected.
                (when (tabkit-serial-sel stab)
                  (tabkit-select-tab tab tabset)
                  (if gcurr
                      (setq currsel tab)
                    (setq lastsel tab)))
                (setq lasttab tab))))))
      ;; Goto either the selected tab in the selected tabset, or the
      ;; selected tab in the last restored tabset, or the last
      ;; restored tab.
      (when-let* ((tab (or currsel lastsel lasttab)))
        (tabkit-tab-select tab)))
    (setq tabkit--persistent-tabs nil)))

(add-hook 'tabkit-after-data-load-hook #'tabkit--restore-tabs)

;;; Reopen serializable tabs recently closed
;;
(defvar tabkit--closed-tabs nil)

(defcustom tabkit-closed-tabs-number 25
  "Maximum number of recorded closed tabs.
Zero means no limit."
  :type 'natnum)

(define-inline tabkit--closed-tab-get (stab)
  "Look up STAB in the list of recently closed tabs.
Return nil if not found, or the found value of STAB.
STAB is the serializable form of a tab as returned by
`tabkit-tab-serialize'."
  (inline-quote
   (assoc (tabkit-serial-key ,stab) tabkit--closed-tabs)))

(defun tabkit--closed-tab-push (stab)
  "Push STAB in the list of recently closed tabs.
Does nothing if a corresponding entry already exists.
Return the value of STAB in the list.
STAB is the serializable form of a tab as returned by
`tabkit-tab-serialize'."
  (or (tabkit--closed-tab-get stab)
      (progn
        (push stab tabkit--closed-tabs)
        (if (> tabkit-closed-tabs-number 0)
            (setq tabkit--closed-tabs
                  (ntake tabkit-closed-tabs-number
                         tabkit--closed-tabs)))
        stab)))

(defun tabkit-closed-tab-delete (tab)
  "If an entry for TAB is found in recently closed tabs, remove it.
Return TAB is an entry has been removed, nil otherwise."
  (when-let* ((stab (tabkit-tab-serialize tab))
              (stab (tabkit--closed-tab-get stab)))
    (setq tabkit--closed-tabs (delq stab tabkit--closed-tabs))
    tab))

(defun tabkit-closed-tabs-clear (&optional arg interactive)
  "Clear the list of recently closed tabs.
Ask for confirmation unless called from a program, or interactively
with prefix ARG.
INTERACTIVE non-nil means called interactively."
  (interactive "P\np")
  (and tabkit--closed-tabs
       (or arg (not interactive)
           (y-or-n-p "Clear the list of recently closed tabs?"))
       (let (message-log-max)
         (setq tabkit--closed-tabs nil)
         (if interactive
             (message "The list of recently closed tabs is empty")))))

(defun tabkit-closed-tab-reopen (stab)
  "Reopen the tab corresponding to STAB in recently closed tabs.
STAB is the serializable form of a tab as returned by
`tabkit-tab-serialize'."
  (when-let* ((stab (tabkit--closed-tab-get stab))
              (buf (tabkit-reopen-file (tabkit-serial-key stab))))
    (pop-to-buffer-same-window buf)
    (redisplay)
    (let ((tab (tabkit-selected-tab (tabkit-current-tabset))))
      (cl-assert (eq buf (tabkit-tab-value tab)))
      (if (tabkit-serial-pin stab) (tabkit-tab-pin tab))
      ;; Removal of the reopened entry from history is done
      ;; in `tabkit-tabs-data'.
      )))

(defun tabkit-closed-tab-reopen-last (&optional arg interactive)
  "Reopen last closed tab.
Only serializable tabs can be reopened, which, for now, means only
those visiting a file.
Ask for confirmation unless called from a program, or interactively
with prefix ARG.
INTERACTIVE non-nil means called interactively."
  (interactive "P\np")
  (or tabkit--closed-tabs
      (user-error "No recently closed tab can be reopened"))
  (when-let* ((stab (car tabkit--closed-tabs))
              (_ (or arg (not interactive)
                     (y-or-n-p (format "Reopen %S?"
                                       (tabkit-serial-key stab))))))
    (message nil)
    (tabkit-closed-tab-reopen stab)))

(defconst tabkit--closed-tabs-clear-item
  ["Clear list..."
   tabkit-closed-tabs-clear
   :help "Clear the list of recently closed tabs"])

(defun tabkit-closed-tabs-menu-items (&optional _menu)
  "Make menu items for recently closed tabs that can be reopened.
Only serializable tabs can be reopened, which, for now, means only
those visiting a file.
This is a menu filter function which ignores the MENU argument."
  (let (items)
    (dolist (stab tabkit--closed-tabs)
      (let ((file (tabkit-serial-key stab)))
        (push `[,(abbreviate-file-name file)
                ;; Use an unique lexical variable `elt' for each
                ;; menu-item closure.
                ,(let ((elt stab))
                   (lambda ()
                     (interactive)
                     (tabkit-closed-tab-reopen elt)))
                :help ,file]
              items)))
    (push menu-bar-separator items)
    (push tabkit--closed-tabs-clear-item items)
    (nreverse items)))

;;; Instant groups
;;
(defvar tabkit--instant-group-criteria nil
  "List of criteria to assign instant groups.
That is, an alist of (tab-name group-name1 ...) elements.")

(defun tabkit-instant-group-names ()
  "Return the list of all currently known instant group names."
  (tabkit-with-memo 'tabkit--instant-group-names
    (seq-uniq (seq-mapcat #'cdr tabkit--instant-group-criteria))))

(defun tabkit-instant-group-p (name)
  "Return non-nil if NAME is an instant group name."
  (and (member name (tabkit-instant-group-names)) t))

(cl-defmethod tabkit-tab-instant-group-unset ((tab tabkit--tab))
  "Remove TAB from the instant group it belongs to.
Return non-nil if TAB was actually removed from instant group.
Does nothing if TAB is pinned or not belong to an instant group."
  (condition-case err
      (if (tabkit-tab-pinned-p tab)
          (user-error "Pinned tab not removed from group")
        (let* ((tn (tabkit-tab-unique-name tab))
               (clause (or (assoc tn tabkit--instant-group-criteria
                                  #'tabkit-name-equal)
                           (user-error
                            "Tab not removed from default group")))
               (gn (tabkit-tab-group-name tab))
               (gl (setcdr clause (delete gn (cdr clause)))))
          ;; Remove clause either after removing last group, or if
          ;; the remaining last group is a default group.
          (when (or (null gl)
                    (and (null (cdr gl))
                         (member (car gl)
                                 (tabkit-tab-default-groups tab))))
            (setf (alist-get tn tabkit--instant-group-criteria
                             nil 'remove #'tabkit-name-equal)
                  nil)
            ;; Tab is back to its default assigned group.
            (message "Tab in default group"))
          ;; Further refresh the list of instant group names.
          (setf (tabkit-memo 'tabkit--instant-group-names) nil)))
    (:success t)
    (error (message "%s" (error-message-string err)) nil)))

(cl-defmethod tabkit-tab-instant-group-add ((tab tabkit--tab) group-name)
  "Add TAB to instant group GROUP-NAME.
Return non-nil if TAB was actually added to instant group.
Does nothing if TAB is already in instant GROUP-NAME."
  (condition-case err
      (if (eq (tabkit-tabset group-name) (tabkit-tab-tabset tab))
          (user-error "Tab already in group")
        (let* ((tn (tabkit-tab-unique-name tab))
               (clause (assoc tn tabkit--instant-group-criteria
                              #'tabkit-name-equal)))
          (if clause
              (if (member group-name (cdr clause))
                  (user-error "Tab already in group")
                (setcdr clause (cons group-name (cdr clause))))
            ;; Because the tab is copied into another group, the
            ;; clause has to match both the from and to groups!
            (push (list tn
                        group-name
                        (tabkit-tab-group-name tab))
                  tabkit--instant-group-criteria))
          ;; Further refresh the list of instant group names.
          (setf (tabkit-memo 'tabkit--instant-group-names) nil)))
    (:success t)
    (error (message "%s" (error-message-string err)) nil)))

(cl-defmethod tabkit-tab-instant-group-set ((tab tabkit--tab) group-name)
  "Put TAB into instant group GROUP-NAME.
That is, add TAB to instant group GROUP-NAME, and remove it from the
instant group it belongs to.
Does nothing if TAB is pinned, or already in instant GROUP-NAME, or
not belong to another instant group.
Return non-nil if TAB was actually put into instant group."
  (let ((igroups tabkit--instant-group-criteria))
    (condition-case nil
        (or (and (tabkit-tab-instant-group-add tab group-name)
                 (tabkit-tab-instant-group-unset tab))
            (user-error ""))
      (:success t)
      ;; Rollback any change if both copy and move didn't succeed.
      (error (setq tabkit--instant-group-criteria igroups) nil))))

(defun tabkit-instant-group-load (&optional group)
  "Open all persistent tabs found in instant GROUP.
Tabs are visually ordered from left to right, from oldest to most
recently added to the group."
  (interactive)
  (let ((group (if (tabkit-instant-group-p group)
                   group
                 (tabkit-read-instant-group)))
        loads)
    (or (tabkit-instant-group-p group)
        (user-error "Group %s does not exist" group))
    (pcase-dolist (`(,name . ,groups) tabkit--instant-group-criteria)
      ;; Don't reopen tab whose name is tagged with "transient" (like
      ;; the tab unique name of a buffer not visiting a file).
      (and (not (tabkit-annotated-name-p name :transient))
           (member group groups)
           (push name loads)))
    ;; In instant group criteria tabs are in reverse order, from
    ;; newest to oldest added.  Reopen them in reverse order.
    (dolist (file loads)
      (if (file-readable-p file)
          (tabkit-reopen-file file)))
    ;; Refresh tabs data to map new buffers to tabs.
    (tabkit-tabs-data-refresh)
    (tabkit-current-tabset)
    ;; Go to the selected tab in group.
    (when-let* ((tab (tabkit-selected-tab (tabkit-tabset group))))
      (tabkit-tab-select tab))))

(cl-defgeneric tabkit-tab-default-groups (_tab)
  "Return the list of default groups buffer TAB belongs to.
An empty list by default."
  nil)

(cl-defmethod tabkit-tab-default-groups ((tab tabkit--buffer-tab))
  "Return the list of default groups buffer TAB belongs to."
  (let (tabkit--instant-group-criteria)
    (tabkit-buffer-groups (tabkit-tab-value tab))))

(cl-defgeneric tabkit-tab-in-default-group-p (tab)
  "Return non-nil if buffer TAB belongs to a default group."
  (member (tabkit-tab-group-name tab)
          (tabkit-tab-default-groups tab)))

;;; Moving tabs
;;
(defun tabkit--tab-move (tabs tab n)
  "Move TAB of N positions in list of TABS.
Move forward if N is positive, backward if N is negative.
If the new position is beyond the first or last element of TABS, TAB
is moved respectively to the beginning or to the end of TABS.
Does nothing and return nil if N is 0 or TAB is not in TABS.
Otherwise, return LIST altered with TAB moved."
  (when-let* (((not (= 0 n)))
              (i (seq-position tabs tab #'eq))
              (j (+ i n)))
    (cond
     ((<= j 0)
      ;; (cons tab (nconc (take i tabs) (nthcdr (1+ i) tabs)))
      ;; Can use delq because tab should be unique in tabs.
      (cons tab (delq tab tabs)))
     ((>= j (length tabs))
      ;; (nconc (take i tabs) (nthcdr (1+ i) tabs) (list tab))
      ;; Can use delq because tab should be unique in tabs.
      (nconc (delq tab tabs) (list tab)))
     (t
      (and (< j i) (setq i (1+ i) j (1- j)))
      (push tab (cdr (nthcdr j tabs)))
      (nconc (take i tabs) (nthcdr (1+ i) tabs))))))

(cl-defmethod tabkit-tab-move ((tab tabkit--tab) n tabset)
  "Move TAB N positions forward (backward if N is negative) in TABSET.
If the new position is beyond the first or last tab in TABSET, TAB
is moved respectively to the first or last position.
Does nothing and return nil if N is 0 or TAB is not in TABSET."
  (when-let* ((tabs (tabkit--tab-move
                     (tabkit-local-tabs tabset) tab n)))
    (setf (tabkit-tabset-prop tabset :tabs) tabs)))

;;; Tabkit completion
;;
(defcustom tabkit-completion-ignore-case t
  "Non-nil means tabkit completion ignores case."
  :type 'boolean)

(defcustom tabkit-completion-styles
  '(substring basic)
  "List of completion styles used in tabkit read functions.
The available styles are listed in `completion-styles-alist'."
  :type `(repeat
          :tag "insert a new menu to add more styles"
          (choice
           :convert-widget
           ,(lambda (widget)
              (let ((sl (mapcar (lambda (x) `(const ,(car x)))
                                completion-styles-alist)))
                (widget-put widget :args (mapcar #'widget-convert sl))
                widget))))
  :set (lambda (variable value)
         (setf (alist-get 'tabkit completion-category-defaults)
               `((styles ,@value)))
         (custom-set-default variable value)))

(defun tabkit--completing-read (prompt collection
                                       &optional predicate
                                       require-match initial-input
                                       hist def inherit-input-method)
  "Read a string in the minibuffer, with tabkit completion.
Use `tabkit-completion-styles' and `tabkit-completion-ignore-case'.

PROMPT is a string to prompt with; normally it ends in a colon and a
space.

COLLECTION must be either a list of strings, an alist, an obarray or a
hash table.

See `completing-read' for the meaning of PREDICATE, REQUIRE-MATCH,
INITIAL-INPUT, HIST, DEF, and INHERIT-INPUT-METHOD arguments."
  (let ((completion-ignore-case tabkit-completion-ignore-case))
    (completing-read prompt
                     (lambda (string pred action)
                       (if (eq action 'metadata)
                           '(metadata (category . tabkit))
                         (complete-with-action
                          action collection string pred)))
                     predicate require-match initial-input hist def
                     inherit-input-method)))

;;; Read tab with completion
;;
(defun tabkit--tab-completion-collection (&optional tabset)
  "Return a collection of tabs for completion.
That is, an alist of (TAB-NAME . TAB) elements.  If optional argument
TABSET is non-nil, return elements for tabs in TABSET, otherwise for
all existing tabs.  If TABSET is the tabset of all selected tabs, NAME
is TAB's group name, otherwise TAB's name."
  (let ((full-collection
         (tabkit-with-memo 'tabkit--tab-completion-collection
           (let ((tl (tabkit-map-tabsets #'tabkit-global-tabs)))
             (mapcar (lambda (tab)
                       (cons (tabkit-tab-name tab) tab))
                     (if (cdr tl) (apply #'append tl) (car tl)))))))
    (if (and tabset tabkit-use-groups)
        (if (tabkit-meta-tabset-p tabset)
            (mapcar (lambda (tab)
                      (cons (tabkit-tab-group-name tab) tab))
                    (tabkit-global-tabs tabset))
          (seq-keep (lambda (e)
                      (and (eq tabset (tabkit-tab-tabset (cdr e))) e))
                    full-collection))
      full-collection)))

;; Refresh completion data.
(defun tabkit-tab-completion-update ()
  "Force update of tab completion data."
  (setf (tabkit-memo 'tabkit--tab-completion-collection) nil))

(add-hook 'tabkit-after-tabs-change-hook
          #'tabkit-tab-completion-update)

(defun tabkit-read-tab (&optional prompt tabset)
  "Read a tab by name with completion.
Optional argument PROMPT is a text prompt which defaults to \"Tab: \".
If optional argument TABSET is non-nil, read a tab that belongs to
this tabset.  Otherwise read a tab that belongs to any tabset.
Return the tab found or nil if no input."
  (let* ((tabs (tabkit--tab-completion-collection tabset))
         (tabn (tabkit--completing-read (or prompt "Tab: ")
                                        tabs nil t))
         (dups (seq-filter (lambda (e) (string-equal tabn (car e)))
                           tabs)))
    (cdr (if (cdr dups)
             ;; Tab with name exists in several groups, select a group
             ;; and return the corresponding tab.
             (let ((g (tabkit-tabset
                       (tabkit--completing-read
                        "Group: " (mapcar #'cddr dups) nil t))))
               (car (seq-filter (lambda (e) (eq g (cddr e))) dups)))
           ;; Tab with name exists only in one group, return the tab.
           (assoc tabn tabs)))))

(define-inline tabkit-read-group-tab ()
  "Read a group tab by group name.
Return a tab name or nil if no input."
  (inline-quote (tabkit-read-tab "Group Tab: " tabkit--meta-tabset)))

(defun tabkit-read-tab-or-tab-group (&optional tabset)
  "Read a tab by name or group tab by group name with completion.
Return a tab name or nil if no input.  If group tabs are displayed in
the selected window (`tabkit-showing-groups' return non-nil), read a
group tab by group name.  Otherwise, if optional argument TABSET is
non-nil, read a tab that belongs to this tabset, otherwise read a tab
that belongs to any tabset."
  (if (tabkit-showing-groups)
      (tabkit-read-group-tab)
    (tabkit-read-tab nil tabset)))

;;; Read group with completion
;;
(defun tabkit-all-group-names ()
  "Return the list of all currently known group names."
  (seq-union (seq-mapcat #'cdr tabkit--instant-group-criteria)
             tabkit-default-group-names))

(defun tabkit-read-group ()
  "Read a group name with completion.
Return the name read or nil if tabs are not grouped or if no input."
  (let ((group (tabkit--completing-read
                "Group: " (tabkit-all-group-names))))
    (unless (string-match-p "\\`[[:space:]]*\\'" group)
      group)))

(defun tabkit-read-instant-group ()
  "Read an instant group name with completion.
Return the name read or nil if tabs are not grouped or if no input."
  (if-let* ((groups (tabkit-instant-group-names)))
      (let ((group (tabkit--completing-read "Instant group: " groups)))
        (unless (string-match-p "\\`[[:space:]]*\\'" group)
          group))
    (user-error "No group found")))

;;; Tab/Group Commands
;;
(defun tabkit-goto-tab (tab)
  "Go to TAB using its name."
  (interactive (list (tabkit-read-tab "Go to tab: ")))
  (and (tabkit-tab-p tab)
       (tabkit-tab-select tab)))

(defun tabkit-goto-selected-tab (&optional event)
  "Go to the selected TAB on window receiving EVENT."
  (interactive (list last-input-event))
  (gmouse-set-current-event event)
  (+-with-selected-window
      (window-normalize-window (posn-window (event-start event)))
    (tabkit-tab-select (tabkit-selected-tab (tabkit-current-tabset)))
    (tabkit-display-update)))

(defun tabkit-tab-copy-to-group (tab &optional group)
  "Copy TAB to GROUP."
  (interactive (list (tabkit-read-tab) (tabkit-read-group)))
  (when (or group (setq group (tabkit-read-group)))
    (and (tabkit-tab-instant-group-add tab group)
         (tabkit-tabs-data-refresh))))

(defun tabkit-tab-move-to-group (tab &optional group)
  "Move TAB to GROUP."
  (interactive (list (tabkit-read-tab) (tabkit-read-group)))
  (when (or group (setq group (tabkit-read-group)))
    (and (tabkit-tab-instant-group-set tab group)
         (tabkit-tabs-data-refresh))))

(defun tabkit-tab-remove-from-group (tab)
  "Remove TAB from the group it belongs to."
  (interactive (list (tabkit-read-tab)))
  (and (tabkit-tab-instant-group-unset tab)
       (tabkit-tabs-data-refresh)))

(defun tabkit-group-reset (name)
  "Reset the tab group with NAME.
That is, remove from this group all tabs not there by default."
  (interactive (list (tabkit-read-instant-group)))
  (when name
    (let (result)
      (dolist (c tabkit--instant-group-criteria)
        ;; Remove clause either after removing last group.
        (or (null (setcdr c (delete name (cdr c))))
            (push c result)))
      (setq tabkit--instant-group-criteria (nreverse result))
      ;; Further refresh the list of instant group names.
      (setf (tabkit-memo 'tabkit--instant-group-names) nil)
      (tabkit-tabs-data-refresh))))

(defun tabkit-group-order-from-local (&optional no-customize)
  "Quickly set default group order from tab groups of selected window.
This sets the `tabkit-group-order' option for the current session,
then shows customization of the new value if NO-CUSTOMIZE is non-nil.
Interactively, NO-CUSTOMIZE is the numeric prefix argument."
  (interactive "P")
  (or (tabkit-showing-groups) (user-error "Tab groups not visible"))
  (customize-set-variable
   'tabkit-group-order
   (mapcar #'tabkit-tab-group-name
           (tabkit-local-tabs tabkit--meta-tabset)))
  (if no-customize
      (message "`tabkit-group-order' set for current session only")
    (customize-option 'tabkit-group-order)))

(defun tabkit-group-order-reset ()
  "Reset group order to default for tab groups of selected window."
  (interactive)
  (or (tabkit-showing-groups) (user-error "Tab groups not visible"))
  (setf (tabkit-tabset-prop tabkit--meta-tabset :tabs) nil))

(defun tabkit-move-selected-tab (&optional n)
  "Move selected tab N positions forward (backward if N is negative).
On reaching first or last position, stop at that position.
Interactively, N is the numeric prefix argument.  If N is omitted or
nil, move 1 position forward (\\[universal-argument] move backward)."
  (interactive "P")
  (setq n (cond ((null n)   1)
                ((eq n '-) -1)
                ((consp n) (- (round (log (car n) 4))))
                (n)))
  (let* ((tabset (tabkit-current-tabset))
         (tab (tabkit-selected-tab tabset)))
    (and (not (tabkit-tab-pinned-p tab))
         (tabkit-tab-move tab (or n 1) tabset)
         (tabkit-display-update))))

(defun tabkit-move-tab-next-to-tab (&optional from to tabset)
  "Move tab FROM next to tab TO in TABSET.
By default TABSET is the one displayed in the selected window.
Both tabs must belong to TABSET.  If TO is before FROM, move FROM just
before TO.  If TO is after FROM, move FROM just after TO."
  (interactive)
  (let (tabs p1 p2)
    (and (or tabset (setq tabset (tabkit-current-tabset)))
         (or from (setq from (tabkit-read-tab "Move tab: " tabset)))
         (or to (setq to (tabkit-read-tab
                          (format "Move tab %s next to tab: "
                                  (propertize
                                   (tabkit-tab-display-name from)
                                   'face 'default))
                          tabset)))
         (not (eq from to))
         (not (tabkit-tab-pinned-p from))
         (not (tabkit-tab-pinned-p to))
         (setq tabs (tabkit-local-tabs tabset))
         (setq p1 (seq-position tabs from #'eq))
         (setq p2 (seq-position tabs to   #'eq))
         (tabkit-tab-move from (- p2 p1) tabset)
         (tabkit-display-update))))

(defun tabkit-read-color (tab)
  "Read a color starting from TAB current color."
  (let ((color (tabkit-tab-color tab)))
    (if (fboundp 'nuancier-read-color)
        (nth 1 (nuancier-read-color "Reading active color..." color))
      (read-color "Enter a color: "))))

(defun tabkit-tab-change-color (tab &optional color)
  "Change TAB color to COLOR."
  (interactive (let ((tab (tabkit-read-tab-or-tab-group)))
                 (list tab (tabkit-read-color tab))))
  (and (tabkit-tab-p tab)
       (or color (setq color (tabkit-read-color tab)))
       (setf (tabkit-tab-instant-color tab) color)
       (tabkit-display-update)))

(defun tabkit-tab-reset-color (tab)
  "Reset TAB default color."
  (interactive (list (tabkit-read-tab-or-tab-group)))
  (and (tabkit-tab-p tab)
       (setf (tabkit-tab-instant-color tab) nil)
       (tabkit-display-update)))

;;; Toggle mark of a tab
;;
(defun tabkit-toggle-tab-mark (&optional tab)
  "Toggle TAB mark."
  (interactive)
  (when-let* ((tab (or tab (tabkit-read-tab-or-tab-group
                            (tabkit-current-tabset))))
              (_ (tabkit-tab-p tab)))
    (if (tabkit-tab-unmark tab)
        (message "Tab %s unmarked" (tabkit-tab-display-name tab))
      (tabkit-tab-mark tab)
      (message "Tab %s marked" (tabkit-tab-display-name tab)))))

;;; Apply action on marked tabs
;;
(defun tabkit-unmark-marked-tabs (&optional tabs)
  "Unmark marked TABS."
  (interactive)
  (when-let* ((tabs (or tabs (tabkit-marked-tabs)))
              (n 0))
    (dolist (tab tabs)
      (if (tabkit-tab-unmark tab)
          (setq n (1+ n))))
    (when (> n 0)
      (message "%s tab%s unmarked" n (if (> n 1) "s" ""))
      (tabkit-display-update))))

(defun tabkit-pin-marked-tabs (&optional tabs)
  "Pin marked TABS."
  (interactive)
  (when-let* ((_ (not (tabkit-showing-groups)))
              (tabs (or tabs (tabkit-marked-tabs))))
    (mapc #'tabkit-tab-pin tabs)
    (tabkit-display-update)))

(defun tabkit-unpin-marked-tabs (&optional tabs)
  "Unpin marked TABS."
  (interactive)
  (when-let* ((_ (not (tabkit-showing-groups)))
              (tabs (or tabs (tabkit-marked-tabs))))
    (mapc #'tabkit-tab-unpin tabs)
    (tabkit-display-update)))

(defun tabkit-change-color-of-marked-tabs (&optional tabs)
  "Change color of marked TABS."
  (interactive)
  (when-let* ((tabs (or tabs (tabkit-marked-tabs)))
              (color  (tabkit-read-color (car tabs))))
    (dolist (tab tabs)
      (setf (tabkit-tab-instant-color tab) color))
    (tabkit-display-update)))

(defun tabkit-reset-color-of-marked-tabs (&optional tabs)
  "Reset color of marked TABS."
  (interactive)
  (when-let* ((tabs (or tabs (tabkit-marked-tabs))))
    (dolist (tab tabs)
      (setf (tabkit-tab-instant-color tab) nil))
    (tabkit-display-update)))

(defun tabkit-copy-to-group-marked-tabs (&optional tabs)
  "Copy marked TABS to another group."
  (interactive)
  (when-let* ((_ (not (tabkit-showing-groups)))
              (tabs (or tabs (tabkit-marked-tabs)))
              (group (tabkit-read-group))
              (n 0))
    (dolist (tab tabs)
      (if (tabkit-tab-instant-group-add tab group)
          (setq n (1+ n))))
    (when (> n 0)
      (tabkit-tabs-data-refresh)
      (message "%d tab%s copied to group %s"
               n (if (= n 1) "" "s") group))))

(defun tabkit-move-to-group-marked-tabs (&optional tabs)
  "Move marked TABS to another group."
  (interactive)
  (when-let* ((_ (not (tabkit-showing-groups)))
              (tabs (or tabs (tabkit-marked-tabs)))
              (group (tabkit-read-group))
              (n 0))
    (dolist (tab tabs)
      (if (tabkit-tab-instant-group-set tab group)
          (setq n (1+ n))))
    (when (> n 0)
      (tabkit-tabs-data-refresh)
      (message "%d tab%s moved to group %s"
               n (if (= n 1) "" "s") group))))

(defun tabkit-remove-from-group-marked-tabs (&optional tabs)
  "Remove marked TABS from the current group."
  (interactive)
  (when-let* ((_ (not (tabkit-showing-groups)))
              (tabs (or tabs (tabkit-marked-tabs)))
              (n 0))
    (dolist (tab tabs)
      (if (tabkit-tab-instant-group-unset tab)
          (setq n (1+ n))))
    (when (> n 0)
      (tabkit-tabs-data-refresh)
      (message "%d tab%s removed from this group"
               n (if (= n 1) "" "s")))))

(defun tabkit-close-marked-tabs (&optional tabs)
  "Close marked TABS."
  (interactive)
  (when-let* ((tabs (or tabs (tabkit-marked-tabs))))
    (if (tabkit-showing-groups)
        (tabkit--close-group-tabs tabs)
      (tabkit--close-single-tabs tabs))))

;;; Popup menus
;;
(defun tabkit-menu-popup (menu &optional position)
  "Popup MENU at POSITION and call the selected option.
Wrap the `popup-menu' function in order to fail gracefully when
another popup is already active."
  (if (menu-or-popup-active-p)
      (error "Popup already active")
    (popup-menu menu position)))

(defun tabkit-menu-value (symbol)
  "Return the menu value setup in SYMBOL or nil if not found."
  (tabkit-with-memo symbol
    (let* ((getter (or (get symbol 'custom-get) 'default-value))
           (menu   (if (default-boundp symbol)
                      (with-demoted-errors "tabkit-menu-value, %S"
                        (funcall getter symbol))
                     (symbol-value symbol)))
           (title  (car menu))
           (items  (if (custom-variable-p symbol)
                       ;; If menu SYMBOL is customizable, add an item
                       ;; to customize this menu.
                       `(,@(cdr menu)
                         ,menu-bar-separator
                         ["Customize..."
                          ,(lambda ()
                             (interactive)
                             (customize-option symbol))
                          :help "Customize this menu"])
                     (cdr menu))))
      (easy-menu-create-menu title items))))

(defun tabkit-menu-value-popup (symbol &optional event)
  "Popup the menu value of SYMBOL if set.
Optional argument EVENT is a received mouse event."
  (when-let* ((menu (tabkit-menu-value symbol)))
    (tabkit-menu-popup menu event)))

;;; The options menu
;;
(defvar tabkit--menu-save-candidates nil
  "Records options candidates for save by `tabkit-menu-options-save'.
For now, those set with `tabkit-menu-make-toggle-item'.")

(defun tabkit-menu-make-toggle-item (option item-name &rest keywords)
  "Define a toggle option menu-item.
OPTION (a symbol) is the variable to set.
ITEM-NAME (a string) is the menu item name.
KEYWORDS is a plist of menu item keywords accepted by
`easy-menu-define'."
  (let ((qopt (macroexp-quote option)))
    (push option tabkit--menu-save-candidates)
    `[,item-name
      ,(lambda ()
         (interactive)
         (custom-load-symbol option)
         (let ((set (or (get option 'custom-set) #'set-default))
               (get (or (get option 'custom-get) #'default-value)))
           (funcall set option (not (funcall get option)))
           (message "Option %s %sabled globally" option
                    (if (default-value option) "en" "dis"))
           (tabkit-display-update)))
      :style toggle
      :selected (and (default-boundp ,qopt)
                     (default-value ,qopt))
      ,@keywords]))

(defun tabkit-menu-options-to-save-p ()
  "Return non nil if there are options to save using Custom."
  (let (need-save)
    (dolist (option tabkit--menu-save-candidates)
      (setq need-save (or need-save
                          (customize-mark-to-save option))))
    need-save))

(defun tabkit-menu-options-save ()
  "Save current values of options changed from menu, using Custom."
  (interactive)
  ;; Save if we changed anything.
  (if (tabkit-menu-options-to-save-p)
      (custom-save-all)
    (message "Nothing to save")))

(defun tabkit-options-menu-do-popup (event)
  "Popup menu to manage options on received EVENT."
  (tabkit-menu-popup
   (easy-menu-create-menu
    "Options"
    `(
      ,(tabkit-menu-make-toggle-item
        'tabkit-use-images
        "Use images"
        :help "Toggle use of images")
      ,(tabkit-menu-make-toggle-item
        'tabkit-use-groups
        "Use tab groups"
        :help "Toggle use of tab groups")
      ,(tabkit-menu-make-toggle-item
        'tabkit-use-persistent-tabs
        "Use persistent tabs"
        :help "Find tabs visiting files in the next Emacs session")
      ,(tabkit-menu-make-toggle-item
        'tabkit-show-tab-icons
        "Show tab icons"
        :help "Toggle display of tab icons")
      ["Save options"
       tabkit-menu-options-save
       :help "Save above options set from the menu"
       :active (tabkit-menu-options-to-save-p)]
      ,menu-bar-separator
      ,@(if tabkit-use-groups
            '(["Show groups"
               tabkit-toggle-show-groups
               :help "Toggle showing group tabs"
               :style toggle
               :selected (tabkit-showing-groups)]))
      ,@(if (tabkit-showing-groups)
            `(["Set groups default display order..."
               tabkit-group-order-from-local
               :help "Set default display order of tab groups\
 from that of selected window"]
              ["Reset groups display order"
               tabkit-group-order-reset
               :help "Restore default display order of tab groups\
 in selected window"]))
      ,menu-bar-separator
      ["Customize..."
       ,(lambda ()
          (interactive)
          (customize-group-other-window 'tabkit))
       :help "Customize Tabkit"]
      ,menu-bar-separator
      ["Hide the Tabkit line for current buffer"
       tabkit-locally-hide
       :help ,(substitute-command-keys
               "Use \\[tabkit-locally-hide] to show it again.")]
      ))
   event))

(defun tabkit-options-menu-popup (&optional event)
  "Popup menu to manage options on received EVENT."
  (interactive (list last-input-event))
  (gmouse-set-current-event event)
  (tabkit-options-menu-do-popup event))

;;; The Quick tab menu
;;
(defcustom tabkit-quicktab-menu-current-tabset-mark
  " ›››" ;; SINGLE RIGHT-POINTING ANGLE QUOTATION MARK (U+203A)
  "In the quick tab menu, mark appended to the current tabset name."
  :type '(string))

(defcustom tabkit-quicktab-menu-pinned-tab-mark
  " ‹‹‹" ;; SINGLE LEFT-POINTING ANGLE QUOTATION MARK (U+2039)
  "In the quick tab menu, mark appended to the name of a pinned tab."
  :type '(string))

(defun tabkit-quicktab-menu-from-tabset (tabset)
  "Return a quick tab menu of tabs in TABSET.
The menu name is TABSET name.  Each tab in TABSET is a radio button
menu item with tab name that will select the corresponding tab.
See also the variables `tabkit-quicktab-menu-pinned-tab-mark' and
`tabkit-quicktab-menu-current-tabset-mark'."
  (let (menu)
    (dolist (tab (tabkit-local-tabs tabset))
      (push `[,(concat (tabkit-tab-name tab)
                       (and (tabkit-tab-pinned-p tab)
                            tabkit-quicktab-menu-pinned-tab-mark))
              ,(lambda ()
                 (interactive)
                 (tabkit-tab-select tab)
                 (tabkit-display-update))
              ;; :help "Go to this tab"
              :active t
              ;; Use a radio button to indicate the selected tab
              :style radio
              :selected ,(tabkit-tab-selected-p tab tabset)]
            menu))
    (cons (concat (tabkit-tabset-name tabset)
                  (and (tabkit-tabset-selected-p tabset)
                       tabkit-quicktab-menu-current-tabset-mark))
          (nreverse menu))))

(defun tabkit-quicktab-menu-do-popup (event)
  "Popup a menu to navigate through tabs on received EVENT.
Unless there is only one tabset (group), tab menu-items are grouped
into submenus by tabsets."
  (let (submenus)
    (dolist (tab (tabkit-global-tabs (tabkit-meta-tabset)))
      (push (tabkit-quicktab-menu-from-tabset (tabkit-tab-tabset tab))
            submenus))
    (setq submenus (nreverse submenus))
    (tabkit-menu-popup
     (easy-menu-create-menu
      "Go to tab"
      `(,@(if (cdr submenus) submenus (cdar submenus))
        ,menu-bar-separator
        ["By name..."
         tabkit-goto-tab
         :help "Go to a tab using its name"]))
     event)))

(defun tabkit-quicktab-menu-popup (&optional event)
  "Popup a menu to navigate through tabs on received EVENT.
Unless there is only one tabset (group), tab menu-items are grouped
into submenus by tabsets."
  (interactive (list last-input-event))
  (gmouse-set-current-event event)
  (+-with-selected-window
      (window-normalize-window (posn-window (event-start event)))
    (tabkit-quicktab-menu-do-popup event)))

;;; The Manage windows menu
;;
(defun tabkit-split-window-below (&optional tab)
  "Select TAB in a new window below.
TAB defaults to the tab selected in the current tab line."
  (let ((w (split-window-below)))
    ;; Select the new window, then TAB on that window.
    (select-window w)
    (and tab (tabkit-tab-select tab))))

(defun tabkit-split-window-right (&optional tab)
  "Select TAB in a new window on right.
TAB defaults to the tab selected in the current tab line."
  (let ((w (split-window-right)))
    ;; Select the new window, then TAB on that window.
    (select-window w)
    (and tab (tabkit-tab-select tab))))

(defun tabkit-delete-window (&optional _tab)
  "Delete the selected window."
  (ignore-errors (delete-window)))

(defun tabkit-delete-other-windows (&optional _tab)
  "Make the selected window occupy whole frame."
  (delete-other-windows))

(defun tabkit-toggle-split-window (&optional tab)
  "Split or delete the selected window.
If window is the only window, split it vertically, and select TAB in
the new window below; otherwise, delete other windows.
TAB defaults to the tab selected in the current tab line."
  (if (one-window-p t)
      (tabkit-split-window-below tab)
    (tabkit-delete-other-windows tab)))

(defun tabkit-window-menu-do-popup (event)
  "Popup a menu to manage windows on received EVENT."
  (let ((split (not (one-window-p t)))
        (tab (tabkit-posn-tab (event-end event))))
    (tabkit-menu-popup
     (easy-menu-create-menu
      "Manage windows"
      `(
        [,(if tab
              "Show focused tab in split window below <S-mouse-2>"
            "Show selected tab in split window below <S-mouse-2>")
         ,(lambda ()
            (interactive)
            (tabkit-split-window-below tab))]
        [,(if tab
              "Show focused tab in split window on right <s-mouse-2>"
            "Show selected tab in split window on right <s-mouse-2>")
         ,(lambda ()
            (interactive)
            (tabkit-split-window-right tab))]
        ["Delete focused window <C-mouse-2>"
         ,(lambda ()
            (interactive)
            (tabkit-delete-window tab))
         :visible ,split]
        ["Delete other windows <mouse-2>"
         ,(lambda ()
            (interactive)
            (tabkit-delete-other-windows tab))
         :visible ,split]
        ))
     event)))

;;; Base keymap
;;
(defun tabkit-key (&rest keys)
  "Return the description of key-sequence KEYS on the tabkit line.
That is, prepend the value of variable `tabkit-line' to KEYS.
The returned value is a string that satisfies `key-valid-p'."
  (key-description (list tabkit-line keys)))

(defvar-keymap tabkit-line-map
  :doc "Base keymap used on the tabkit line."
  ;; Define a default handler for all tabkit line events.
  (tabkit-key "<t>") #'tabkit-line-event-handler
  )

;; FIXME: should report only enabled bindings.  Maybe a command to
;; display a global help could be better than recomputing tooltip
;; texts every time they are requested?
(defconst tabkit-line-help ""
;;   "
;; <k2>long mouse-1</k2>: Pop up a menu to quickly access any tab<br>
;; <k2>long mouse-2</k2>: Pop up a menu to manage windows<br>
;; <k2>long mouse-3</k2>: Pop up a menu to manage options<br>
;; <k2>mouse-wheel</k2>: cycle across group or single tabs"
  "Help echo string associated to the base keymap.")

(gmouse-define-global-handler tabkit-line-event-handler
  "Handle any EVENT on the tabkit line.
And forward it to `tabkit-line-handle-event'."
  #'tabkit-line-handle-event)

(cl-defgeneric tabkit-line-handle-event (event)
  "Default handling of EVENT on the tabkit line.
Ignore events that are not otherwise handled."
  (ignore event))

(cl-defmethod tabkit-line-handle-event ((event down-mouse-1))
  "Handle a `down-mouse-1' EVENT on the tabkit line.
On long press, forward EVENT to `tabkit-quicktab-menu-do-popup'"
  (gmouse-funcall-on-long-press
   (lambda ()
     ;; GTK3: move the mouse pointer inside the context menu to keep
     ;; it open when the mouse is released.  The move must be at
     ;; least 6 px to both left and down for the mouse pointer to be
     ;; detected as being inside the menu.  Use 10 as safety margin.
     (tabkit--move-mouse-pointer 10 10)
     (tabkit-quicktab-menu-do-popup event))))

(cl-defmethod tabkit-line-handle-event ((event down-mouse-2))
  "Handle a `down-mouse-2' EVENT on the tabkit line.
On long press, forward EVENT to `tabkit-window-menu-do-popup'"
  (gmouse-funcall-on-long-press
   (lambda ()
     ;; GTK3: move the mouse pointer inside the context menu to keep
     ;; it open when the mouse is released.  The move must be at
     ;; least 6 px to both left and down for the mouse pointer to be
     ;; detected as being inside the menu.  Use 10 as safety margin.
     (tabkit--move-mouse-pointer 10 10)
     (tabkit-window-menu-do-popup event))))

(cl-defmethod tabkit-line-handle-event ((event down-mouse-3))
  "Handle a `down-mouse-3' EVENT on the tabkit line.
On long press, forward EVENT to `tabkit-options-menu-do-popup'"
  (gmouse-funcall-on-long-press
   (lambda ()
     ;; GTK3: move the mouse pointer inside the context menu to keep
     ;; it open when the mouse is released.  The move must be at
     ;; least 6 px to both left and down for the mouse pointer to be
     ;; detected as being inside the menu.  Use 10 as safety margin.
     (tabkit--move-mouse-pointer 10 10)
     (tabkit-options-menu-do-popup event))))

(cl-defmethod tabkit-line-handle-event ((event mouse-2))
  "Handle a `mouse-2' EVENT on the tabkit line.
Call `tabkit-toggle-split-window' with tab at event position."
  (tabkit-toggle-split-window (tabkit-posn-tab (event-end event))))

(cl-defmethod tabkit-line-handle-event ((event C-mouse-2))
  "Handle a `C-mouse-2' EVENT on the tabkit line.
Call `tabkit-delete-window' with tab at event position."
  (tabkit-delete-window (tabkit-posn-tab (event-end event))))

(cl-defmethod tabkit-line-handle-event ((event S-mouse-2))
  "Handle a `S-mouse-2' EVENT on the tabkit line.
Call `tabkit-split-window-below' with tab at event position."
  (tabkit-split-window-below (tabkit-posn-tab (event-end event))))

(cl-defmethod tabkit-line-handle-event ((event s-mouse-2))
  "Handle a `s-mouse-2' EVENT on the tabkit line.
Call `tabkit-split-window-right' with tab at event position."
  (tabkit-split-window-right (tabkit-posn-tab (event-end event))))

(cl-defmethod tabkit-line-handle-event ((event wheel-up))
  "Handle a `wheel-up' EVENT on the tabkit line.
Call `tabkit-backward' with received event."
  (tabkit-backward event))

(cl-defmethod tabkit-line-handle-event ((event C-wheel-up))
  "Handle a `C-wheel-up' EVENT on the tabkit line.
Call `tabkit-backward-tab' with received event."
  (tabkit-backward-tab event))

(cl-defmethod tabkit-line-handle-event ((event S-wheel-up))
  "Handle a `S-wheel-up' EVENT on the tabkit line.
Call `tabkit-backward-group' with received event."
  (tabkit-backward-group event))

(cl-defmethod tabkit-line-handle-event ((event wheel-down))
  "Handle a `wheel-down' EVENT on the tabkit line.
Call `tabkit-forward' with received event."
  (tabkit-forward event))

(cl-defmethod tabkit-line-handle-event ((event C-wheel-down))
  "Handle a `C-wheel-down' EVENT on the tabkit line.
Call `tabkit-forward-tab' with received event."
  (tabkit-forward-tab event))

(cl-defmethod tabkit-line-handle-event ((event S-wheel-down))
  "Handle a `S-wheel-down' EVENT on the tabkit line.
Call `tabkit-forward-group' with received event."
  (tabkit-forward-group event))

;;; Separator
;;
(defcustom tabkit-separator 0
  "Separator used on the tabkit line.
That is, a space width in pixels.
Separator face is `tabkit-separator'."
  :type '(natnum :tag "Space width in pixels")
  :set #'tabkit--custom-set-and-refresh-elt)

;;; Separator composition
;;
(defun tabkit--separator ()
  "Return a separator from option `tabkit-separator' spec.
That is, a space string of the specified number of pixels width, whose
face is `tabkit-separator'."
  (tabkit-space tabkit-separator nil 'tabkit-separator))

;;; Buttons
;;
(defvar-keymap tabkit-button-map
  :doc "Default keymap of a button."
  :parent tabkit-line-map
  )

;;; Button composition
;;
(define-inline tabkit-button-at-pos (pos string)
  "Return the button object at POS in STRING, or nil if not a button."
  (inline-quote (get-text-property ,pos :button ,string)))

(defun tabkit-posn-button (position)
  "Return the button object of POSITION, or nil if not a button.
POSITION should be a list of the form returned by the `event-start'
and `event-end' functions."
  (when-let* ((target (posn-string position)))
    (tabkit-button-at-pos (cdr target) (car target))))

(cl-defgeneric tabkit--button-create (_ name ui keymap)
  "Return a new simple button with NAME, UI and KEYMAP."
  (tabkit-text (tabkit-icon ui 'tabkit-button-icon)
               nil
               (list :button name
                     'help-echo #'tabkit--button-help-handler
                     'keymap (or keymap tabkit-line-map)
                     'mouse-face (if keymap 'tabkit-button-highlight)
                     ;; Don't turn mouse-1 into mouse-2
                     'follow-link #'ignore
                     'pointer 'arrow)))

(cl-defmethod tabkit--button-create ((_ (eql 'cond)) name ui keymap)
  "Return a new conditional button with NAME, UI and KEYMAP."
  (cons
   ;; Enabled
   (tabkit--button-create 'simple name (car ui) keymap)
   ;; Disabled
   (tabkit--button-create 'simple name (cdr ui) nil)))

(cl-defmethod tabkit--button-create ((_ (eql 'toggle)) name ui keymap)
  "Return a new toggle button with NAME, UI and KEYMAP."
  (cons
   ;; State1
   (tabkit--button-create 'simple name (car ui) keymap)
   ;; State2
   (tabkit--button-create 'simple name (cdr ui) keymap)))

(defun tabkit--button-property (name prop)
  "Return button NAME property PROP.
Return nil is button is not present on the tabkit line displayed in
the selected window, or if it does not have a property PROP."
  (when-let* ((line (tabkit-window-local :line))
              (pos (text-property-any 0 (length line) :button name line)))
    (get-text-property pos prop line)))

(define-inline tabkit--button-active-p (name)
  "Return non-nil if button NAME is active.
That is, if button is present on the tabkit line displayed in the
selected window and can be clicked on."
  ;; Assume a valid mouse-face property means an active button.
  (inline-quote
   (and (facep (tabkit--button-property ,name 'mouse-face)) t)))

(cl-defgeneric tabkit-button-help (_name _window _object _pos)
  "Return the help-echo string for the home button.
No help-echo by default."
  nil)

(defun tabkit--button-help-handler (window object pos)
  "In WINDOW, return the help string for button OBJECT at POS.
Call `tabkit-button-help' if there is a button OBJECT at POS."
  (when-let* ((name (get-text-property pos :button object)))
    (tabkit-button-help name window object pos)))

;;; Home button
;;
(defvar-keymap tabkit-home-button-map
  :doc "Default keymap of the home button."
  :parent tabkit-button-map
  (tabkit-key 'mouse-1) #'tabkit-home
  (tabkit-key 'C 'mouse-1) #'tabkit-goto-selected-tab
  )

(defcustom tabkit-home-button-ui
  '(
    ((:symbol "  ▼  ")) ;; BLACK DOWN-POINTING TRIANGLE (U+25BC)
    .
    ((:symbol "  ▲  ")) ;; BLACK UP-POINTING TRIANGLE (U+25B2)
    )
  "The home toggle button icons.
That is, a pair (STATE1 . STATE2) where STATE1 is the button icon
when showing group tabs, and STATE2 when showing individual tabs."
  :type 'tabkit-icon-toggle
  :set #'tabkit--custom-set-and-refresh-elt)

(cl-defmethod tabkit-button ((name (eql 'home)))
  "Create and return a new NAME button (`home')."
  (if tabkit-home-button-ui
      (tabkit--button-create 'toggle name
                             tabkit-home-button-ui
                             tabkit-home-button-map)))

(defconst tabkit--home-help-group-on
  (tabkit-text-render
   "<d>
    <k1>mouse-1</k1>: show tabs in current group<br>
    <k1>C-mouse-1</k1>: go to the selected tab<br>
    %s
    </d>"
   tabkit-line-help)
  "Home button help string when showing groups.")

(defconst tabkit--home-help-group-off
  (tabkit-text-render
   "<d>
    <k1>mouse-1</k1>: show group tabs<br>
    <k1>C-mouse-1</k1>: go to the selected tab<br>
    %s
    </d>"
   tabkit-line-help)
  "Home button help string when showing buffers.")

(cl-defmethod tabkit-button-help ((_ (eql 'home)) window &rest)
  "Return the help-echo string for the home button.
WINDOW is the window in which the help was found."
  (if (tabkit-showing-groups window)
      tabkit--home-help-group-on
    tabkit--home-help-group-off))

(defun tabkit-toggle-show-groups (&optional event)
  "Toggle the display of group tabs on window receiving EVENT."
  (interactive (list last-input-event))
  (+-with-selected-window
      (window-normalize-window (posn-window (event-start event)))
    (setf (tabkit-showing-groups) (not (tabkit-showing-groups)))))

(defun tabkit-home (&optional event)
  "Toggle display of group tabs on window receiving EVENT."
  (interactive (list last-input-event))
  (gmouse-set-current-event event)
  (+-with-selected-window
      (window-normalize-window (posn-window (event-start event)))
    (tabkit-toggle-show-groups event)))

;;; Scroll left button
;;
(defvar-keymap tabkit-scroll-left-button-map
  :doc "Default keymap of the scroll left button."
  :parent tabkit-button-map
  (tabkit-key 'mouse-1) #'tabkit-scroll-left-line
  (tabkit-key 'C 'mouse-1) #'tabkit-scroll-left
  (tabkit-key 'S 'mouse-1) #'tabkit-scroll-left-end
  )

(defcustom tabkit-scroll-left-button-ui
  '(
    ((:symbol "  ◀  ")) ;; BLACK LEFT-POINTING TRIANGLE (U+25C0)
    .
    ((:symbol "  ◁  ")) ;; WHITE LEFT-POINTING TRIANGLE (U+25C1)
    )
  "The scroll left conditional button icons.
That is, a pair (ENABLED . DISABLED) where ENABLED, DISABLED are
respectively the enabled, disabled button icons."
  :type 'tabkit-icon-conditional
  :set #'tabkit--custom-set-and-refresh-elt)

(cl-defmethod tabkit-button ((name (eql 'scroll-left)))
  "Create and return a new button NAME (`scroll-left')."
  (if tabkit-scroll-left-button-ui
      (tabkit--button-create 'cond name
                             tabkit-scroll-left-button-ui
                             tabkit-scroll-left-button-map)))

(defconst tabkit--scroll-left-help
  (tabkit-text-render
   "<d>
    <k1>mouse-1</k1>: scroll line to the left<br>
    <k1>C-mouse-1</k1>: scroll one tab to the left<br>
    <k1>S-mouse-1</k1>: scroll to the left end<br>
    %s
    </d>"
   tabkit-line-help)
  "Help string shown when mouse hovers the scroll left button.")

(cl-defmethod tabkit-button-help ((_ (eql 'scroll-left)) &rest)
  "On mouse EVENT, scroll current tabset on left."
  tabkit--scroll-left-help)

(defun tabkit-scroll-left (&optional event)
  "Scroll one tab to the left on window receiving EVENT."
  (interactive (list last-input-event))
  (gmouse-set-current-event event)
  (+-with-selected-window
      (window-normalize-window (posn-window (event-start event)))
    (tabkit-tabset-scroll (tabkit-current-tabset) -1)
    (tabkit-display-update)))

(defun tabkit-scroll-left-line (&optional event)
  "Scroll the whole tabkit line on left on window receiving EVENT.
That is, scroll tabs to the left until the tab that was originally at
left end ends up at right end, or the first available tab is at left
end."
  (interactive (list last-input-event))
  (gmouse-set-current-event event)
  (+-with-selected-window
      (window-normalize-window (posn-window (event-start event)))
    (let ((tabset (tabkit-current-tabset))
          (end (car (tabkit-visible-tabs))))
      (while (and (tabkit-tabset-scroll tabset -1)
                  (not (memq end (last (tabkit-visible-tabs))))))
      (tabkit-display-update))))

(defun tabkit-scroll-left-end (&optional event)
  "Scroll to left end of the tabkit line on window receiving EVENT."
  (interactive (list last-input-event))
  (gmouse-set-current-event event)
  (+-with-selected-window
      (window-normalize-window (posn-window (event-start event)))
    (let ((tabset (tabkit-current-tabset)))
      ;; To avoid an infinite loop, must clear the index of tab
      ;; partially visible at left end, which overrides any left
      ;; scrolling!
      (setf (tabkit-tabset-prop tabset :start0) nil)
      (while (tabkit-tabset-scroll tabset -1))
      (tabkit-display-update))))

;;; Scroll right button
;;
(defvar-keymap tabkit-scroll-right-button-map
  :doc "Default keymap of the scroll right button."
  :parent tabkit-button-map
  (tabkit-key 'mouse-1) #'tabkit-scroll-right-line
  (tabkit-key 'C 'mouse-1) #'tabkit-scroll-right
  (tabkit-key 'S 'mouse-1) #'tabkit-scroll-right-end
  )

(defcustom tabkit-scroll-right-button-ui
  '(
    ((:symbol "  ▶  ")) ;; BLACK LEFT-POINTING TRIANGLE (U+25C0)
    .
    ((:symbol "  ▷  ")) ;; WHITE LEFT-POINTING TRIANGLE (U+25C1)
    )
  "The scroll right conditional button icons.
That is, a pair (ENABLED . DISABLED) where ENABLED, DISABLED are
respectively the enabled, disabled button icons."
  :type 'tabkit-icon-conditional
  :set #'tabkit--custom-set-and-refresh-elt)

(cl-defmethod tabkit-button ((name (eql 'scroll-right)))
  "Create and return a new button NAME (`scroll-right')."
  (if tabkit-scroll-right-button-ui
      (tabkit--button-create 'cond name
                             tabkit-scroll-right-button-ui
                             tabkit-scroll-right-button-map)))

(defconst tabkit--scroll-right-help
  (tabkit-text-render
   "<d>
    <k1>mouse-1</k1>: scroll line to the right<br>
    <k1>C-mouse-1</k1>: scroll one tab to the right<br>
    <k1>S-mouse-1</k1>: scroll to the right end<br>
     %s
     </d>"
   tabkit-line-help)
  "Help string shown when mouse hovers the scroll right button.")

(cl-defmethod tabkit-button-help ((_ (eql 'scroll-right)) &rest)
  "Return the help-echo string for the scroll right button."
  tabkit--scroll-right-help)

(defun tabkit-scroll-right (&optional event)
  "Scroll one tab to the right on window receiving EVENT."
  (interactive (list last-input-event))
  (gmouse-set-current-event event)
  (+-with-selected-window
      (window-normalize-window (posn-window (event-start event)))
    (tabkit-tabset-scroll (tabkit-current-tabset) 1)
    (tabkit-display-update)))

(defun tabkit-scroll-right-line (&optional event)
  "Scroll the whole tabkit line on right on window receiving EVENT.
That is, scroll tabs to the right until the tab that was originally at
right end ends up at left end, or the last available tab is at right
end."
  (interactive (list last-input-event))
  (gmouse-set-current-event event)
  (+-with-selected-window
      (window-normalize-window (posn-window (event-start event)))
    (let ((tabset (tabkit-current-tabset))
          (end (car (last (tabkit-visible-tabs)))))
      (while (and (tabkit-tabset-scroll tabset 1)
                  (not (eq end (car (tabkit-visible-tabs))))))
      (tabkit-display-update))))

(defun tabkit-scroll-right-end (&optional event)
  "Scroll to right end of the tabkit line on window receiving EVENT."
  (interactive (list last-input-event))
  (gmouse-set-current-event event)
  (+-with-selected-window
      (window-normalize-window (posn-window (event-start event)))
    (let ((tabset (tabkit-current-tabset)))
      (while (tabkit-tabset-scroll tabset 1))
      (tabkit-display-update))))

;;; Newtab button
;;
(defvar-keymap tabkit-newtab-button-map
  :doc "Default keymap of the newtab button."
  :parent tabkit-button-map
  (tabkit-key 'mouse-1) #'tabkit-newtab-menu-popup
  )

(defcustom tabkit-newtab-button-ui
  '((:symbol " ＋ ")) ;; FULLWIDTH PLUS SIGN (U+FF0B)
  "The newtab simple button icon."
  :type 'tabkit-icon-simple
  :set #'tabkit--custom-set-and-refresh-elt)

(cl-defmethod tabkit-button ((name (eql 'newtab)))
  "Create and return a new button NAME (`newtab')."
  (if tabkit-newtab-button-ui
      (tabkit--button-create 'simple name
                             tabkit-newtab-button-ui
                             tabkit-newtab-button-map)))

(defconst tabkit--newtab-help
  (tabkit-text-render
   "<d>
     <k1>mouse-1</k1>: open a new tab<br>
     %s
     </d>"
   tabkit-line-help)
  "Help string shown when mouse hovers the new tab button.")

(cl-defmethod tabkit-button-help ((_ (eql 'newtab)) &rest)
  "Return the help-echo string for the new tab button."
  tabkit--newtab-help)

(defun tabkit-newtab-menu-popup (&optional event)
  "Popup the newtab menu on received EVENT."
  (interactive (list last-input-event))
  (gmouse-set-current-event event)
  (tabkit-menu-value-popup 'tabkit-buffer-newtab-menu event))

;;; Tabs
;;
(defvar tabkit-tab-min-width 180
  "Minimum width of tab in pixels.
See also the option `tabkit-tab-width'.")

(defvar tabkit-tab-max-width 320
  "Maximum width of tab in pixels.
See also the option `tabkit-tab-width'.")

(defcustom tabkit-tab-width '(0 nil nil)
  "Width of a tab.
If nil, the tab width varies with the label length.  Otherwise tab
width is specified by a list (WIDTH TYPE ELLIPSIS), where:

WIDTH is either:
- `min' for `tabkit-tab-min-width';
- `max' for `tabkit-tab-max-width';
- 0 (default) for the total width available for tabs, divided by the
  number of tabs;
- a positive integer for this number of pixels;
- a negative integer for the total width available for tabs, divided
  by the absolute value of this number;

`tabkit-tab-min-width' <= WIDTH <= `tabkit-tab-max-width'.

TYPE is either:
- nil (default) for fixed width: all tabs have the same WIDTH and
  labels are truncated or padded accordingly;
- t for maximum width: labels larger than WIDTH are truncated to
  WIDTH, otherwise the width varies with the label length).

ELLIPSIS is either:
- nil (default) to use the default ellipsis string at end of
  truncated label;
- a string to use this string at end of truncated label;
- `fade' to fade the end of truncated label."
  :type `(choice :tag ""
                 :format ,(concat "%{%t%}"
                                  (propertize ":" 'display "")
                                  " %[Value Menu%] %v\n")
                 (const :tag "Label length" nil)
                 (list  :tag "As specified" :extra-offset 3
                        :format "%{%t%}\n\n%v"
                        (choice  :tag "Width   "
                                 (const :tag "minimum" min)
                                 (const :tag "maximum" max)
                                 (integer))
                        (choice  :tag "Type    "
                                 (const :tag "fixed width" nil)
                                 (const :tag "maximum width" t))
                        (choice  :tag "Ellipsis"
                                 (const  :tag "default" nil)
                                 (string :tag "string")
                                 (const  :tag "fade" fade)))))

(defun tabkit-tab-width (width tabs)
  "Return computed value of option `tabkit-tab-width'.
That is, a list (PIXEL-WIDTH TYPE ELLIPSIS), or nil.
WIDTH is the total number of pixels currently available for tabs in
the visible area, and TABS is the list of tabs."
  (when-let* ((tabw (car tabkit-tab-width)))
    (cons (pcase tabw
            ;; WIDTH/(length TABS).
            (0
             (max (min (/ width (max 1 (length tabs)))
                       tabkit-tab-max-width)
                  tabkit-tab-min-width))
            ;; Min width.
            ('min tabkit-tab-min-width)
            ;; Max width.
            ('max tabkit-tab-max-width)
            ;; Number of pixels.
            ((pred natnump)
             (max tabw tabkit-tab-min-width))
            ;; Fraction of WIDTH.
            (_
             (max (min (/ width (abs tabw)) tabkit-tab-max-width)
                  tabkit-tab-min-width)))
          (cdr tabkit-tab-width))))

;;; Tab icon
;;
(defcustom tabkit-tab-icon-default-icon
  '((:emoji "♈")
    (:image ((:file "icons/hicolor/scalable/apps/emacs.svg"))))
  "The default tab icon."
  :type 'tabkit-icon-simple
  :set #'tabkit--custom-set-and-refresh-elt)

(defcustom tabkit-tab-icon-custom-icons
  '(
    ("Customize"       (:emoji "⚙️"))
    ("Dired"           (:emoji "📁"))
    ("Terminal"        (:emoji "💻"))
    ("Debugger"        (:emoji "🆘"))
    ("Help"            (:emoji "🛟"))
    ("Mail"            (:emoji "📧"))
    ("File"            (:emoji "📄"))
    ("Process"         (:emoji "⌛"))
    ("Version Control" (:emoji "📚"))
    ("Other"           (:emoji "👽"))
    )
  "Custom icon name/icon associations.
This is a list of (TAB-ICON-NAME . ICON-GLYPHS) associations, where
TAB-ICON-NAME is a string returned by `tabkit-tab-icon-name', and
ICON-GLYPHS is a list of icon glyphs accepted by `define-pic-icon'.

The default custom icon names are the default group names defined in
option `tabkit-buffer-group-criteria'.  It make sense to customize
both options at the same time."
  :type `(repeat :tag "Tab Icon Name/Icon Glyphs associations"
                 (cons
                  :tag ""
                  (string :sample-face 'bold :tag "Tab Icon Name")
                  (pic-icon-glyphs :tag "Icon Glyphs")))
  :set #'tabkit--custom-set-and-refresh-all)

(defcustom tabkit-tab-pin-icon
  '((:emoji "📌")) ;; STAR WITH RIGHT HALF BLACK (U+2BEB)
  "Default icon used to represent a pinned tab.
Normally the tab icon is used to represent a pinned tab, and this icon
is used instead when the icon tab is missing.  Unless the option
`tabkit-tab-prefer-pin-icon' is enabled, in which case this icon is
always used."
  :type 'tabkit-icon-simple
  :set #'tabkit--custom-set-and-refresh-elt)

(defcustom tabkit-tab-prefer-pin-icon nil
  "Non-nil to prefer the default pin icon to represent a pinned tab.
See also the option `tabkit-tab-pin-icon'."
  :type 'boolean)

(defmacro tabkit--tab-icon-default-icon ()
  "Return the default tab icon construct.
The returned value is a propertized string for both the text and image
representation of a tab icon."
  `(tabkit-with-memo 'tabkit--tab-icon-default-icon
     (tabkit-icon tabkit-tab-icon-default-icon 'tabkit-tab-icon
                  :margin 0)))

(defun tabkit--tab-icon-cache ()
  "Return a cache to store icons.
That is a hash table to associate an icon name to its value."
  (tabkit-with-memo 'tabkit--tab-icon-cache
    (let ((cache (make-hash-table :test 'equal :size 101)))
      ;; Initialize cache with custom icons.
      (dolist (ielt tabkit-tab-icon-custom-icons)
        (puthash (car ielt)
                 (tabkit-icon (cdr ielt) 'tabkit-tab-icon :margin 0)
                 cache))
      cache)))

(cl-defgeneric tabkit-tab-icon (tab)
  "Return an icon for TAB.
Return nil if `tabkit-show-tab-icons' is disabled, otherwise return
either the icon whose `tabkit-tab-icon-name' is found in
`tabkit-tab-icon-custom-icons'; or, if not found, the default icon
defined in `tabkit-tab-icon-default-icon'."
  (when tabkit-show-tab-icons
    (gethash (tabkit-tab-icon-name tab)
             (tabkit--tab-icon-cache)
             (tabkit--tab-icon-default-icon))))

(cl-defmethod tabkit-tab-icon ((tab tabkit--buffer-tab))
  "Return an icon for buffer TAB.
Return nil if `tabkit-show-tab-icons' is disabled, otherwise, if TAB
buffer is visiting a file, return a mime-type icon if possible; or an
icon in `tabkit-tab-icon-custom-icons'; or, the default icon defined
in `tabkit-tab-icon-default-icon'."
  (when tabkit-show-tab-icons
    (propertize
     (cond
      ;; Group tab.
      ((tabkit-showing-groups)
       (cl-call-next-method))
      ;; File buffer tab.
      ((when-let* ((file (buffer-file-name (tabkit-tab-value tab)))
                   (type (and (fboundp 'xdg-mime-file-type)
                              (tabkit-with-stashed-tab-property
                                  tab :file-type
                                (xdg-mime-file-type file))))
                   (icon (gethash type (tabkit--tab-icon-cache) 'new)))
         (if (eq icon 'new)
             (let ((im (tabkit-icon `((:image ((:mime-type ,type))))
                                    'tabkit-tab-icon
                                    :margin 0)))
               (puthash type (or im (cl-call-next-method))
                        (tabkit--tab-icon-cache)))
           icon)))
      ;; Other buffer tab.
      ((cl-call-next-method)))
     'help-echo (tabkit-tab-icon-help tab))))

(cl-defgeneric tabkit-tab-icon-help (tab)
  "Return a help-echo string for TAB icon."
  (tabkit-text-render (tabkit-tab-group-name-help tab)))

(cl-defmethod tabkit-tab-icon-help ((tab tabkit--buffer-tab))
  "Return a help-echo string for buffer TAB icon."
  (if (tabkit-showing-groups)
      (tabkit-tab-help tab)
    (if-let* ((type (tabkit-tab-property tab :file-type)))
        (tabkit-text-render
         "%s<br>Type %s" (tabkit-tab-group-name-help tab) type)
      (cl-call-next-method))))

;;; Tab label
;;
(defvar-keymap tabkit-tab-map
  :doc "Default keymap of a tab."
  :parent tabkit-line-map
  (tabkit-key 'down-mouse-1) #'tabkit-tab-press
  (tabkit-key      'mouse-1) #'tabkit-tab-select-and-show-tabs
  (tabkit-key      'mouse-3) #'tabkit-tab-menu-popup
  ;; Ctl-mouse-1
  (tabkit-key 'C   'mouse-1) #'tabkit-mark-tab
  ;; Shift-mouse-1
  (tabkit-key 'S   'mouse-1) #'tabkit-mark-tab-range
  ;; Ctl-mouse-3
  (tabkit-key 'C   'mouse-3) #'tabkit-unmark-tab
  ;; Shift-mouse-3
  (tabkit-key 'S   'mouse-3) #'tabkit-unmark-marked-tabs
  ;; Click on tab badge icons
  "<tabkit-tab-marked> <t>"              #'tabkit-line-event-handler
  "<tabkit-tab-marked> <down-mouse-1>"   #'tabkit-tab-press
  "<tabkit-tab-marked> <mouse-1>"        #'tabkit-unmark-tab
  "<tabkit-tab-marked> <mouse-3>"        #'tabkit-tab-menu-popup
  "<tabkit-tab-modified> <t>"            #'tabkit-line-event-handler
  "<tabkit-tab-modified> <down-mouse-1>" #'tabkit-tab-press
  "<tabkit-tab-modified> <mouse-1>"      #'tabkit-tab-save-buffer
  "<tabkit-tab-modified> <mouse-3>"      #'tabkit-tab-menu-popup
  )

(defcustom tabkit-tab-drag-scroll-delay 0.5
  "The pause between scroll steps caused by tab drags, in seconds.
If you drag a tab and the mouse hovers an active scroll button, the
tabs scroll with a delay of that many seconds between each scroll
step.  Scrolling stops when the mouse leaves the scroll button or the
mouse button is released.
This variable's value may be non-integral.  Setting this to zero
causes scroll tabs as fast as possible."
  :type 'number)

(defun tabkit-tab-press (event)
  "Handle mouse button press EVENT on TAB.
That is, either respond to a long mouse button press, or drag TAB to
another position following mouse movements."
  (interactive "@e")
  ;; Call global handler to schedule processing of long press event.
  (tabkit-line-event-handler event)
  (let* ((tab (tabkit-posn-tab (event-start event)))
         (window (posn-window (event-start event)))
         (otrack track-mouse)
         (moving (format "Moving tab %s..."
                         (propertize (tabkit-tab-display-name tab)
                                     'face '( :foreground "red"
                                              :weight bold))))
         ;; Unconditionally deactivate transient keymap.
         (exitfun nil)
         ;; Cleanup after transient keymap is deactivated.
         (clearfun
          (lambda ()
            (setf (tabkit-dragged-tab) nil)
            (setq track-mouse otrack)
            (tabkit-display-update)))
         ;; Transient keymap to handle mouse-movement events.
         (keymap
          (define-keymap "<mouse-movement>"
            (lambda (e)
              (interactive "@e")
              ;; If initial EVENT was a successful long mouse button
              ;; press, or TAB is pinned, or showing group tabs, stop
              ;; dragging.
              (if (or (gmouse-long-press-completed)
                      (tabkit-tab-pinned-p tab))
                  (funcall exitfun)
                (gmouse-set-current-event e)
                (let* ((pos (event-end e))
                       (to  (tabkit-posn-tab pos))
                       ;; Don't log tracking messages.
                       message-log-max)
                  (setf (tabkit-dragged-tab) tab)
                  (message moving)
                  (cond
                   ;; Don't move if hover another window.
                   ((not (eq (posn-window pos) window)))
                   ;; Scroll tabs if hover an active scroll button.
                   ((null to)
                    (let ((btn (tabkit-posn-button pos)))
                      (cond
                       ((and (eq btn 'scroll-left)
                             (tabkit--button-active-p btn))
                        (tabkit-scroll-left)
                        ;; Pause before next scroll step.
                        (sleep-for tabkit-tab-drag-scroll-delay))
                       ((and (eq btn 'scroll-right)
                             (tabkit--button-active-p btn))
                        (tabkit-scroll-right)
                        ;; Pause before next scroll step.
                        (sleep-for tabkit-tab-drag-scroll-delay)))))
                   ;; Don't move to a pinned tab.
                   ((tabkit-tab-pinned-p to))
                   ;; Move tab.
                   ((or (tabkit-move-tab-next-to-tab tab to)
                        (tabkit-display-update))))))))))
    (setq track-mouse t ;; Use t to change the mouse pointer shape
                        ;; during mouse motion.
          exitfun (set-transient-map keymap t clearfun))))

(defun tabkit--tab-help-handler (window object position)
  "Return a help string for the tab under the mouse.
Return nil for no help string.  WINDOW is the window in which the help
was found.  OBJECT is the tab label under the mouse.  POSITION is the
position in that label.  Call `tabkit-tab-help' with the associated
tab."
  (+-with-selected-window window
    (tabkit-tab-help (tabkit-tab-at-pos position object))))

(cl-defmethod tabkit-tab-select ((tab tabkit--tab))
  "Make TAB the selected tab."
  (let ((tabset (if (tabkit-showing-groups)
                    tabkit--meta-tabset
                  (tabkit-tab-tabset tab))))
    ;; Mark TAB as selected in the current window.
    (tabkit-select-tab tab tabset)
    ;; Record the last selected tabset in the selected window.
    (setf (tabkit-last-tabset) tabset)
    ;; Force hscrolling to the selected tab.
    (tabkit-tabset-set-hscroll tabset 'force)))

(cl-defmethod tabkit-tab-select ((tab tabkit--buffer-tab))
  "Switch to TAB buffer and make TAB selected in selected window.
Return non-nil on success."
  (when (buffer-live-p (tabkit-tab-value tab))
    (pop-to-buffer-same-window (tabkit-tab-value tab))
    ;; Call next method to make TAB the selected tab.
    (cl-call-next-method)))

(cl-defgeneric tabkit-tab-help (_tab)
  "Return a help-echo string for TAB.
No help-echo by default."
  nil)

(cl-defmethod tabkit-tab-help ((tab tabkit--tab))
  "Return a help-echo string for TAB.
Return the TAB display name."
  (tabkit-tab-display-name tab))

(cl-defmethod tabkit-tab-help ((tab tabkit--buffer-tab))
  "Return a help-echo string for buffer TAB."
  (let (mouse1 title)
    (if (tabkit-showing-groups)
        (setq title (tabkit-tab-group-name-help tab)
              mouse1 "show tabs in this group")
      (setq title (tabkit-tab-name-help tab)
            mouse1 "select this tab"))
    (tabkit-text-render
     "%s
      <d>
      <k1>mouse-1</k1>: %s<br>
      <k1>mouse-3</k1>: tab menu<br>
      %s
      </d>"
     title mouse1 tabkit-line-help)))

;;; Tab mouse commands
;;
(defun tabkit-tab-select-and-show-tabs (event)
  "Select tab at EVENT position and show individual tabs.
That is, if TAB is successfully selected, and the tabkit line is
showing group tabs, unconditionally toggle showing of individual tabs."
  (interactive "@e")
  (gmouse-set-current-event event)
  (when-let* ((tab (tabkit-posn-tab (event-end event))))
    (setf (tabkit-showing-groups) nil)
    (tabkit-tab-select tab)))

(defun tabkit-tab-menu-popup (event)
  "Popup TAB menu on received EVENT."
  (interactive "@e")
  (gmouse-set-current-event event)
  (when-let* ((tab (tabkit-posn-tab (event-end event))))
    (tabkit-menu-popup (tabkit-tab-menu tab) event)))

(defun tabkit-tab-save-buffer (event)
  "Save buffer in tab at EVENT position.
Call `save-buffer' after confirmation."
  (interactive "@e")
  (gmouse-set-current-event event)
  (when-let* ((tab (tabkit-posn-tab (event-end event)))
              (buf (tabkit-tab-value tab))
              (bfn (buffer-file-name buf))
              ((y-or-n-p (format "Save file %s?"
                                 (abbreviate-file-name bfn)))))
    (with-current-buffer buf
      (save-buffer))))

(defun tabkit-mark-tab (event)
  "Mark the tab at EVENT position."
  (interactive "@e")
  (gmouse-set-current-event event)
  (when-let* ((tab (tabkit-posn-tab (event-end event))))
    (if (tabkit-tab-mark tab)
        (message "Tab %s marked" (tabkit-tab-display-name tab)))
    (tabkit-display-update)))

(defun tabkit-mark-tab-range (event)
  "Mark tabs between tab at EVENT position and the selected tab."
  (interactive "@e")
  (gmouse-set-current-event event)
  (when-let* ((tab (tabkit-posn-tab (event-end event)))
              (n (tabkit-mark-range-from-tab tab)))
    (message "%d tab%s marked" n (if (> n 1) "s" ""))
    (tabkit-display-update)))

(defun tabkit-unmark-tab (event)
  "Unmark tab at EVENT position."
  (interactive "@e")
  (gmouse-set-current-event event)
  (when-let* ((tab (tabkit-posn-tab (event-end event)))
              (_ (tabkit-tab-unmark tab)))
    (message "Tab %s unmarked" (tabkit-tab-display-name tab)))
    (tabkit-display-update))

;;; Tab close button
;;
(defvar-keymap tabkit-tab-close-button-map
  :doc "Default keymap of a tab close button."
  :parent tabkit-button-map
  (tabkit-key 'mouse-1) #'tabkit-tab-close-handler
  (tabkit-key 'C 'mouse-1) #'tabkit-tab-close-handler
  )

(defcustom tabkit-tab-close-button-ui
  '((:symbol "✕")) ;; MULTIPLICATION X (U+2715)
  "The tab close simple button icon."
  :type 'tabkit-icon-simple
  :set #'tabkit--custom-set-and-refresh-elt)

(defun tabkit-tab-close-handler (event)
  "Handle a mouse EVENT on a tab close button.
Call `tabkit-tab-close' with the tab at EVENT position.
Also try to close the selected window if the control key is pressed."
  (interactive "@e")
  ;; Does nothing if event type has changed between pressing and
  ;; releasing the mouse button (for example, when mouse-2 is
  ;; received, produced by the follow link machinery from an original
  ;; mouse-1 event).
  (when (and (eq last-command 'gmouse-set-current-event)
             (eq (event-basic-type event)
                 (event-basic-type (gmouse-current-event))))
    (gmouse-set-current-event event)
    (tabkit-tab-close (tabkit-posn-tab (event-end event))
                      ;; Also try to close the selected window if the
                      ;; control key was also pressed.
                      (memq 'control (event-modifiers event)))))

(cl-defmethod tabkit-button ((name (eql 'tab-close)))
  "Create and return a new button NAME (`tab-close')."
  (if tabkit-tab-close-button-ui
      (tabkit--button-create 'simple name
                             tabkit-tab-close-button-ui
                             tabkit-tab-close-button-map)))

(cl-defmethod tabkit-button-help ((_ (eql 'tab-close)) window object pos)
  "Return the help-echo string for the tab close button.
WINDOW is the window in which the help was found.  OBJECT is the tab
component under the mouse.  POS is the position in that label.
Call `tabkit-tab-close-help' with the tab object at POS."
  (+-with-selected-window window
    (tabkit-tab-close-help (tabkit-tab-at-pos pos object))))

(cl-defmethod tabkit-tab-close-button ((_tab tabkit--tab))
  "Return a close button for TAB, or nil if no close button.
Return the button defined by option `tabkit-tab-close-button-ui'.  The
close button is the same regardless of TAB."
  (tabkit-with-memo 'tabkit-tab-close-button-ui
    (tabkit-button 'tab-close)))

(defun tabkit--close-single-tabs (tabs)
  "Perform close action on single TABS."
  (mapc #'tabkit-tab-close-single tabs)
  (tabkit-display-update))

(cl-defmethod tabkit-tab-close-single ((tab tabkit--tab))
  "Delete TAB from parent tabset."
  (tabkit-delete-tab tab)
  (tabkit-display-update))

(cl-defmethod tabkit-tab-close-single ((tab tabkit--buffer-tab))
  "Kill buffer in TAB and delete TAB.
Return non-nil if successful."
  (let ((stab (tabkit-tab-serialize tab))
        (buf  (tabkit-tab-value tab)))
    (kill-buffer buf)
    ;; When buffer is actually killed:
    (unless (buffer-live-p buf)
      ;; Push serializable form of TAB in recently closed tabs.
      (if stab (tabkit--closed-tab-push stab))
      ;; Delete TAB.
      (cl-call-next-method))))

(defun tabkit--close-group-tabs (tabs)
  "Perform close action on group TABS."
  (mapc #'tabkit-tab-close-group tabs)
  (tabkit-display-update))

(cl-defmethod tabkit-tab-close-group ((tab tabkit--tab))
  "Close group TAB on received EVENT.
Close all tabs in the group.
Return non-nil if the group no more exists."
  (let* ((tabset (tabkit-tab-tabset tab))
         (group  (tabkit-tabset-name tabset)))
    (tabkit--close-single-tabs (tabkit-global-tabs tabset))
    ;; Return non-nil if the group no more exists (that is, all member
    ;; tabs were actually closed).
    (null (tabkit-tabset group))))

(cl-defgeneric tabkit-tab-close (tab &optional close-window)
  "Handle close action on TAB.
If TAB is a group tab, close all tabs in the group, otherwise close
this single tab.
If optional argument CLOSE-WINDOW is non-nil, also close the selected
window when possible."
  (and (if (tabkit-showing-groups)
           (tabkit-tab-close-group tab)
         (tabkit-tab-close-single tab))
       close-window
       (ignore-errors (delete-window))))

(cl-defgeneric tabkit-tab-close-help (_tab)
  "Return a help-echo string for TAB close button."
  (let ((type (if (tabkit-showing-groups) "group" "tab")))
    (tabkit-text-render
     "<d><k1>mouse-1</k1>: close this %s<br>%s%s</d>"
     type
     (if (one-window-p t)
         ""
       (format "<k1>C-mouse-1</k1>: close both this %s and window<br>"
               type))
     tabkit-line-help)))

;;; Tab menu
;;
(cl-defmethod tabkit-tab-menu ((tab tabkit--tab))
  "Return TAB menu."
  (let* ((split  (not (one-window-p t)))
         (group  (tabkit-tab-showing-group tab))
         (show-g (and group t))
         (marked (tabkit-marked-tabs))
         (pinned (tabkit-tab-pinned-p tab))
         (icolor (and (tabkit-tab-instant-color tab t) t))
         (around (unless (or show-g pinned)
                   (tabkit-tabs-around
                    tab (tabkit-tabset-view
                         (tabkit-tab-tabset tab))))))
    (easy-menu-create-menu
     nil
     `(
       ,@(when marked
           `(("Marked tabs"
              ["Unmark"
               ,(lambda ()
                  (interactive)
                  (tabkit-unmark-marked-tabs marked))]
              ["Pin"
               ,(lambda ()
                  (interactive)
                  (tabkit-pin-marked-tabs marked))
               :visible ,(not show-g)]
              ["Unpin"
               ,(lambda ()
                  (interactive)
                  (tabkit-unpin-marked-tabs marked))
               :visible ,(not show-g)]
              ["Change color..."
               ,(lambda ()
                  (interactive)
                  (tabkit-change-color-of-marked-tabs marked))]
              ["Reset color"
               ,(lambda ()
                  (interactive)
                  (tabkit-reset-color-of-marked-tabs marked))]
              ,@(unless show-g
                  `(,menu-bar-separator
                    ["Move to another group..."
                     ,(lambda ()
                        (interactive)
                        (tabkit-move-to-group-marked-tabs marked))]
                    ["Copy to another group..."
                     ,(lambda ()
                        (interactive)
                        (tabkit-copy-to-group-marked-tabs marked))]
                    ["Remove from this group"
                     ,(lambda ()
                        (interactive)
                        (tabkit-remove-from-group-marked-tabs marked))]
                    ))
              ,menu-bar-separator
              ["Close"
               ,(lambda ()
                  (interactive)
                  (tabkit-close-marked-tabs marked))])
             ,menu-bar-separator))
       ["Place this tab next to another..."
        ,(lambda ()
           (interactive)
           (tabkit-move-tab-next-to-tab tab))
        :visible ,(not pinned)]
       ["Pin this tab"
        ,(lambda ()
           (interactive)
           (tabkit-tab-pin tab)
           (tabkit-display-update))
        :visible ,(and (not show-g) (not pinned))]
       ["Unpin this tab"
        ,(lambda ()
           (interactive)
           (tabkit-tab-unpin tab)
           (tabkit-display-update))
        :visible ,(and (not show-g) pinned)]
       ["Change color of this tab..."
        ,(lambda ()
           (interactive)
           (tabkit-tab-change-color tab)
           (tabkit-display-update))]
       ["Reset color of this tab"
        ,(lambda ()
           (interactive)
           (tabkit-tab-reset-color tab)
           (tabkit-display-update))
        :visible ,icolor]
       ,@(when (and tabkit-use-groups (not show-g))
           `(,menu-bar-separator
             ["Move this tab to another group..."
              ,(lambda ()
                 (interactive)
                 (tabkit-tab-move-to-group tab))
              :visible ,(not pinned)]
             ["Copy this tab to another group..."
              ,(lambda ()
                 (interactive)
                 (tabkit-tab-copy-to-group tab))]
             ["Remove this tab from this group"
              ,(lambda ()
                 (interactive)
                 (tabkit-tab-remove-from-group tab))
              :visible ,(not pinned)]
             ))
       ,menu-bar-separator
       ["Reset this group to default tabs"
        ,(lambda ()
           (interactive)
           (tabkit-group-reset group))
        :visible ,(and show-g (tabkit-instant-group-p group))
        ]
       [,(format "Close this %s" (if show-g "group" "tab"))
        ,(lambda ()
           (interactive)
           (tabkit-tab-close tab))]
       [,(format "Close this %s and window" (if show-g "group" "tab"))
        ,(lambda ()
           (interactive)
           (tabkit-tab-close tab t))
        :visible ,split]
       ("Close multiple tabs"
        :visible ,(if (or (car around) (cdr around)) t)
        ["Close tabs on left"
         ,(lambda ()
            (interactive)
            (tabkit--close-single-tabs (car around)))
         :active ,(and (not pinned) (car around) t)]
        ["Close tabs on right"
         ,(lambda ()
            (interactive)
            (tabkit--close-single-tabs (cdr around)))
         :active ,(and (not pinned) (cdr around) t)]
        ["Close other tabs"
         ,(lambda ()
            (interactive)
            (tabkit--close-single-tabs
             (append (car around) (cdr around))))]
        )))))

;;; Tabkit line construction
;;
(defsubst tabkit--components-initialize (&rest compositions)
  "Initialize the components in COMPOSITIONS."
  (dolist (components compositions)
    (mapc #'tabkit-component-initialize components)))

(defsubst tabkit--components-finalize (&rest compositions)
  "Finalize the components in COMPOSITIONS."
  (dolist (components compositions)
    (mapc #'tabkit-component-finalize components)))

(defun tabkit--custom-update-components (variable value)
  "Set composition option VARIABLE to VALUE.
Like `custom-set-default', plus initialize components added to the
composition; finalize those removed; and clear cached value of
VARIABLE."
  (let ((old (symbol-value variable)))
    ;; Finalize removed components.
    (tabkit--components-finalize (seq-difference old value #'eq))
    ;; Initialize added components.
    (tabkit--components-initialize (seq-difference value old #'eq))
    (tabkit--custom-set-and-refresh-elt variable value)))

(cl-defgeneric tabkit-component-initialize (_name)
  "Initialize component NAME.
Does nothing by default."
  nil)

(cl-defgeneric tabkit-component-finalize (_name)
  "Finalize component NAME.
Does nothing by default."
  nil)

(defconst tabkit--line-components
  '(home pinned-tabs tabs newtab filler))

(defcustom tabkit-line-composition tabkit--line-components
  "Composition of the tabkit line.
That is, a list of component names as symbols, that specifies which
components are part of the tabkit line and in which order.  The same
component can only appear once on the tabkit line.  Only the first
occurrence matters, the others are ignored.

The below components are predefined, listed in order of appearance in
the default tabkit line composition:

- `home', button that toggles viewing of group tabs.
- `pinned-tabs', list of the pinned tab icons.
- `tabs', list of tabs.
- `newtab', button that shows a new tab menu.
- `filler', flexible space.

`tabs' and `filler' are special components always present on the
tabkit line.  If not specified, they are added in this order to the
composition.  They automatically consume any remaining free space as
they are added to the composition of tabkit line."
  :type '(repeat
          :tag "Name of components"
          :extra-offset 2
          (symbol))
  :set (lambda (variable value)
         ;; Update components based on the new value of
         ;; `tabkit-line-composition'.  If option is not yet bound
         ;; (i.e. `custom-file' is first loaded before this library),
         ;; this will be done when `tabkit-mode' is enabled.
         (if (boundp variable)
             (tabkit--custom-update-components variable value)
           (custom-set-default variable value)))
  )

(defconst tabkit--pad-props
  `( keymap ,tabkit-line-map
     ;; help-echo ,(tabkit-text-render "<d>%s</d>" tabkit-line-help)
     face (tabkit-default tabkit--base)
     follow-link ignore
     pointer arrow)
  "Properties of pad space used in the tabkit line.")

(cl-defgeneric tabkit-component (_type &rest _args)
  "Return a tabkit line component of TYPE.
ARGS are any arguments needed to build to the component.
By default just return nil to ignore component not implemented."
  nil)

(cl-defmethod tabkit-component ((_ (eql 'filler)) _ width)
  "Return the `filler' component.
WIDTH is the width in pixels of filling space.
Return a string or nil."
  (and (natnump width)
       (tabkit-text (tabkit-space width) nil tabkit--pad-props)))

(cl-defmethod tabkit-component ((_ (eql 'home)) &rest)
  "Return the `home' component.
Return a string or nil."
  ;; Show home button when using groups.
  (and tabkit-use-groups
       (let ((btn (tabkit-with-memo 'tabkit-home-button-ui
                    (tabkit-button 'home))))
         (if (tabkit-showing-groups) (car btn) (cdr btn)))))

(cl-defmethod tabkit-component ((_ (eql 'newtab)) &rest)
  "Return the `newtab' component.
Return a string or nil."
  (tabkit-with-memo 'tabkit-newtab-button-ui
    (tabkit-button 'newtab)))

(cl-defmethod tabkit-component ((_ (eql 'pinned-tabs)) tabset)
  "Return the `pinned-tabs' component.
TABSET is the tabset used to compose the tabkit line.
Return a string or nil."
  (and tabkit--pinned-tabs
       (not (tabkit-meta-tabset-p tabset))
       (let* ((spc (tabkit-space 4))
              (sep (tabkit-memo 'tabkit-separator))
              (def (tabkit-memo 'tabkit-tab-pin-icon))
              (pic (and tabkit-tab-prefer-pin-icon def))
              elts)
         (dolist (tab tabkit--pinned-tabs)
           (let* ((faces (tabkit-tab-faces tab))
                  (icon  (or pic (tabkit-tab-icon tab) def))
                  (badge (tabkit-tab-badge tab)))
             (push (tabkit-text
                    (apply #'concat (list spc icon badge spc))
                    (car faces)
                    `( :tab ,tab
                       mouse-face ,(cdr faces)
                       keymap ,tabkit-tab-map
                       help-echo tabkit--tab-help-handler
                       follow-link ignore pointer arrow))
                   elts)))
         (tabkit-text-join sep (string-join elts sep) sep))))

;;; Tab appearance
;;
(defconst tabkit--tab-components
  '(icon label badge close))

(define-widget 'tabkit--tab-composition 'repeat
  "Widget to customize elements that compose a tab."
  :tag "Name of components"
  :extra-offset 2
  :args `(
          (symbol
           :validate
           ,(lambda (widget)
              (let ((v (widget-value widget)))
                (unless (memq v tabkit--tab-components)
                  (widget-put
                   widget :error (format "Invalid component: %S" v))
                  widget))))
          )
  )

(defcustom tabkit-tab-composition tabkit--tab-components
  "Composition of a tab.
That is, a list of component names as symbols, that specifies which
components are part of a tab and in which order.  The same component
can only appear once on a tab.  Only the first occurrence matters, the
others are ignored.

The possible tab components are, listed in order of appearance in the
default tab composition:

- `icon',  optional tab icon;
- `label', mandatory tab label, automatically added if missing;
- `badge', optional tab badge to indicate marked/modified tab;
- `close', optional tab close button."
  :type 'tabkit--tab-composition
  :set (lambda (variable value)
         ;; Update components and definition of the function
         ;; 'tabkit-tab-compose', based on the new value of
         ;; `tabkit-tab-composition'.  If option is not yet bound
         ;; (i.e. `custom-file' is first loaded before this library),
         ;; this will be done when `tabkit-mode' is enabled.
         (if (not (boundp variable))
             (custom-set-default variable value)
           (tabkit--custom-update-components variable value)
           (tabkit--tab-set-composer)
           (tabkit-display-update))))

(defcustom tabkit-tab-text-align 'left
  "How to align text label of tabs.
That is, one of the symbols `left' (default), `right' or `center' to
respectively left-align right-align, or center text."
  :type '(choice
          (const left)
          (const right)
          (const center))
  :set (lambda (variable value)
         (custom-set-default variable value)
         ;; `tabkit-display-update' is inlined, so `fboundp' when
         ;; custom-file loaded before library.
         (tabkit-display-update)))

(define-inline tabkit--tab-composition (composition)
  "Remove duplicate and unknown symbols from COMPOSITION.
Return the new list of valid tab components symbols."
  (inline-quote
   (seq-intersection (seq-union ,composition '(label))
                     tabkit--tab-components)))

(defalias 'tabkit-tab-compose #'ignore)

(defun tabkit--tab-set-composer ()
  "Set function `tabkit-tab-compose' from `tabkit-tab-composition'."
  (fset 'tabkit-tab-compose
        (+-byte-compile-lexical
         `(lambda (icon label close badge sep)
            "Compose tab with ICON, LABEL, CLOSE, EMBLEM and SEP.
SEP is the separator used between sub-components and around result.
Return the tab composed according to option `tabkit-tab-composition'."
            ;; Avoid "Unused lexical argument" warning on optional
            ;; elements omitted from tab COMPOSITION.
            (ignore icon close badge)
            (concat sep (string-join
                         (delq nil (list ,@(tabkit--tab-composition
                                            tabkit-tab-composition)))
                         sep)
                    sep)))))

(define-inline tabkit-tab-compose-p (symbol)
  "Return non-nil if SYMBOL is a sub-component of tab composition.
That is, in the list specified by option `tabkit-tab-composition'."
  (inline-quote (memq ,symbol tabkit-tab-composition)))

(cl-defmethod tabkit-component ((_ (eql 'label)) tab face)
  "Return the sub-component NAME (`label') of TAB.
Return a string with FACE."
  (let ((name (tabkit-tab-display-name tab)))
    (tabkit-text name
                 face
                 `(,@(if (tabkit-tab-showing-group tab)
                         `(face ,(if (tabkit-instant-group-p name)
                                     'tabkit-tab-instant-group
                                   'tabkit-tab-group)))
                   keymap ,tabkit-tab-map
                   help-echo tabkit--tab-help-handler))))

(cl-defmethod tabkit-component ((name (eql 'icon)) tab face)
  "Return the sub-component NAME (`icon') of TAB.
Return a string with FACE or nil."
  (when-let* ((_ (tabkit-tab-compose-p name))
              (icon (tabkit-tab-icon tab)))
    (tabkit-text icon face `(keymap ,tabkit-tab-map))))

(cl-defmethod tabkit-component ((name (eql 'close)) tab face)
  "Return the sub-component NAME (`close') of TAB.
Return a string with FACE or nil."
  (when-let* ((_ (tabkit-tab-compose-p name))
              (close (tabkit-tab-close-button tab)))
    (tabkit-text close face)))

(cl-defmethod tabkit-component-initialize ((_ (eql 'badge)))
  "Initialize the `badge' tab component."
  (add-hook 'tabkit-refresh-ui-functions
            #'tabkit-tab-badge-refresh-ui))

(cl-defmethod tabkit-component-finalize ((_ (eql 'badge)))
  "Finalize the `badge' tab component."
  (remove-hook 'tabkit-refresh-ui-functions
               #'tabkit-tab-badge-refresh-ui))

(cl-defmethod tabkit-component ((name (eql 'badge)) tab face)
  "Return the sub-component NAME (`badge') of TAB.
Return a string with FACE or nil."
  (when-let* ((_ (tabkit-tab-compose-p name))
              (badge (tabkit-tab-badge tab)))
    (tabkit-text badge face `(keymap ,tabkit-tab-map))))

(cl-defmethod tabkit-component ((tab tabkit--tab) width)
  "Return TAB component.
WIDTH is the width in pixels of the TAB component.
The tab layout is specified by option `tabkit-tab-composition'.
Return a string."
  (let* ((faces  (tabkit-tab-faces tab)) ;; Tab (FACE . HIGHLIGHT)
         ;; In order to correctly compute the pixel width of tab,
         ;; temporarily mask any :box attribute from tab faces to
         ;; exclude the additional width of box vertical lines.
         (nobox  (list :box nil))
         (shape  'arrow) ;; Pointer shape on tab.
         (h-face nil)    ;; Tab highlight face.
         (t-face (cond   ;; Tab face.
                  ((tabkit-tab-dragged-p tab)
                   ;; On a dragged tab, use the `hand' pointer and the
                   ;; `tabkit-tab-drag-highlight' face, in addition to
                   ;; tab face, to highlight the tab.
                   (setq shape 'hand)
                   `(,nobox tabkit-tab-drag-highlight ,(car faces)))
                  (t
                   (setq h-face (cdr faces))
                   `(,nobox ,(car faces)))))
         (sep    (tabkit-text (tabkit-space 2) ;; 2px space separator
                              t-face
                              `( keymap ,tabkit-tab-map
                                 help-echo tabkit--tab-help-handler)))
         (sepw   (string-pixel-width sep))
         (usedw  (+ sepw sepw))
         (label  (tabkit-component 'label tab t-face))
         (icon   (tabkit-component 'icon  tab t-face))
         (close  (tabkit-component 'close tab t-face))
         (badge  (tabkit-component 'badge tab t-face)))
    ;; Count icon width.
    (if icon (setq usedw (+ usedw sepw (string-pixel-width icon))))
    ;; Count close button width.
    (if close (setq usedw (+ usedw sepw (string-pixel-width close))))
    ;; Count badge width.
    (if badge (setq usedw (+ usedw sepw (string-pixel-width badge))))
    ;; Adjust the label to fit in passed WIDTH, if non-nil.
    ;; Otherwise, width is that of label contents.
    (if width (setq label (tabkit-text-fit
                           label (- (car width) usedw)
                           tabkit-tab-text-align
                           (cadr width) (caddr width))))
    ;; Do the final layout according to `tabkit-tab-composition'.
    (setq label (tabkit-tab-compose icon label close badge sep))
    ;; Undo masking of the :box attribute by changing the :box
    ;; attribute from nil to `unspecified' in the lexical variable
    ;; `nobox'.  It works because internal functions that deal with
    ;; text properties use unaltered (not copy) references to property
    ;; values.
    (setcdr nobox (list 'unspecified))
    ;; Common properties.
    (add-text-properties 0 (length label)
                         `( :tab ,tab
                            mouse-face ,h-face
                            pointer ,shape
                            ;; Don't turn mouse-1 into mouse-2.
                            follow-link ignore)
                         label)
    label))

(cl-defmethod tabkit-component ((_ (eql 'tabs)) tabset width)
  "Return the `tabs' component.
TABSET is the tabset used to compose the tabkit line.
WIDTH is the maximum width in pixels the component can occupy.
Return a string or nil."
  (when (natnump width)
    (let* ((tabs  (tabkit-tabset-view tabset))
           (start (tabkit-tabset-start tabset))
           (view  (nthcdr start tabs))
           (sel   (tabkit-selected-tab tabset))
           (sep   (tabkit-memo 'tabkit-separator))
           (slb   (tabkit-memo 'tabkit-scroll-left-button-ui))
           (srb   (tabkit-memo 'tabkit-scroll-right-button-ui))
           (tabw  (tabkit-tab-width width tabs))
           (hscr  (tabkit-tabset-hscroll tabset))
           ;; Auto scrolling overrides manual scrolling.
           (mscr  (if hscr
                      (setf (tabkit-tabset-prop tabset :scroll) nil)
                    (tabkit-tabset-prop tabset :scroll)))
           curw scrl elts-1 elts scrr beg end)
      ;; Clear index of previous partially visible tab on left side.
      (setf (tabkit-tabset-prop tabset :start0) nil)
      ;; -------------------------------------------------------------
      ;; Auto-scroll to ensure the selected tab is visible when both
      ;; conditions below are met:
      ;; 1. showing group tabs or the selected tab is unpinned;
      ;; 2. there are hidden scrollable tabs or auto-scrolling has
      ;;    been requested.
      (when (and (or (tabkit-meta-tabset-p tabset)
                     (not (tabkit-tab-pinned-p sel)))
                 (or (and tabs (not view)) hscr))
        (tabkit-tabset-set-hscroll tabset nil)
        ;; If necessary, auto-scroll tabs right to ensure the selected
        ;; tab is in the view.
        (while (not (memq sel view))
          (setq start (1- start)
                view (nthcdr start tabs)))
        ;; Compose tabs up to and including the selected tab.
        (let (atsel)
          (while (and view (not atsel))
            ;; Each elt is double: tab component + separator.
            (push sep elts)
            (push (tabkit-component (car view) tabw) elts)
            (setq atsel (eq (car view) sel)
                  view  (cdr view))))
        (setq elts (nreverse elts)
              ;; Auto-scrolling means that tabs are truncated, so keep
              ;; room for the scroll buttons, assuming size of enabled
              ;; buttons >= size of disabled buttons.
              scrl (car slb)
              scrr (car srb))
        ;; At this point, the selected tab is the last one in ELTS.
        ;; If necessary, auto-scroll tabs left to ensure that it will
        ;; be (possibly partially) visible at the right end of the
        ;; line.
        (let ((w (- width (string-pixel-width
                           (tabkit-text-join beg scrl sep scrr)))))
          (while (and (cddr elts) ;; skip tab+separator.
                      (let ((txt (tabkit-text-fit (string-join elts)
                                                  w 'left nil "")))
                        (not (eq sel (tabkit-tab-at-pos
                                      (1- (length txt)) txt)))))
            (setq elts (cddr elts) ;; skip tab+separator.
                  start (1+ start))))
        (tabkit-tabset-set-start tabset start))
      ;; -------------------------------------------------------------
      ;; Compute the width of consumed space taking into account
      ;; scroll buttons, assuming size of enabled buttons >= size of
      ;; disabled buttons.
      (setq scrl (and (> start 0) (car slb))
            scrr (and scrl (car srb))
            elts (string-join elts)
            curw (string-pixel-width
                  (tabkit-text-join scrl elts sep scrr)))
      ;; Append tabs until all the available space is consumed.
      (while (and view (<= curw width))
        (let ((elt (concat sep (tabkit-component (car view) tabw))))
          (setq curw (+ curw (string-pixel-width elt))
                elts-1 elts
                elts (concat elts elt)
                view (cdr view))))
      ;; -------------------------------------------------------------
      ;; Do the final layout.
      (cond

       ;; 1 - Not all visible tabs fit in the line.
       ((> curw width)
        ;; By default, align the first visible tab to the left and
        ;; truncate the line to the right.
        (setq end (tabkit-text-join sep (or scrr (car srb)))
              beg (tabkit-text-fit
                   (tabkit-text-join (or scrl (cdr slb)) elts)
                   (- width (string-pixel-width end))
                   'left nil ""))
        ;; Unless scrolling manually, if the selected tab appears
        ;; truncated at the right end of the line, align the visible
        ;; tabs to the right and truncate the line to the left, to
        ;; display the entire selected tab at the right end.
        (and (null mscr)
             (eq sel (tabkit-tab-at-pos (1- (length beg)) beg))
             (setq beg (tabkit-text-join (car slb) sep)
                   end (tabkit-text-fit
                        (tabkit-text-join
                         ;; If the tab following the last visible tab
                         ;; is also in ELTS, it must be deleted to be
                         ;; sure to align the last visible tab to the
                         ;; right end.
                         (if (eq sel (tabkit-tab-at-pos
                                      (1- (length elts)) elts))
                             elts elts-1)
                         sep (or scrr (car srb)))
                        (- width (string-pixel-width beg))
                        'right nil ""))))

       ;; 2 - The remaining visible tabs do not occupy the whole line
       ;;     (there is empty space on the right), align the last
       ;;     visible tab to the right end (truncate the line on the
       ;;     left).
       (scrl
        (setq view (nreverse (take start tabs)))
        ;; Add tabs on left until all the available space is consumed.
        (while (and view (<= curw width))
          (let ((elt (concat sep (tabkit-component (car view) tabw))))
            (setq curw (+ curw (string-pixel-width elt))
                  elts (concat elt elts)
                  start (1- start)
                  view (cdr view))))
        (if (<= curw width)
            ;; When the last visible tab is aligned to the right end,
            ;; if all visible tabs fit in the line, there is nothing
            ;; else to do.
            (setq beg elts)
          ;; Otherwise, truncate visible tabs to the left.
          (setq beg (tabkit-text-join scrl sep)
                end (tabkit-text-fit
                     (tabkit-text-join elts sep (cdr srb))
                     (- width (string-pixel-width beg))
                     'right nil ""))
          ;; Keep the index of the tab partially visible on the left
          ;; end to later adjust scroll position accordingly.
          (setf (tabkit-tabset-prop tabset :start0) start)))

       ;; 3 - All visible tabs fit in the line, there is nothing else
       ;;     to do.
       ((setq beg (concat elts sep))))
      ;; -------------------------------------------------------------
      ;; Return the composed list of tabs.
      (tabkit-text-join beg end))))

(defconst tabkit--flexible-components '(tabs filler)
  "List of flexible component names.
These components are mandatory and build in the specified order.
Order is important because these components gradually consume all the
free space in the visible area.  The corresponding `tabkit-component'
method receives the width of the remaining free space.")

(defun tabkit--format-prepare ()
  "Prepare internal-use data needed to format the tabkit line."
  ;; Make sure that all elements in the tabkit line inherit an
  ;; absolute default :height to ensure their pixel width is
  ;; calculated correctly.
  (face-spec-reset-face 'tabkit--base)
  (set-face-attribute
   'tabkit--base nil
   :height (internal-get-lisp-face-attribute 'default :height))
  ;; Preload internal data when needed.
  (tabkit-with-memo 'tabkit-vertical-space
    (let ((h (tabkit-space 1 (tabkit-line-pixel-height))))
      ;; Prepare the internal-use component that sets the height.
      (setf (tabkit-memo 'tabkit--component-height)
            (cons (make-symbol "height") h))
      h))
  (tabkit-with-memo 'tabkit-line-composition
    ;; Ensure the composition of the tabkit line includes the
    ;; mandatory flexible components.
    (seq-union
     tabkit-line-composition tabkit--flexible-components #'eq))
  (tabkit-with-memo 'tabkit-separator
    (tabkit--separator))
  (tabkit-with-memo 'tabkit-scroll-left-button-ui
    (tabkit-button 'scroll-left))
  (tabkit-with-memo 'tabkit-scroll-right-button-ui
    (tabkit-button 'scroll-right))
  (tabkit-with-memo 'tabkit-tab-pin-icon
    (tabkit-icon tabkit-tab-pin-icon 'tabkit-tab-icon)))

(defmacro tabkit--format-component (name &rest args)
  "Wrapper to safely format a component.
Call the `tabkit-component' method for NAME with ARGS.  If the method
fails, return nil to ignore it, and demote errors to messages."
  (let ((err (make-symbol "err")))
    `(condition-case-unless-debug ,err
         (tabkit-component ,name ,@args)
       (error
        (message (format "tabkit-component `%s', %%s" ,name)
                 (error-message-string ,err))
        nil))))

(defun tabkit--format-line (tabset)
  "From TABSET, format a tabkit line.
Layout components according to the option `tabkit-line-composition'.
Return a string."
  (tabkit--format-prepare)
  (let* ((comps (tabkit-memo 'tabkit-line-composition))
         (width (window-pixel-width))
         ;; Init. the composition with pre-built `height' component.
         (compo (list (tabkit-memo 'tabkit--component-height)))
         (free width))
    ;; Build all the fixed size components first, in order to know the
    ;; width of available free space.
    (dolist (cn comps)
      (push (if (memq cn tabkit--flexible-components)
                (list cn)
              (when-let* ((elt (tabkit--format-component cn tabset)))
                (setq free (- free (string-pixel-width elt)))
                (cons cn elt)))
            compo))
    (setq compo (nreverse compo))
    ;; Now that the fixed size components are built, the width of
    ;; available free space is known: build the flexible components
    ;; passing them the remaining free space width.
    (dolist (cn tabkit--flexible-components)
      (when-let* ((elt (tabkit--format-component cn tabset free)))
        (setq free (- free (string-pixel-width elt)))
        (setf (alist-get cn compo) elt)))
    ;; ---------------------------------------------------------------
    ;; Put in cache both the current window pixel width, and the
    ;; composed tab line, and return the latter.
    (setf (tabkit-window-local :width) width)
    (setf (tabkit-window-local :line)
          (apply #'tabkit-text-join (mapcar #'cdr compo)))))

(defcustom tabkit-safe-commands
  '(
    right-char left-char
    next-line previous-line
    mwheel-scroll scroll-bar-toolkit-scroll
    cua-scroll-down cua-scroll-up
    scroll-down scroll-up
    scroll-down-command scroll-up-command
    mouse-set-point
    )
  "Commands without side effects on the tabkit line.
After such commands, assume that the previous value of the tabkit line
can be safely reused."
  :type '(repeat (symbol :tag "command")))

(defun tabkit--line-reuse ()
  "Return a reusable value of the tabkit line.
Return nil if a fresh value will have to be composed."
  ;; Return the cached value of the tabkit line when the display area
  ;; didn't change and, either a command input is pending; or,
  ;; `last-command' is assumed to not have side effects on the tabkit
  ;; line (cf. `tabkit-safe-commands').
  (and (eql (window-pixel-width) (tabkit-window-local :width))
       (or (input-pending-p) (memq last-command tabkit-safe-commands))
       (tabkit-window-local :line)))

(defun tabkit-line-expired ()
  "Mark the tabkit line expired (to be refreshed) on all windows.
Also run the hook `tabkit-after-tabs-change-hook'."
  (run-hooks 'tabkit-after-tabs-change-hook)
  (+-with-all-windows
   (setf (tabkit-window-local :width) nil)
   (tabkit-walk-tabsets #'tabkit--sync-local-tabs)))

(defun tabkit-line ()
  "Return the tabkit line to display in the current window.
Return a valid `tab-line-format' construct or nil when the tabkit line
is hidden in the current window, that is, when the function specified
in `tabkit-exclude-predicate' return non-nil."
  (if (and (functionp tabkit-exclude-predicate)
           (funcall tabkit-exclude-predicate))
      (progn
        (set tabkit-line-format nil) ;; Hide the tabkit line.
        (tabkit-display-update))
    ;; Try to reuse the tabkit line from cache, otherwise refresh it.
    (let* ((inhibit-redisplay t)
           (tabset (tabkit-current-tabset t)))
      (or (tabkit--line-reuse)
          (tabkit--format-line tabset)))))

;;; Navigation through tabs
;;
(defcustom tabkit-cycle-functions
  (cons #'switch-to-prev-buffer #'switch-to-next-buffer)
  "Custom functions used to cycle through tabs.
That is, a pair (BACKWARD . FORWARD) where BACKWARD and FORWARD are
functions called without argument to respectively cycle backward and
forward.  They default to `switch-to-prev-buffer' and
`switch-to-next-buffer'.  These functions are in effects only when
`tabkit-cycle-scope' is t."
  :type '(cons :tag "Cycle by functions..."
               (function :tag "Function to cycle backward")
               (function :tag "Function to cycle forward")))

(defcustom tabkit-cycle-scope nil
  "Specify the scope of cyclic navigation.
The following values are possible:
- `tabs' to cycle through visible tabs.
- `groups' to cycle through tab groups.
- nil (default) to cycle through visible tabs across tab groups.
- t to use the functions defined in `tabkit-cycle-functions' to cycle
    backward and forward."
  :type '(choice :tag "Cycle through..."
                 (const :tag "Visible tabs" tabs)
                 (const :tag "Tab groups" groups)
                 (const :tag "Visible tabs across tab groups" nil)
                 (const :tag "Custom functions" t)))

(defun tabkit-cycle (&optional backward)
  "Cycle to the next tab.
The scope of the cyclic navigation is specified by the option
`tabkit-cycle-scope'.  If optional argument BACKWARD is non-nil, cycle
to the previous tab instead.  Optional argument EVENT is a received
mouse event."
  (let ((tabset (tabkit-current-tabset t)))
    (if (or (eq tabkit-cycle-scope t) (null tabset))
        ;; If cycle by custom functions, or if no tabset available,
        ;; call the functions specified in `tabkit-cycle-functions'.
        (funcall (if backward
                     (car tabkit-cycle-functions)
                   (cdr tabkit-cycle-functions)))
      (let* ((ttabset (tabkit-meta-tabset))
             ;; If navigation through groups is requested, and there
             ;; is only one group, navigate through visible tabs.
             (cycle (if (and (eq tabkit-cycle-scope 'groups)
                             (not (cdr (tabkit-local-tabs ttabset))))
                        'tabs
                      tabkit-cycle-scope))
             (selected (tabkit-selected-tab tabset))
             tabs tab)
        (cond
         ;; Cycle through visible tabs.
         ((eq cycle 'tabs)
          (setq tab (tabkit-tab-next tabset selected backward))
          ;; When there is no tab after/before the selected one, cycle
          ;; to the first/last visible tab.
          (or tab (setq tabs (tabkit-local-tabs tabset)
                        tab  (car (if backward (last tabs) tabs)))))
         ;; Cycle through tab groups.
         ((eq cycle 'groups)
          (setq tab (tabkit-tab-next ttabset selected backward))
          ;; When there is no group after/before the selected one,
          ;; cycle to the first/last available group.
          (or tab
              (setq tabs (tabkit-local-tabs ttabset)
                    tab  (car (if backward (last tabs) tabs)))))
         (t
          ;; Cycle through visible tabs across tab groups.
          (setq tab (tabkit-tab-next tabset selected backward))
          ;; When there is no visible tab after/before the selected
          ;; one, cycle to the next/previous available group.
          (unless tab
            (setq tab (tabkit-tab-next ttabset selected backward))
            ;; When there is no next/previous group, cycle to the
            ;; first/last available group.
            (or tab
                (setq tabs (tabkit-local-tabs ttabset)
                      tab  (car (if backward (last tabs) tabs))))
            ;; Select the first/last visible tab of the new group.
            (setq tabs (tabkit-local-tabs (tabkit-tab-tabset tab))
                  tab  (car (if backward (last tabs) tabs))))))
        ;; (message "tabkit-cycle from %s to %s" selected tab)
        (tabkit-tab-select tab)
        (tabkit-display-update)))))

(defun tabkit-backward (&optional event)
  "Select the previous available tab.
Apply on EVENT window or the selected window if EVENT is nil.
Depend on the setting of the option `tabkit-cycle-scope'."
  (interactive (list last-input-event))
  (+-with-selected-window
      (window-normalize-window (posn-window (event-start event)))
    (tabkit-cycle t)))

(defun tabkit-forward (&optional event)
  "Select the next available tab.
Apply on EVENT window or the selected window if EVENT is nil.
Depend on the setting of the option `tabkit-cycle-scope'."
  (interactive (list last-input-event))
  (+-with-selected-window
      (window-normalize-window (posn-window (event-start event)))
    (tabkit-cycle)))

(defun tabkit-backward-group (&optional event)
  "Go to selected tab in the previous available group.
Apply on EVENT window or the selected window if EVENT is nil."
  (interactive (list last-input-event))
  (+-with-selected-window
      (window-normalize-window (posn-window (event-start event)))
    (let ((tabkit-cycle-scope 'groups))
      (tabkit-cycle t))))

(defun tabkit-forward-group (&optional event)
  "Go to selected tab in the next available group.
Apply on EVENT window or the selected window if EVENT is nil."
  (interactive)
  (+-with-selected-window
      (window-normalize-window (posn-window (event-start event)))
    (let ((tabkit-cycle-scope 'groups))
      (tabkit-cycle))))

(defun tabkit-backward-tab (&optional event)
  "Select the previous visible tab.
Apply on EVENT window or the selected window if EVENT is nil."
  (interactive (list last-input-event))
  (+-with-selected-window
      (window-normalize-window (posn-window (event-start event)))
    (let ((tabkit-cycle-scope 'tabs))
      (tabkit-cycle t))))

(defun tabkit-forward-tab (&optional event)
  "Select the next visible tab.
Apply on EVENT window or the selected window if EVENT is nil."
  (interactive (list last-input-event))
  (+-with-selected-window
      (window-normalize-window (posn-window (event-start event)))
    (let ((tabkit-cycle-scope 'tabs))
      (tabkit-cycle))))

;;; Local display
;;
(defun tabkit-mode-on-p ()
  "Return non-nil if tabkit mode is on."
  (equal (default-value tabkit-line-format) tabkit-line-template))

(defun tabkit-locally-hidden-p (&optional buffer)
  "Return non-nil if the tabkit line is locally hidden for BUFFER.
See also the command `tabkit-locally-hide'"
  (with-current-buffer (or buffer (current-buffer))
    (and (local-variable-p tabkit-line-format)
         (null (symbol-value tabkit-line-format)))))

(defun tabkit--locally-hide-on (&optional buffer)
  "Hide the tabkit line locally for BUFFER."
  (with-current-buffer (or buffer (current-buffer))
    (set tabkit-line-format nil)
    (tabkit-display-update)))

(defun tabkit--locally-hide-off (&optional buffer)
  "Restore display of the tabkit line if locally hidden for BUFFER."
  (with-current-buffer (or buffer (current-buffer))
    ;; Preserve non-nil local value of the variable defined in
    ;; `tabkit-line-format', which wasn't setup by tabkit.
    (and (local-variable-p tabkit-line-format)
         (null (symbol-value tabkit-line-format))
         (kill-local-variable tabkit-line-format)))
  (tabkit-display-update))

(defun tabkit-locally-hide (&optional arg)
  "Toggle hide of the tabkit line locally for current buffer.
With prefix argument ARG, locally hide the tabkit line if positive,
restore global display otherwise."
  (interactive (list (if current-prefix-arg
                         (prefix-numeric-value current-prefix-arg)
                       'toggle)))
  (when (tabkit-mode-on-p)
    (if (or (and (eq arg 'toggle) (tabkit-locally-hidden-p))
            (and (numberp arg) (> arg 0)))
        ;; Restore global display.
        (tabkit--locally-hide-off)
      ;; Locally hide.
      (tabkit--locally-hide-on))))

;;; tabkit-mode
;;
(defvar tabkit--last-template nil)

(defun tabkit--init ()
  "Initialize internal data structures."
  (setq tabkit--tabsets (obarray-make 31))
  (setq tabkit--pinned-tabs nil)
  (setq tabkit--meta-tabset (make-symbol "tabkit--meta-tabset"))
  (set tabkit--meta-tabset nil)
  (tabkit-memo-init)
  (+-map-all-windows #'tabkit-window-local-clear)
  (tabkit-tabs-data-refresh))

(defun tabkit--mode-on ()
  "Turn on global `tabkit-mode'."
  ;; Save current default value of tabkit-line-format.
  (setq tabkit--last-template (default-value tabkit-line-format))
  ;; Reset internal data structures
  (tabkit--init)
  ;; Load persistent data.
  (tabkit-persistent-data-load)
  ;; Save persistent data when Emacs exits.
  (add-hook 'kill-emacs-hook #'tabkit-persistent-data-save)
  ;; Initialize components.
  (add-hook 'tabkit-refresh-ui-functions
            #'tabkit-refresh-ui-element -90)
  (tabkit--components-initialize tabkit-line-composition
                                 tabkit-tab-composition)
  ;; Set tabkit line format'.
  (set-default tabkit-line-format tabkit-line-template))

(defun tabkit--mode-off ()
  "Turn off global `tabkit-mode'."
  ;; Restore previous line-format'.
  (set-default tabkit-line-format tabkit--last-template)
  ;; Save persistent data.
  (tabkit-persistent-data-save)
  ;; No more need to save persistent data when Emacs exits.
  (remove-hook 'kill-emacs-hook #'tabkit-persistent-data-save)
  ;; Finalize components.
  (tabkit--components-finalize tabkit-line-composition
                               tabkit-tab-composition)
  (remove-hook 'tabkit-refresh-ui-functions
               #'tabkit-refresh-ui-element)
  ;; Reset internal data structures.
  (tabkit--init))

(defvar-keymap tabkit-mode-map
  :doc "Keymap for `tabkit-mode'."
  "C-c C-<left>"  #'tabkit-backward
  "C-c C-<right>" #'tabkit-forward
  "C-c C-<up>"    #'tabkit-backward-group
  "C-c C-<down>"  #'tabkit-forward-group
  "C-c C-<home>"  #'tabkit-home
  "C-c C-<prior>" #'tabkit-scroll-left
  "C-c C-<next>"  #'tabkit-scroll-right
  "C-c C-n"       #'tabkit-newtab-menu-popup
  "C-c C-o"       #'tabkit-options-menu-popup
  "C-c C-g"       #'tabkit-goto-tab
  "C-c C-l"       #'tabkit-locally-hide
  )

(defvar-keymap tabkit-repeat-map
  :doc "Keymap to repeat tabkit navigation and scroll commands.
Used in `repeat-mode'."
  :repeat t
  "<left>"  #'tabkit-backward
  "<right>" #'tabkit-forward
  "<up>"    #'tabkit-backward-group
  "<down>"  #'tabkit-forward-group
  "<home>"  #'tabkit-home
  "<prior>" #'tabkit-scroll-left
  "<next>"  #'tabkit-scroll-right
  )

;;;###autoload
(define-minor-mode tabkit-mode
  "Toggle display of a tabkit line.
To be enabled `tabkit-mode' needs a graphic display.  The tabkit line
is located in the `tab-line' by default, but can also be in the
`header-line' (see the variable `tabkit-line').  With prefix argument
ARG, turn on if positive, otherwise off.  Returns non-nil if enabled.

\\{tabkit-mode-map}"
  :global t
  :keymap tabkit-mode-map
  (unless (display-graphic-p)
    (setq-default tabkit-mode nil)
    (message "Cannot enable if not using a graphic display."))
  ;; Kill nil local values of the variable defined in
  ;; `tabkit-line-format' in order to display the tabkit line which is
  ;; defined in the global value.
  (mapc #'tabkit--locally-hide-off (buffer-list))
  (if (default-value 'tabkit-mode)
      (tabkit--mode-on)
    (tabkit--mode-off)))

;;; Buffer tabs
;;
(defgroup tabkit-buffer nil
  "Display buffers in the tabkit line."
  :group 'tabkit)

(defcustom tabkit-buffer-exclude-criteria
  (list "*Completions*"
        "*Checkdoc Status*"
        (if (boundp 'ispell-choices-buffer)
            ispell-choices-buffer
          "*Choices*"))
  "List of criteria to exclude buffer tabs from the tabkit line.
This is a mixed list of buffer names and major mode symbols.  No tab
will be added for buffers whose name or major mode match a criterion."
  :type '(repeat :tag "Buffer Names/Major Modes"
          (choice (string :tag "Name")
                  (symbol :tag "Mode")))
  :set #'tabkit--custom-set-and-refresh-tabs)

;;; Misc. functions
;;
(defun tabkit--buffer-unique-name (&optional buffer)
  "Return an unique name for BUFFER.
No argument or nil as argument means use the current buffer."
  (or (buffer-file-name buffer)
      ;; Emacs allows you to open at the same time a buffer that
      ;; visits a file and a buffer with the same name that does not
      ;; visit a file! To distinguish between the two uses of the same
      ;; name, the name of the buffer not visiting a file is annotated
      ;; with a `:transient' tag.
      (tabkit-annotated-name (buffer-name buffer) :transient)))

;;; Buffer groups
;;
(defcustom tabkit-buffer-group-criteria
  '(
    ((member (buffer-name) '( "*scratch*" "*Messages*"))
     "Emacs")
    ((memq major-mode
           '( debugger-mode backtrace-mode))
     "Debugger")
    ((memq major-mode
           '( help-mode apropos-mode Info-mode Man-mode
              shortdoc-mode))
     "Help")
    ((eq major-mode 'Custom-mode)
     "Customize")
    ((eq major-mode 'dired-mode)
     "Dired")
    ((eq major-mode 'term-mode)
     "Terminal")
    ((or (get-buffer-process (current-buffer))
         (derived-mode-p 'comint-mode)
         (derived-mode-p 'compilation-mode))
     "Process")
    ((string-match-p "^\\*\\(log-edit-files\\*\\|vc\\)" (buffer-name))
     "Version Control")
    ((memq major-mode
           '( rmail-mode rmail-edit-mode rmail-summary-mode
              vm-summary-mode vm-mode mail-mode
              mh-letter-mode mh-show-mode mh-folder-mode
              gnus-summary-mode message-mode gnus-group-mode
              gnus-article-mode score-mode gnus-browse-killed-mode))
     "Mail")
    ;; Put all other buffers visiting files in one group.
    ((buffer-file-name)
     "File")
    (t
     "Other")
    )
  "Criteria to group buffers.
A list of clauses (CONDITION . GROUPS), where CONDITION is an
s-expression, and GROUPS is a list of group names.  With buffer
temporarily current, try each clause until CONDITION returns non-nil,
in which case the corresponding GROUPS is returned.  Otherwise a list
with the symbol name of the `major-mode' is returned."
  :type '(repeat :tag "Criteria"
          (cons :tag "Clause"
           (sexp :tag "Condition")
           (repeat :tag "Group Names" (string))))
  :set (lambda (variable value)
         (tabkit--custom-set-and-refresh-tabs variable value)
         ;; Re-define the function 'tabkit-buffer-groups' from value.
         ;; When not fbound when custom-file loaded before library,
         ;; delay the initial definition when tabkit is loaded,
         (and (fboundp 'tabkit--define-buffer-groups-function)
              (tabkit--define-buffer-groups-function))))

(defconst tabkit--buffer-groups-fallback '(t "&rest")
  "Internal fallback clause used by default.
When no `tabkit-buffer-group-criteria' matched.")

(defalias 'tabkit-buffer-groups #'ignore)

(defun tabkit--define-buffer-groups-function ()
  "Define the `tabkit-buffer-groups' function.
Also collect all explicit group names in `tabkit-default-group-names'.
This function receive a buffer and return the associated list of
groups according to `tabkit-buffer-group-criteria'."
  (let* ((buf (make-symbol "buffer"))
         (err (make-symbol "err"))
         (cond nil) ;; record the last cond condition.
         (gl nil))
    (fset
     'tabkit-buffer-groups
     (+-byte-compile-lexical
      `(lambda (,buf)
         "Return the list of groups associated to BUFFER."
         (with-current-buffer ,buf
           (condition-case ,err
               (cond
                ((cdr (assoc (tabkit--buffer-unique-name)
                             tabkit--instant-group-criteria
                             #'tabkit-name-equal)))
                ,@(let (rest)
                    (dolist (clause tabkit-buffer-group-criteria)
                      (push
                       `(,(setq cond (car clause))
                         ,@(if (+-string-listp (cdr clause))
                               (progn
                                 (setq gl (seq-union (cdr clause) gl))
                                 `(,(macroexp-quote (cdr clause))))
                             (cdr clause)))
                       rest))
                    ;; Don't append the fallback clause if the last
                    ;; condition is a non-nil constant, to suppress
                    ;; the byte-compiler warning about useless
                    ;; trailing cond clauses introduced in Emacs 30.
                    (nreverse (if (byte-compile-trueconstp cond)
                                  rest
                                (cons tabkit--buffer-groups-fallback
                                      rest)))))
             ;; On error fallback to the "&rest" group name.
             (error
              (message "tabkit-buffer-groups, %s"
                       (error-message-string ,err))
              ,(macroexp-quote
                (cdr tabkit--buffer-groups-fallback))))))))
    (setq tabkit-default-group-names gl)))

;;; Tab color
;;
(defcustom tabkit-buffer-color-criteria
  '(
    ((member (buffer-name) '( "*scratch*" "*Messages*"))
     "#ffd0ff")
    ((memq major-mode
           '( debugger-mode backtrace-mode))
     "#ffdcdc")
    ((memq major-mode
           '( help-mode apropos-mode Info-mode Man-mode
              shortdoc-mode))
     "#dbffc8")
    ((eq major-mode 'Custom-mode)
     "lemon chiffon")
    ((eq major-mode 'dired-mode)
     "khaki3")
    ((eq major-mode 'term-mode)
     "LavenderBlush2")
    ((or (get-buffer-process (current-buffer))
         (derived-mode-p 'comint-mode)
         (derived-mode-p 'compilation-mode))
     "wheat2")
    ((string-match-p "^\\*\\(log-edit-files\\*\\|vc\\)" (buffer-name))
     "LightCyan1")
    ((memq major-mode
           '( rmail-mode rmail-edit-mode rmail-summary-mode
              vm-summary-mode vm-mode mail-mode
              mh-letter-mode mh-show-mode mh-folder-mode
              gnus-summary-mode message-mode gnus-group-mode
              gnus-article-mode score-mode gnus-browse-killed-mode))
     "#d3d3ff")
    )
  "Criteria to assign color to buffer.
A list of clauses (CONDITION COLOR), where CONDITION is an
s-expression, and COLOR is a color name.  With buffer temporarily
current, try each clause until CONDITION returns non-nil, in which
case the corresponding COLOR is returned.  Otherwise, nil is returned
to use a default color."
  :type '(repeat :tag "Criteria"
                 (list :tag "Clause"
                       (sexp :tag "Condition")
                       (color)))
  :set (lambda (variable value)
         (custom-set-default variable value)
         ;; Re-define the function 'tabkit-buffer-color' from value.
         ;; When not fbound when custom-file loaded before library,
         ;; delay the initial definition when tabkit is loaded,
         (and (fboundp 'tabkit--define-buffer-color-function)
              (tabkit--define-buffer-color-function))))

(defalias 'tabkit-buffer-color #'ignore)

(defun tabkit--define-buffer-color-function ()
  "Define the function `tabkit-buffer-color'.
Based on current value of `tabkit-buffer-color-criteria'."
  (let ((buf (make-symbol "buf"))
        (err (make-symbol "err")))
    (fset 'tabkit-buffer-color
          (+-byte-compile-lexical
           `(lambda (,buf)
              "Return color of buffer BUF, or nil if none.
See also `tabkit-buffer-color-criteria'."
              (with-current-buffer ,buf
                (condition-case ,err
                    (cond ,@tabkit-buffer-color-criteria)
                  (error
                   (message "tabkit-buffer-color-criteria, %s"
                            (error-message-string ,err))
                   nil))))))
    (tabkit-display-update)))

(defcustom tabkit-buffer-list-function #'tabkit-buffer-list
  "Function that returns the list of buffers to show in tabs.
That function is called with no arguments and must return a list of
buffers.
This variable can also be set in a `tabkit-mode-hook' run when
`tabkit-mode' is toggled."
  :type 'function)

(defun tabkit-buffer-exclude-p (&optional buffer)
  "Return non-nil to exclude BUFFER from tabs.
BUFFER defaults to the current buffer."
  (with-current-buffer (or buffer (current-buffer))
    (or (char-equal ?\  (aref (buffer-name) 0))
        (tabkit-locally-hidden-p)
        (member (buffer-name) tabkit-buffer-exclude-criteria)
        (memq   major-mode    tabkit-buffer-exclude-criteria))))

(defun tabkit-buffer-list ()
  "Return the list of buffers to show in tabs.
Exclude buffers whose name starts with a space, and those excluded
from tabs (cf. `tabkit-exclude-predicate')."
  (if (functionp tabkit-exclude-predicate)
      (delq nil (mapcar
                 (lambda (b)
                   (unless (funcall tabkit-exclude-predicate b) b))
                 (buffer-list)))
    (buffer-list)))

(defun tabkit-buffer-tabs ()
  "Return the tabset for buffer tabs to display on the tabkit line.
Also select tab of the buffer displayed in selected window."
  (let* ((buffer (window-buffer))
         (tabset (tabkit-tabs-data
                  (funcall tabkit-buffer-list-function)
                  #'tabkit-buffer-groups
                  buffer)))
    (tabkit-select-tab-value buffer tabset)
    tabset))

;;; Buttons
;;
(defcustom tabkit-buffer-newtab-menu
  `("Open a new tab with"
    ["*scratch*" scratch-buffer
     :help "Open the *scratch* buffer."
     :visible (not (get-buffer "*scratch*"))]
    ("Recently closed tabs"
     :filter tabkit-closed-tabs-menu-items
     :help "Reopen a recently closed, reopenable tab"
     :visible tabkit--closed-tabs)
    ("Recent File..."
     :filter recentf-make-menu-items
     :visible (and (boundp 'recentf-mode)
                   (default-value 'recentf-mode)))
    ["File..." find-file
     :help "Read an existing file or visit a new file"]
    ["Library..." find-library
     :help "Read the Emacs Lisp source of a library"]
    ["Directory..." dired
     :help "Read a directory, to operate on its files"]
    ["VC Dir..." vc-dir-root
     :help "Show the VC status of the repository"]
    ["Mail..." menu-bar-read-mail
     :help "Read your mail"]
    )
  "Specify the menu to popup when clicking on the New Tab button.
The first element is a menu title, the rest are menu items, as
expected by `easy-menu-define' to define the menu."
  :type '(cons
          (string :tag "Menu title")
          (repeat (sexp :tag "Menu item")))
  :set #'tabkit--custom-set-and-refresh-elt)

;;; Initialize functions dynamically created from custom data
;;
(tabkit--define-reopen-file-function)   ;; `tabkit-reopen-file'
(tabkit--tab-set-composer)              ;; `tabkit-tab-compose'
(tabkit--define-buffer-groups-function) ;; `tabkit-buffer-groups'
(tabkit--define-buffer-color-function)  ;; `tabkit-buffer-color'

(provide 'tabkit)
;;; tabkit.el ends here
