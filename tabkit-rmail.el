;;; tabkit-rmail.el --- A tabkit rmail component -*- lexical-binding:t -*-

;; Copyright (C) 2024-2025 David Ponce

;; Author: David Ponce <da_vid@orange.fr>
;; Maintainer: David Ponce <da_vid@orange.fr>
;; Created: 30 Mai 2024
;; Keywords: convenience mail

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; This library implements a tabkit `rmail' component that shows a
;; button on the tabkit line when there is unread mail in Rmail
;; inboxes.  Clicking the button will read the mail and hide the
;; button until there is unread mail again.
;;
;; To use this component:
;;
;;   1. Put this library in a directory in your `load-path', and add
;;      this into your Emacs init file:
;;
;;      (with-eval-after-load 'tabkit
;;        (with-demoted-errors "Error: %S"
;;          (require 'tabkit-rmail)))
;;
;;   2. Customize the option `tabkit-line-composition' to include the
;;      `rmail' component in the composition of the tabkit line.

;;; Code:

(require 'tabkit)

(defgroup tabkit-rmail nil
  "Tabkit `rmail' component."
  :group 'tabkit)

;;; The `rmail' button
;;
(defvar-keymap tabkit-rmail-button-map
  :doc "Keymap of the `rmail' button."
  :parent tabkit-button-map
  (tabkit-key 'mouse-1) #'tabkit-rmail-read
  )

(defcustom tabkit-rmail-button-ui
  '((:emoji " 📩 "))
  "The `rmail' simple button icon."
  :type 'tabkit-icon-simple
  :set #'tabkit--custom-set-and-refresh-elt)

(cl-defmethod tabkit-button ((name (eql 'rmail)))
  "Return a new button with NAME equal to `rmail'."
  (if tabkit-rmail-button-ui
      (tabkit--button-create 'simple name
                             tabkit-rmail-button-ui
                             tabkit-rmail-button-map)))

(cl-defmethod tabkit-button-help ((_name (eql 'rmail))
                                  _window _object _pos)
  "Return the help-echo string for the `rmail' button."
  (tabkit-memo 'tabkit-rmail--help))

(defun tabkit-rmail-read (&optional event)
  "Read mail when receiving mouse EVENT on the `rmail' button."
  (interactive (list last-input-event))
  (gmouse-set-current-event event)
  (rmail))

;;; Mail checking
;;
(defcustom tabkit-rmail-check-every 900
  "Check for new mail every specified seconds.
Default is 900 seconds, 15 minutes."
  :type '(natnum :tag "Number of seconds")
  :set #'tabkit--custom-set-and-refresh-elt)

(defsubst tabkit-rmail--files ()
  "Return the list of Rmail inbox files."
  (or rmail-primary-inbox-list
      (list (or (getenv "MAIL")
                (file-name-concat rmail-spool-directory
                                  (user-login-name))))))

(defun tabkit-rmail--file-nonempty-p (file)
  "Return non-nil if FILE exists and is not empty."
  (and (file-exists-p file)
       (< 0 (file-attribute-size
             (file-attributes (file-chase-links file))))))

(defconst tabkit-rmail--notice1 "There is new mail in <k0>%s</k0>")
(defconst tabkit-rmail--notice2 "<k1>mouse-1</k1>: Read Mail")

(defun tabkit-rmail--notify (files)
  "Notify about new mail presence in FILES, if non-nil."
  (if files
      (let* ((in (string-join files ", "))
             (h1 (tabkit-text-render tabkit-rmail--notice1 in))
             (h2 (tabkit-text-render tabkit-rmail--notice2)))
        (setf (tabkit-memo 'tabkit-rmail--help) (concat h1 "\n" h2))
        (message h1))
    (setf (tabkit-memo 'tabkit-rmail--help) nil)))

(defvar tabkit-rmail--state nil)

(defun tabkit-rmail--state ()
  "Get the state of the `rmail' component."
  (and (setq tabkit-rmail--state
             (if (tabkit-memo 'tabkit-rmail--p)
                 (progn
                   (tabkit-rmail--stop-checking)
                   (tabkit-with-memo 'tabkit-rmail-button-ui
                     (tabkit-button 'rmail)))
               ;; Deactivate checking when repeat time is set.
               (tabkit-with-memo 'tabkit-rmail-check-every
                 (tabkit-rmail--stop-checking)
                 tabkit-rmail-check-every)
               ;; Activate automatic checking when inactive.
               (tabkit-with-memo 'tabkit-rmail--check
                 (tabkit-rmail--start-checking))
               nil))
       (tabkit-line-expired)))

(defun tabkit-rmail--check ()
  "Check if there is new mail."
  (let ((files (seq-filter #'tabkit-rmail--file-nonempty-p
                           (tabkit-rmail--files))))
    (tabkit-rmail--notify files)
    (setf (tabkit-memo 'tabkit-rmail--p) files)
    (tabkit-rmail--state)))

(defun tabkit-rmail--start-checking ()
  "Start checking for new mail."
  ;; When checking for new mail is already active, no need to enable
  ;; it again after Rmail has retrieved pending mails.
  (remove-hook 'rmail-get-new-mail-hook
               #'tabkit-rmail--start-checking)
  ;; Activate automatic checking if needed.
  (tabkit-with-memo 'tabkit-rmail--check
    ;; Check for new mail every specified repeat time.
    (run-with-timer nil tabkit-rmail-check-every
                    #'tabkit-rmail--check)))

(defun tabkit-rmail--stop-checking (&optional deactivate)
  "Stop checking for new mail.
If DEACTIVATE is non-nil, don't reactivate checking for new mail after
Rmail has retrieved pending mails."
  (let ((timer (tabkit-memo 'tabkit-rmail--check)))
    (setf (tabkit-memo 'tabkit-rmail--check) nil)
    (and (timerp timer) (cancel-timer timer))
    ;; If the `rmail' component is active, enable checking for new
    ;; mail after Rmail has retrieved pending mails.
    (funcall (if deactivate #'remove-hook #'add-hook)
             'rmail-get-new-mail-hook
             #'tabkit-rmail--start-checking)))

;;; Refresh UI
;;
(defun tabkit-rmail-refresh-ui (element)
  "Clear the cached value of UI ELEMENT.
If the `rmail' component is active, ELEMENT is t, which means all UI
elements need to be refreshed, or ELEMENT is `tabkit-rmail-button-ui',
ensure to refresh both the `rmail' button and the component state."
  (when (memq element '(t tabkit-rmail-button-ui))
    (tabkit-refresh-ui-element 'tabkit-rmail-button-ui)
    (tabkit-rmail--state)))

;;; The `rmail' component
;;
(cl-defmethod tabkit-component-initialize ((_ (eql 'rmail)))
  "Initialize the `rmail' line component."
  (add-hook 'tabkit-refresh-ui-functions
            #'tabkit-rmail-refresh-ui)
  (tabkit-rmail--start-checking))

(cl-defmethod tabkit-component-finalize ((_ (eql 'rmail)))
  "Finalize the `rmail' line component."
  (tabkit-rmail--stop-checking t)
  (remove-hook 'tabkit-refresh-ui-functions
               #'tabkit-rmail-refresh-ui)
  (tabkit-line-expired))

(cl-defmethod tabkit-component ((_ (eql 'rmail)) _tabset)
  "Return the `rmail' tabkit component.
This component will show up when there is new mail."
  tabkit-rmail--state)

(provide 'tabkit-rmail)

;;; tabkit-rmail.el ends here
