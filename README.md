# tabkit

This library<sup>[[1](#1)]</sup> provides the global minor mode
`tabkit-mode` to display tabs on the `tab-line` area of every window
(the **tabkit line**), when Emacs is using a graphic display.  The
command `tabkit-mode` toggles this mode.

When `tabkit-mode` is enabled, buffers are displayed in tabs (see the
sample screen shot below) allowing you to quickly navigate between
them, in a similar way the Firefox browser tabbed UI is used to
navigate between web pages.

- [Installation](#installation)
- [Data structure](#data-structure)
- [User Interface](#user-interface)
- [Commands](#commands)
- [Buffer tabs](#buffer-tabs)

###### tabkit line using images

![\ ](tabkit-with-images.png)

<br>&HorizontalLine;&HorizontalLine;&HorizontalLine;&HorizontalLine;<br>
<sup id="1">**[1]**</sup> <small>Originally developed as
[**tabbar.el**](https://sourceforge.net/projects/emhacks/files/tabbar/).</small>

## Installation

1. Put `tabkit.el` and its dependencies in a directory in your
   `load-path`.
2. Either type `M-x load-library RET tabkit` to manually load the
   library, or add `(autoload 'tabkit-mode "tabkit" "Toggle display of
   a tabkit line."  t)` in your init file to automatically load the
   library on first use.
3. `M-x tabkit-mode` enable/disable the tabkit line.
4. To have `tabkit-mode` enabled by default, type `M-x
   customize-option RET tabkit-mode` to customize the option.

### Dependencies

- [gmouse.el](https://codeberg.org/dponce/elib.d/src/branch/master/gmouse.el)
  to manage mouse events.<sup>*</sup>.
- [memo.el](https://codeberg.org/dponce/elib.d/src/branch/master/memo.el)
  to memoize data with symbols<sup>*</sup>.
- [nuancier.el](https://codeberg.org/dponce/nuancier/src/branch/master/nuancier.el)
  to select colors.
- [plus.el](https://codeberg.org/dponce/elib.d/src/branch/master/plus.el)
  for various functions<sup>*</sup>.
- [pic.el](https://codeberg.org/dponce/elib.d/src/branch/master/pic.el)
  for visual components<sup>*</sup>.
- [qolor.el](https://codeberg.org/dponce/elib.d/src/branch/master/qolor.el)
  for colors<sup>*</sup>.
- [xdg-mime.el](https://codeberg.org/dponce/elxdg/src/branch/master/xdg-mime.el)
  to lookup mime type of files.
- [xdg-icontheme.el](https://codeberg.org/dponce/elxdg/src/branch/master/xdg-icontheme.el)
  to lookup image of xdg icons.
- [wilo.el](https://codeberg.org/dponce/elib.d/src/branch/master/wilo.el)
  to manage window-local variables<sup>*</sup>.

<sup>*</sup><small>Mandatory</small>.

**IMPORTANT**: tabkit does not work in versions of [GNU
Emacs](https://www.gnu.org/software/emacs/) below 29. Also,
`tabkit-mode` can only be enabled on a graphical display, when
`(display-graphic-p)` returns non-nil.

## Data structure

The collection (group) of tabs displayed on the tabkit line is
represented by an internal data structure called a **tabset**,
identified by an unique name.

A tab is represented by a list that associates the value of the
tab, which can be of any Elisp data type; a reference to the
parent tabset; and a list of optional tab properties.

Several tabsets can co-exist at the same time, but only one can be
displayed at a time on the tabkit line (see [Tab
groups](#tab-groups)).  This tabset is obtained by calling the
function specified in the variable `tabkit-current-tabset-function`.

Here are the available options related to tab and tabset creation, and
their default value.  These options can be set either globally
(customization), or in a `tabkit-mode-hook` run when `tabkit-mode` is
toggled.

| Option | Description | Default value |
|:-------|:------------|:--------------|
| `tabkit-current-tabset-function` | Function to obtain the list of tabs to display on the tabkit line.<br><small>This function is called with no argument, and must return a tabset. | `tabkit-buffer-tabs` |
| `tabkit-exclude-predicate` | Function to exclude tab from the tabkit line.<br><small>This function receives an optional tab value, and return non-nil to not create a corresponding tab on the tabkit line.  If not a function, no tab is excluded.</small> | `tabkit-buffer-exclude-p` |

A tabset is displayed on the tabkit line through a **view** defined by
the index in an active window of the leftmost tab shown.  Thus, a
tabset may have as many views as there are active windows.  In a view,
changing the index of the leftmost tab scrolls the tabs horizontally.

In each tabset, at any time, one and only one tab is designated as
selected in an active window. Thus, for each tabset, there may be as
many selected tab as there are active windows.

More generally, the state of the tabkit line depends on the Emacs
window where the tabkit line is currently displayed. This includes the
currently selected tab, the horizontal scroll position, if tab groups
are currently displayed, etc.  All such data is maintained in window
parameters.

A special _transient_ tabset is also maintained, which contains, for
the currently active window, the selected tabs in the set of existing
tabsets. The function `tabkit-all-tabsets-tabset` returns this special
tabset which can be further displayed on the tabkit line to show
tabsets as tabs, for example to easily navigate through them (see also
[Buffer groups](#buffer-groups)).

## User Interface

- [Composition](#composition)
- [Images](#images)
- [Faces](#faces)
- [Tooltips](#tooltips)
- [Tabs](#tabs)
- [Performance considerations](#performance-considerations)

###### tabkit line UI

![\ ](tabkit-by-default.png)

### Composition

The content of the tabkit line is defined by a juxtaposition of
_components_, such as static texts and images, buttons, tab lists,
filling space, etc.

The option `tabkit-line-height` controls the height of the tabkit
line.  Its value is either an absolute number of pixels, or a fraction
of the `tabkit-default` face font height.  The function
`tabkit-line-pixel-height` returns the actual value in pixels, which
is not less than the `tabkit-default` face font height.

The option `tabkit-line-composition` specifies which components are
part of the tabkit line and in which order.  This is a list of
component names as symbols.

The same component can only appear once on the tabkit line.  Only the
first occurrence matters, others are ignored.

The below components are predefined, listed in order of appearance in
the default value of option `tabkit-line-composition`:

| Component name | Description                                |
|:---------------|:-------------------------------------------|
| `home`         | Button that toggles viewing of tab groups. |
| `pinned-tabs`  | List of the pinned tab icons.              |
| `tabs`         | List of tabs.                              |
| `newtab`       | Button that shows a new tab menu.          |
| `filler`       | Flexible space.                            |

`tabs` and `filler` are special components always present on the
tabkit line.  When not specified, they are added in this order at the
end of the component list.  They automatically consume any remaining
free space as they are added to the tabkit line.

Creating a new component is a matter of:

1. Implementing a specialized `tabkit-component` method and possibly
the `tabkit-component-initialize` and `tabkit-component-finalize`
methods used to respectively initialize the component when it is added
to the composition, and finalize it when it is removed from the
composition.

2. Customize the option `tabkit-line-composition` to add the component
name to the composition of the tabkit line.

Here is an example of a simple `date` component that will show the
current date and time.

```emacs-lisp
(cl-defmethod tabkit-component ((_ (eql 'date)) &rest)
  "Return the `date' component that displays the date and time."
  (propertize (format-time-string " %F %R ")
              'face 'error
              'keymap tabkit-line-map))
```

### Images

Images can be used for special buttons, for tab icons, and tab close
button (like in this [screenshot](#tabkit-line-using-images)), unless
the option `tabkit-use-images` is disabled.

The option `tabkit-image-height` controls the height of images used on
the tabkit line.  Its value is a percentage of the tabkit line height
(see [Composition](Composition)).  The function
`tabkit-image-pixel-height` returns the actual value in pixels.

The core function to lookup images is `tabkit-find-image`.  Images can
be specified to be searched the normal way in `image-load-path`
directories, or to be looked up in the icon theme specified by the
option `tabkit-image-icon-theme`, or to be loaded from a remote
location.

Every image used in tabkit is specified by a list of image
specifications `(SPEC ...)`.  In addition to the `:file` and `:data`
properties, these specific image properties are recognized in `SPEC`:

1) Remote images:

   - `:url URL`, a string, the http(s) URL from which to get the image
     file.

2) XDG icons (when the external libraries `xdg-mime` and
`xdg-icontheme` are available, see [Dependencies](#dependencies)):

   - `:xdg-icon ICON-SPEC`, either a string, the looked up icon name,
     or a pair `(ICON-NAME . ICON-PROPS)` where `ICON-NAME` is the
     looked up icon name and `ICON-PROPS` is a plist with these
     additional properties:

     * `:theme THEME`, a string, the icon theme where to lookup icon.
       Default is `tabkit-image-icon-theme`.

     * `:size SIZE`, a positive integer, pixel size of looked up icon.
       Default is `tabkit-image-height`.

     * `:scale SCALE`, a positive integer, scale of looked up icon.
       Defaults is 1.

     * `:context CONTEXT`, a string, name of the context where to
       lookup icon.  Default is to lookup icon regardless of context.

All other properties in image SPEC are passed to `create-image`.

Here is an example of a tabkit image specification:

```emacs-lisp
'((:xdg-icon "arrow-left" :scale 0.7 :conversion disabled :margin (8 . 0))
  (:xdg-icon ("arrow-left" :theme "oxygen"))
  (:file "tabkit/icons/left-off.svg" :margin (4 . 0))
  (:url "https://github.com/marella/material-design-icons/raw/main/svg/outlined/face.svg"))
```

1. First line specifies to lookup an XDG icon named `"arrow-left"` in
   `tabkit-image-icon-theme`.

2. Second line, to lookup an XDG icon named `"arrow-left"`in the
   `"oxygen"` theme.

3. Third line, to lookup a relative image file name
   `"tabkit/icons/left-off.svg"` in `image-load-path`.

4. Fourth line, to load the image file `"face.svg"` from a remote
   location.

The returned image will be the first meeting a spec and will have the
additional image properties mentioned in the spec.

### Faces

The below faces are available to customize the appearance of buttons
and tabs:

| Face                                | Description                                        |
|:------------------------------------|:---------------------------------------------------|
| `tabkit-default`                    | Default face of the tabkit line.                   |
| `tabkit-separator`                  | Face of separator.                                 |
| `tabkit-button`                     | Face of button component.                          |
| `tabkit-button-highlight`           | Face to highlight button component on mouse hover. |
| `tabkit-tab-unselected`             | Face of unselected tab.                            |
| `tabkit-tab-selected`               | Face of selected tab.           |
| `tabkit-tab-highlight`              | Face to highlight tab on mouse hover.              |
| `tabkit-tab-drag-highlight`         | Face to highlight dragged tab.                     |
| `tabkit-tab-group`                  | Face of group tab.                                 |
| `tabkit-tab-instant-group`          | Face of instant group tab.                         |
| `tabkit-tab-icon`                   | Face of tab icon.                                  |
| `tabkit-tab-close-button`           | Face of tab close button.                          |
| `tabkit-tab-close-button-highlight` | Face to highlight tab close button on mouse hover. |

### Tooltips

When Emacs is compiled with libxml2, and `use-system-tooltips` is
non-nil, tootip texts are rendered according to the simple markup
below, with faces associated to tags:

| Tag    | Associated Face    | Description                                                 |
|:-------|:-------------------|:------------------------------------------------------------|
| No Tag | `tooltip`          | Default tooltip text.                                       |
| `<d>`  | `tabkit-text-desc` | Paragraph that describes available mouse shortcuts.         |
| `<k0>` | `tabkit-text-key0` | Level 0 key text used to emphasize tab, group, type names   |
| `<k1>` | `tabkit-text-key1` | Level 1 key text used for main specific mouse shortcuts.    |
| `<k2>` | `tabkit-text-key2` | Level 2 key text used for secondary common mouse shortcuts. |

###### Sample tabkit tooltip

![\ ](tabkit-tooltip.png)

### Tabs

A tab component is composed of 3 sub-components:

1. An optional icon, obtained by calling the method `tabkit-tab-icon`
   (see [Tab icons](#tab-icons)).

2. A text label, obtained by calling the method `tabkit-tab-label`.

3. An optional close button, obtained by calling the method
   `tabkit-tab-close-button`.

The option `tabkit-tab-composition` specifies which of the above
sub-components are part of the tab component and in which order.  This
is a list of sub-component names as symbols.  If a sub-component
name is duplicated, only the first occurrence matters, others are
ignored.

Here are the recognized tab sub-component names, listed in order of
appearance in the default value of option `tabkit-tab-composition`:

| Component name | Description                                          |
|:---------------|:-----------------------------------------------------|
| `icon`         | Optional tab icon.                                   |
| `label`        | mandatory tab label, automatically added if missing. |
| `badge`        | Optional tab badge to indicate marked/modified tab.  |
| `close`        | Optional tab close button.                           |

The option `tabkit-tab-text-align` controls how the text in the
sub-component `label` is aligned.  It can be left-aligned (the
default), right-aligned, or centered.

The option `tabkit-tab-width` specifies a tab width.  The width can
vary with the label width, or can be a fixed or computed number of
pixels, which can represent either a fixed width or a maximum width.
Fixed width means that all tabs have the same width and labels are
truncated or padded accordingly.  Maximum width means that labels
larger than the specified width are truncated, otherwise the width
varies with the label width.  When a tab label is truncated the option
can also specify how to handle the end of the truncated string.  That
is, either use a default ellipsis string at the end of the truncated
label; or use a specific ellipsis string; or fade the end of the
truncated label instead.

#### Tab functions (see also [Commands](#commands))

Mouse click on a tab calls the method `tabkit-tab-onclick` which is
passed the corresponding tab and the received event.

When the mouse hovers a tab, the method `tabkit-tab-help` is called
with the hovered tab as parameter, to return a help string to
display. (see [Tooltips](#tooltips)).

Mouse click on a tab close button calls the method `tabkit-tab-close`
which is passed the corresponding tab and the received mouse event.

When the mouse hovers a tab close button, the method
`tabkit-tab-close-help` is called with the hovered tab as parameter,
to return a help string to display (see [Tooltips](#tooltips)).

#### Tab icons

The option `tabkit-show-tab-icons` controls the display of tab icons.
By default, tab icons are displayed.

The option `tabkit-tab-icon-custom-icons` defines custom icons
associated to group names.  When a group name matches an association,
the corresponding icon is displayed on both the tab of this group, and
on all the tabs belonging to this group (see
[example](#default-tabkit-line-with-tab-icons) below).

When the library `xdg-mime` (see [Dependencies](#dependencies)) is
available, it is used to get a mime type icon displayed on buffer tabs
visiting a file (see [tabkit line using
images](#tabkit-line-using-images) for an example of tabs showing mime
type icons when `tabkit-image-icon-theme` is set to `"breeze"`, the
KDE Plasma Breeze theme<sup>[[2](#2)]</sup>).

The option `tabkit-tab-icon-default-icon` defines a global default
fallback icon used when no other icon is found.

###### Default tabkit line with tab icons

![\ ](tabkit-by-default+icons.png)

<br>&HorizontalLine;&HorizontalLine;&HorizontalLine;&HorizontalLine;<br>
<sup id="2">**[2]**</sup> <small>No icon theme is defined by default,
so icon images are looked up in the default `hicolor` theme.  To get
proper mime type icons it is better to define a more complete theme
like `breeze` or `oxygen` or any other similar available icon
theme.</small>

#### Tab groups

To avoid display of too many tabs at the same time and to improve
navigation between different categories of tabs, tabs can be grouped
according to various criteria.

The option `tabkit-use-groups`, enabled by default, controls whether
tabs are placed in groups.  In this case, each tab belongs to at least
one group, and only one group of tabs can be displayed at the same
time.  Left mouse click on the _home_ button toggles between display
of individual tabs and _group tabs_ which is helpful to quickly show
all tabs in a group by clicking on the associated group tab.

When `tabkit-use-groups` is disabled, no group is used, and **all**
tabs are displayed on the tabkit line. In this case, the _home_ button
is hidden too.

Currently the criteria to associate tabs to groups depends on buffer
tabs and are based on buffer criteria like buffer name, major-mode,
etc.  They are specified in the option `tabkit-buffer-group-criteria`
which is a list of clauses `(CONDITION . GROUPS)`, where `CONDITION`
is an s-expression, and `GROUPS` is a list of group names.  With
buffer temporarily current, try each clause until `CONDITION` returns
non-nil, in which case the corresponding `GROUPS` is returned.
Otherwise a list with the symbol name of the `major-mode` is returned.

If the option `tabkit-use-groups` is disabled, no group is used, and
**all** buffer tabs are displayed on the tabkit line.  In this case,
the _home_ button is hidden too.

##### Instant groups

Instant groups are groups created/deleted automatically when tabs are
interactively added or removed from them.  Instant Groups persist
across Emacs sessions, automatically saved before Emacs is killed, and
restored after Emacs is restarted, when `tabkit-mode` is enabled.
When a tab is not in any instant group, it belongs to the _default_
groups specified by the option `tabkit-buffer-group-criteria`.

The below commands manage assignment of tabs to instant groups.  They
are also available on the tab menu that pops up on right mouse click
on a tab.

- `tabkit-tab-copy-to-group`, copy tab to group.
- `tabkit-tab-move-to-group`, move tab to group.
- `tabkit-tab-remove-from-group`, remove tab from group it belongs to.

#### Tab colors

It is possible to give different colors to certain tabs to ease
distinguish them.

Currently the criteria to associate color to tabs depends on buffer
tabs and are based on buffer criteria like buffer name, major-mode,
etc.  They are specified in the option `tabkit-buffer-color-criteria`
which is a list of clauses `(CONDITION COLOR)`, where `CONDITION` is
an s-expression, and `COLOR` is a color name.  With tab and buffer
temporarily current, try each clause until CONDITION returns non-nil,
in which case the corresponding COLOR is returned.  Otherwise, nil is
returned meaning to use a default color.  In CONDITION, the function
`tabkit-current-tab` can be used to get the buffer tab currently
processed.

Unless otherwise specified, the color of the selected tab becomes the
color of its parent group.  Hence, it could make sense to have
criteria consistent in both options: `tabkit-buffer-group-criteria`
and `tabkit-buffer-color-criteria`.  To assign a different color
depending on whether group or normal tabs are displayed, you can use
criteria like this (assuming `"*scratch*"` and `"*Messages*"` belongs
to the `"Emacs"` group):

```emacs-lisp
;; Color of the group tab.
((equal "Emacs" (tabkit-tab-showing-group (tabkit-current-tab)))
 "blue")
;; Color of normal tabs.
((member (buffer-name) '( "*scratch*" "*Messages*"))
 "green")
```
Hence, when tabs in group `"Emacs"` are displayed on the tabkit line,
the color of the `"*scratch*"` and `"*Messages*"` tabs will be
`"green"`. When group tabs are displayed on the tabkit line, the color
of the `"Emacs"` group tab will be `"blue"`.

Reciprocally, unless otherwise specified, the color of a normal tab is
inherited from that of the parent group currently displayed.  With the
below example of color criteria (assuming `"*scratch*"` and
`"*Messages*"` belongs to the `"Emacs"` group):

```emacs-lisp
;; Color of the group tab.
((equal "Emacs" (tabkit-tab-showing-group (tabkit-current-tab)))
 "blue")
;; Color of normal tabs.
((member (buffer-name) '( "*scratch*"))
 "green")
```

Hence, when tabs in group `"Emacs"` are displayed on the tabkit line,
the color of the `"*scratch*"` tab will be `"green"`, as specified;
and the color of the `"*Messages*"` tab (unspecified) will be
`"blue"`, inherited from its parent group `"Emacs"`.

##### Instant colors

Instant colors are colors assigned interactively, specific to each
tab.  They apply separately to both group or normal tabs depending on
which ones are displayed on the tabkit line.  When no instant color is
assigned to a tab, the color specified by the criteria to associate
color to tabs applies.

Instant colors persist across Emacs sessions, automatically saved
before Emacs is killed, and restored after Emacs is restarted, when
`tabkit-mode` is enabled.

The below commands manage assignments of instant color to tab.  They
are also available on the tab menu that pops up on right mouse click
on a tab.

- `tabkit-tab-change-color`, change a tab color.
- `tabkit-tab-reset-color`, reset a tab default color.

#### Pinned tabs

For quick access, tabs, except _group tabs_ (see [Tab
groups](#tab-groups) above), can be pinned on the tabkit line.  Pinned
tabs are kept visible on the left of each tabkit line, except when
group tabs are displayed. The selection of a pinned tab displays the
tabkit line (group tab) to which this tab belongs.

Normally the tab icon is used to represent a pinned tab.  When the tab
icon is missing, the default icon specified by the option
`tabkit-tab-pin-icon` is used instead.  Unless the option
`tabkit-tab-prefer-pin-icon` is enabled, in which case the default
icon is always used.

#### Display order of tabs

By default, standard tabs are displayed in the order they were
created, meaning each new tab is placed at the right end of the tabkit
line.

By default, instant group tabs are initially displayed on the left
side of the tabkit-line, newest first, followed by other group tabs in
alphabetical order.  The `tabkit-group-order` option allows you to
change this default behavior.

Alternatively, any (unpinned) tab can be moved to another location on
the tabkit line, either by dragging it with the mouse or by using the
commands `tabkit-move-tab-next-to-tab` or `tabkit-move-selected-tab`.

### Performance considerations

To improve performance, most of the visual elements that compose the
tabkit line are put in a cache and reused from it.  The cached
elements are automatically refreshed when their specification or some
global options are changed.

Additionally, to reduce the number of tabkit line refreshes, the
`tabkit-safe-commands` option specifies a list of _safe_ commands,
i.e. which will not trigger tab changes, after which it is therefore
not useful to update the tabkit line.  This option defaults to a set
of common scroll and movement commands, considered as _safe_.

The function `tabkit-line-expired` marks the tabkit line expired, that
is, to be refreshed.  This function is called after tabs have changed
and runs the hook `tabkit-after-tabs-change-hook`, useful to also
refresh other dependent data.

The function `tabkit-tabs-refresh` is called to cleanup internal tabs
data in order to force refresh of all tabs.  It runs the hook
`tabkit-tabs-data-cleanup-hook`, useful to also cleanup other
dependent tabs data.

## Commands

The primary way to interact with the tabkit line is to use the mouse
to select tabs, cycle through tabs and tab groups, and press (click on)
buttons.

Default mouse actions:

| Mouse event | On | Action |
|:------------|:---|:-------|
| `mouse-1`   | Tab | Select tab. |
| `drag-mouse-1` | Tab  | Move dragged tab next to another tab<sup>[[4](#4)]</sup>. |
| `mouse-3` | Tab  | Popup a menu to act on tab. |
| `mouse-1` | Tab close button | Close tab. |
| `C-mouse-1` | Tab close button | Close both tab and window when possible. |
| `mouse-1` | Home button | Toggle display of group tabs. |
| `C-mouse-1` | Home button | Go to the selected tab. |
| `mouse-1` | Scroll button | Scroll line to the left or right. |
| `C-mouse-1` | Scroll button | Scroll one tab to the left or right. |
| `S-mouse-1` | Scroll button | Scroll to the left or right end. |
| long `down-mouse-1`<sup>[[3](#3)]</sup> | Line | Popup a menu to quickly navigate to any tab. |
| `mouse2` | Line | If one window, split it vertically, otherwise, delete other windows. |
| `S-mouse2` | Line | Unconditionally split window vertically. |
| `s-mouse2` | Line | Unconditionally split window horizontally. |
| `C-mouse2` | Line | Unconditionally delete window. |
| long `down-mouse-2`<sup>[[3](#3)]</sup> | Line | Popup a menu to manage windows. |
| long `down-mouse-3`<sup>[[3](#3)]</sup> | Line | Popup the `tabkit-options-menu` to quickly manage global options. |
| `wheel up/down` | Line | Cycle to previous/next tab or group<sup>[[5](#5)]</sup>. |
| `shift wheel up/down` | Line | Cycle through groups (or tabs when only 1 group). |
| `control wheel up/down` | Line | Cycle through visible tabs. |

In addition to using the mouse, the following commands can be used to
navigate through tabs and groups of tabs, or to move visible tabs.

| Command                             | Keybinding  | Description                                            |
|:------------------------------------|:------------|:-------------------------------------------------------|
| `tabkit-backward`                   | C-c C-left  | Cycle to previous tab or group<sup>[[5](#5)]</sup>.    |
| `tabkit-forward`                    | C-c C-right | Cycle to next tab or group<sup>[[5](#5)]</sup>.        |
| `tabkit-backward-group`             | C-c C-up    | Cycle to previous group of tabs.                       |
| `tabkit-forward-group`              | C-c C-down  | Cycle to next group of tabs.                           |
| `tabkit-backward-tab`               |             | Cycle to previous tab visible on the tab line.         |
| `tabkit-forward-tab`                |             | Cycle to next tab visible on the tab line.             |
| `tabkit-goto-tab`                   | C-c C-g     | Locate a tab by its name.                              |
| `tabkit-move-selected-tab`          |             | Move the selected tab N positions forward or backward. |
| `tabkit-move-tab-next-to-tab`       |             | Move a tab next to another tab<sup>[[4](#4)]</sup>.    |

These commands are also provided to use buttons or locally hide the
tabkit line:

| Command                     | Keybinding<sup>[[6](#6)]</sup> |
|:----------------------------|:-------------------------------|
| `tabkit-home`               | C-c C-home                     |
| `tabkit-scroll-left`        | C-c C-prior                    |
| `tabkit-scroll-right`       | C-c C-next                     |
| `tabkit-newtab-menu-popup`  | C-c C-n                        |
| `tabkit-options-menu-popup` | C-c C-o                        |
| `tabkit-locally-hide`       | C-c C-l                        |

Finally, these commands are available for miscellaneous actions:

| Command                             | Description                                         |
|:------------------------------------|:----------------------------------------------------|
| `tabkit-instant-group-load`         | Open all persistent tabs found in an instant group. |

<br>&HorizontalLine;&HorizontalLine;&HorizontalLine;&HorizontalLine;<br>
<sup id="3">**[3]**</sup> <small>A mouse down event it considered as
_long_ when the time elapsed between pressing a mouse button (mouse
down event) and releasing it (mouse click event) is greater than the
delay specified in option `gmouse-short-click-timeout`.</small>

<sup id="4">**[4]**</sup> <small>Both tabs must belong to the same
group (that is, not be group tabs), and not be pinned.  If tab is
moved to a previous tab, it is put before the target tab.  If moved to
a following tab, it is put after the target tab.</small>

<sup id="5">**[5]**</sup> <small>The cycling scope is controlled by
the `tabkit-cycle-scope` and `tabkit-cycle-functions` options.  The
default is to navigate through all tabs across groups of tabs.  Other
possible options are to navigate only through the tabs visible on the
tabkit line, through groups of tabs only, or by using the functions
provided by the option `tabkit-cycle-functions` to cycle backward and
forward.</small>

<sup id="6">**[6]**</sup> <small>Numeric prefix argument 2 and 3
emulate respectively `mouse-2` (middle mouse button) and `mouse-3`
(right mouse button).  The default is `mouse-1` (left mouse
button).</small>

## Buffer tabs

The default implementation provided displays buffers in tabs.  Mouse
left click on a tab select this tab by switching to the corresponding
buffer.

The below additional option apply to buffer tabs: 

| Option                        | Description                                      |
|:------------------------------|:-------------------------------------------------|
| `tabkit-buffer-modified-mark` | Mark on tab label to indicate a modified buffer. |

### Persistent tabs

Tabs visiting files can be made persistent by setting the option
`tabkit-use-persistent-tabs`.  If non-nil, all tabs visiting files
that are still open when Emacs ends, are automatically reopened on
next Emacs session. The pinned state of persistent tabs is also
restored.

Persistent data are automatically loaded when `tabkit-mode` is
enabled, and saved when `tabkit-mode` is disabled or before ending the
Emacs session.  If the option `tabkit-backup-persistent-data` is
enabled, also backs up a copy of the previous version of persistent
data.

### Buffer list

The list of buffers associated to tabs is provided by the function
specified in the variable `tabkit-buffer-list-function`.  The default
value, `tabkit-buffer-list`, excludes from `buffer-list` all buffers
matching the predicate defined in variable
`tabkit-exclude-predicate`. By default the exclude predicate is the
function `tabkit-buffer-exclude-p` matching internal buffers, those
for which the tabkit line is locally hidden by the command
`tabkit-locally-hide`, and those matching the criteria defined by the
option `tabkit-buffer-exclude-criteria`.

### New tab menu

###### New Tab popup menu

![\ ](tabkit-newtab-menu.png)

By default, mouse click on the `tabkit-newtab-button` (see [Buttons
and separators](#buttons-and-separators)) runs the command
`tabkit-newtab-popup-menu` that popup a menu to open a new tab with a
file, a directory, your mails, etc.  This menu can be customized by
the option `tabkit-buffer-newtab-menu`.
